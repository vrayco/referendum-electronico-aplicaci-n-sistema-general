<?php

namespace AppBundle\Tests\Utils;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use AppBundle\Entity\Usuario;

class UsuariosTest extends WebTestCase
{

    private $em;

    public function setUp()
    {
        $fixtures = array(
            'AppBundle\DataFixtures\ORM\RolesLoader',
            'AppBundle\DataFixtures\ORM\UsuariosLoader'
        );
        $this->loadFixtures($fixtures);
        $this->em = $em = $this->getContainer()->get('doctrine')->getManager();
    }

    public function tearDown()
    {
        $this->em->close();
    }

    public function testAddRole()
    {
        $faker = \Faker\Factory::create();
        $rolUsuario = $this->em->getRepository('AppBundle:Role')->findOneBy(array('name' => 'Usuario'));
        $rolAdministrador = $this->em->getRepository('AppBundle:Role')->findOneBy(array('name' => 'Administrador'));
        $usuario = new Usuario();
        $usuario->setDni($faker->numberBetween(78000000, 99000000));
        $usuario->setNombre($faker->name);
        $usuario->setApellidos($faker->lastName.' '.$faker->lastName);
        // Funcion a testear
        $this->getContainer()->get('usuario')->addRole($usuario,'Usuario');
        // Test 1: Se añadio el rol correctamente
        $roles = $usuario->getRoles();
        $this->assertContains($rolUsuario, $roles);
        // Test 2: El usuario no tiene ese rol
        $encontrado = false;
        foreach($roles as $rol) {
            if($rol === $rolAdministrador) {
                $encontrado = true;
                $this->fail("El usuario tiene un rol que no debería");
            }
        }
        $this->assertFalse($encontrado);
    }

    /**
     * @expectedException RuntimeException
     */
    public function testEnviarCodigoException()
    {
        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array());
        $opciones = array();
        $this->getContainer()->get('usuario')->enviarCodigo($usuario->getTelefonoMovil(),$opciones);
    }

    public function testEnviarCodigoOk()
    {
        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array());
        $resultado = $this->getContainer()->get('usuario')->enviarCodigo(
            $usuario->getTelefonoMovil(),
            array(
                'texto_codigo_sms'      => "Texto para el sms",
            ));
        $this->assertTrue($resultado);
    }
}