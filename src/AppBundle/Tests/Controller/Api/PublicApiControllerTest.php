<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\SolicitudApp;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class PublicApiControllerTest extends WebTestCase
{
    CONST NUMERO_DNI = '78676137Z';

    // URLS
    CONST INICIAR_SOLICITUD         = "/api/public/v1/solicitud/app.json";
    CONST VALIDAR_CODIGO1           = "/api/public/v1/solicitud/app/codigo1.json";
    CONST VALIDAR_CODIGO2           = "/api/public/v1/solicitud/app/codigo2.json";
    CONST ENTREGAR_CLAVE_PUBLICA    = "/api/public/v1/clave-publica/entregar.json";

    private $em;

    public function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->loadSolicitudesApp();
    }

    public function tearDown()
    {
        $this->em->close();
    }

    protected function assertJsonResponse($response, $statusCode = 200) {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );

        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }

    // El usuario no existe (dni incorrecto)
    public function testPostSolicitudApp1Action()
    {
        $data = array(
            'dni'               => "78888888x",
            'sistema_operativo' => "ANDROID",
            'registration_id'   => "abcdefghifk",
            'app_version'       => "1.0"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::INICIAR_SOLICITUD,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"code":400,"message":"No existe usuario con dni 78888888x en el sistema."}',
            $client->getResponse()->getContent(),
            '',
            true
        );
    }

    // Faltan parametros
    public function testPostSolicitudApp2Action()
    {
        $data = array(
            'dni'               => "78888888x",
            'sistema_operativo' => "ANDROID",
            'registration_id'   => "abcdefghifk",
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::INICIAR_SOLICITUD,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"error":{"code":400,"message":"Bad Request","exception":[{"message":"Request parameter \"app_version\"',
            $client->getResponse()->getContent(),
            '',
            true
        );
    }

    // Correcto, aunque el registration_id no es real, por eso 409
    public function testPostSolicitudApp3Action()
    {
        $data = array(
            'dni'               => "78676137z",
            'sistema_operativo' => "ANDROID",
            'registration_id'   => "abcdefghifk",
            'app_version'       => "1.0"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::INICIAR_SOLICITUD,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

    }

    // No existe la solicitud en el sistema
    public function testPostCodigo1App1Action() {

        $data = array(
            'registration_id'   => "abcdefghifk",
            'codigo_1'          => "1.00000"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);  // Porque no se consigue enviar el mensaje gcm

        $this->assertContains(
            '{"code":400,"message":"No existe la solicitud en el sistema"}',
            $client->getResponse()->getContent(),
            '',
            true
        );
    }

    // Parametros incorrectos
    public function testPostCodigo1App2Action() {

        $data = array(
            'dni'               => "77777777A",
            'codigo_2'          => "1.00000"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);  // Porque no se consigue enviar el mensaje gcm

        $this->assertContains(
            '{"error":{"code":400,"message":"Bad Request","exception":[{"message":"Request parameter \"registration_id\" is',
            $client->getResponse()->getContent(),
            '',
            true
        );

        $data = array(
            'registration_id'   => "abcdefghifk",
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);  // Porque no se consigue enviar el mensaje gcm

        $this->assertContains(
            '{"error":{"code":400,"message":"Bad Request","exception":[{"message":"Request parameter \"codigo_1\" is empty"',
            $client->getResponse()->getContent(),
            '',
            true
        );
    }

    // Codigo incorrecto y todo correcto
    public function testPostCodigo1pp3Action() {

        $solicitudes = $this->em->getRepository('AppBundle:SolicitudApp')->findAll();
        $solicitud = $solicitudes[0];

        $data = array(
            'registration_id'   => $solicitud->getRegistrationId(),
            'codigo_1'          => "abcdef"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"code":400,"message":"No existe la solicitud en el sistema"}',
            $client->getResponse()->getContent(),
            '',
            true
        );

        $data = array(
            'registration_id'   => $solicitud->getRegistrationId(),
            'codigo_1'          => $solicitud->getCodigo1()
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

        $this->assertContains(
            '{"id":',
            $client->getResponse()->getContent(),
            '',
            true
        );

    }

    // Codigo incorrecto y todo correcto
    public function testPostCodigo2App3Action() {

        $solicitudes = $this->em->getRepository('AppBundle:SolicitudApp')->findAll();
        $solicitud = $solicitudes[0];

//        $this->em->clear();

        $apps = $this->em->getRepository('AppBundle:App')->findAll();

        $app = $apps[0];

        // No se ha validado previamente el codigo 1
        $data = array(
            'dni'       => $solicitud->getUsuario()->getDni(),
            'codigo_2'  => $solicitud->getCodigo2(),
            'public_key'=> "OpenSSLRSAPublicKey{modulus=ba86db9e1128473c7c79183491a962a03ecada16fd83489f6ba1f9721a5ff95882952158082fcf32ada28e2da9aa159c96c4c502a890d8937b4dbfa6df9eb0e90674834d3b86165408d1457bdb5ec7edee648d4e96a10c3853678abd003b8ad1e8f68f847c685fb268a17ec7e8b32545b19306f386e0790e3bd497d74392ba02b97045109eecf9147987642390ae594feb24f9b5f51ce7a9e21ef241ab22f287a3caf616d00ff744881ec1699814f4d20de5b3a54ab768a8c71b5522bc86fcccec0046a1aead2bd5332d7c39e515d6ad77d60817bfe79c7261251bb1ee8084b2ee4a20a8d5e4eae9593cbc0f65c3bd10662a8a5110c3b893683b0a0cc3df45f3,publicExponent=10001}"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO2,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"code":400,"message":"La solicitud no ha valido el codigo 1"}',
            $client->getResponse()->getContent(),
            '',
            true
        );

        // Similo la validacion de codigo1
        $solicitud->setValidadoCodigo1(new \DateTime('now'));
        $this->em->flush();

        // codigo erróno
        $data = array(
            'dni'       => $solicitud->getUsuario()->getDni(),
            'codigo_2'  => "abcdefg",
            'public_key'=> "OpenSSLRSAPublicKey{modulus=ba86db9e1128473c7c79183491a962a03ecada16fd83489f6ba1f9721a5ff95882952158082fcf32ada28e2da9aa159c96c4c502a890d8937b4dbfa6df9eb0e90674834d3b86165408d1457bdb5ec7edee648d4e96a10c3853678abd003b8ad1e8f68f847c685fb268a17ec7e8b32545b19306f386e0790e3bd497d74392ba02b97045109eecf9147987642390ae594feb24f9b5f51ce7a9e21ef241ab22f287a3caf616d00ff744881ec1699814f4d20de5b3a54ab768a8c71b5522bc86fcccec0046a1aead2bd5332d7c39e515d6ad77d60817bfe79c7261251bb1ee8084b2ee4a20a8d5e4eae9593cbc0f65c3bd10662a8a5110c3b893683b0a0cc3df45f3,publicExponent=10001}"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO2,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"code":400,"message":"No existe la solicitud en el sistema"}',
            $client->getResponse()->getContent(),
            '',
            true
        );

        $solicitudes[1]->setApp($app);
        $this->em->flush();

        // Correcto
        $data = array(
            'dni'       => $solicitud->getUsuario()->getDni(),
            'codigo_2'  => $solicitud->getCodigo2(),
            'public_key'=> "OpenSSLRSAPublicKey{modulus=ba86db9e1128473c7c79183491a962a03ecada16fd83489f6ba1f9721a5ff95882952158082fcf32ada28e2da9aa159c96c4c502a890d8937b4dbfa6df9eb0e90674834d3b86165408d1457bdb5ec7edee648d4e96a10c3853678abd003b8ad1e8f68f847c685fb268a17ec7e8b32545b19306f386e0790e3bd497d74392ba02b97045109eecf9147987642390ae594feb24f9b5f51ce7a9e21ef241ab22f287a3caf616d00ff744881ec1699814f4d20de5b3a54ab768a8c71b5522bc86fcccec0046a1aead2bd5332d7c39e515d6ad77d60817bfe79c7261251bb1ee8084b2ee4a20a8d5e4eae9593cbc0f65c3bd10662a8a5110c3b893683b0a0cc3df45f3,publicExponent=10001}"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO2,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

        $this->assertContains(
            '{"id":',
            $client->getResponse()->getContent(),
            '',
            true
        );

    }

    public function testEjemploValidacion() {
        // Creo una solicitud de prueba
        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>'78676137z'));

        $registrationId = "123456789";
        $solicitud = new SolicitudApp();
        $solicitud->setUsuario($usuario);
        $solicitud->setSistemaOperativo("ANDROID");
        $solicitud->setRegistrationId($registrationId);
        $solicitud->setAppVersion("1.0.0");

        $this->em->persist($solicitud);
        $this->em->flush();

        $idSolicitud = $solicitud->getId();

        $data = array(
            'registration_id'   => $solicitud->getRegistrationId(),
            'codigo_1'          => $solicitud->getCodigo1()
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

        $this->em->clear();
        $solicitud2 = $this->em->getRepository('AppBundle:SolicitudApp')->find($idSolicitud);
        $this->assertNotNull($solicitud2->getValidadoCodigo1());


        // Envio el codigo 2
        $data = array(
            'dni'       => $solicitud2->getUsuario()->getDni(),
            'codigo_2'  => $solicitud2->getCodigo2(),
            'public_key'=> "OpenSSLRSAPublicKey{modulus=ba86db9e1128473c7c79183491a962a03ecada16fd83489f6ba1f9721a5ff95882952158082fcf32ada28e2da9aa159c96c4c502a890d8937b4dbfa6df9eb0e90674834d3b86165408d1457bdb5ec7edee648d4e96a10c3853678abd003b8ad1e8f68f847c685fb268a17ec7e8b32545b19306f386e0790e3bd497d74392ba02b97045109eecf9147987642390ae594feb24f9b5f51ce7a9e21ef241ab22f287a3caf616d00ff744881ec1699814f4d20de5b3a54ab768a8c71b5522bc86fcccec0046a1aead2bd5332d7c39e515d6ad77d60817bfe79c7261251bb1ee8084b2ee4a20a8d5e4eae9593cbc0f65c3bd10662a8a5110c3b893683b0a0cc3df45f3,publicExponent=10001}"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO2,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

        $this->em->clear();
        $solicitud3 = $this->em->getRepository('AppBundle:SolicitudApp')->find($idSolicitud);
        $this->assertNotNull($solicitud3->getValidadoCodigo2());

        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>'78676137z'));
        $registrationId1 = $usuario->getApp()->getRegistrationId();
        $this->assertEquals($registrationId, $registrationId1);


        // Realizo actualización
        $registrationId2 = "abcdeffg";
        $solicitud4 = new SolicitudApp();
        $solicitud4->setUsuario($usuario);
        $solicitud4->setSistemaOperativo("ANDROID");
        $solicitud4->setRegistrationId($registrationId2);
        $solicitud4->setAppVersion("2.0.0");

        $this->em->persist($solicitud4);
        $this->em->flush();

        $idSolicitud2 = $solicitud4->getId();

        $data = array(
            'registration_id'   => $solicitud4->getRegistrationId(),
            'codigo_1'          => $solicitud4->getCodigo1()
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO1,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

        $this->em->clear();
        $solicitud5 = $this->em->getRepository('AppBundle:SolicitudApp')->find($idSolicitud2);
        $this->assertNotNull($solicitud5->getValidadoCodigo1());


        // Envio el codigo 2
        $data = array(
            'dni'       => $solicitud5->getUsuario()->getDni(),
            'codigo_2'  => $solicitud5->getCodigo2(),
            'public_key'=> "OpenSSLRSAPublicKey{modulus=ba86db9e1128473c7c79183491a962a03ecada16fd83489f6ba1f9721a5ff95882952158082fcf32ada28e2da9aa159c96c4c502a890d8937b4dbfa6df9eb0e90674834d3b86165408d1457bdb5ec7edee648d4e96a10c3853678abd003b8ad1e8f68f847c685fb268a17ec7e8b32545b19306f386e0790e3bd497d74392ba02b97045109eecf9147987642390ae594feb24f9b5f51ce7a9e21ef241ab22f287a3caf616d00ff744881ec1699814f4d20de5b3a54ab768a8c71b5522bc86fcccec0046a1aead2bd5332d7c39e515d6ad77d60817bfe79c7261251bb1ee8084b2ee4a20a8d5e4eae9593cbc0f65c3bd10662a8a5110c3b893683b0a0cc3df45f3,publicExponent=10001}"
        );

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::VALIDAR_CODIGO2,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

        $this->em->clear();
        $solicitud6 = $this->em->getRepository('AppBundle:SolicitudApp')->find($idSolicitud2);
        $this->assertNotNull($solicitud6->getValidadoCodigo2());

        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>'78676137z'));
        $registrationId3 = $usuario->getApp()->getRegistrationId();
        $this->assertEquals($registrationId2, $registrationId3);

        $solicitud7 = $usuario->getApp()->getSolicitudApp();
        $this->assertNotNull($solicitud7);
    }

    public function testEntregarClavePublica()
    {
        // Petición con falta de parametros
        $data = array(
            'dni'   => "78676137z"
        );

        $client = static::createClient(array(), array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::ENTREGAR_CLAVE_PUBLICA,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"error":{"code":400,"message":"Bad Request","exception":[{"message":"Request parameter \"clave_publica\" is empty"',
            $client->getResponse()->getContent(),
            '',
            true
        );

        // El dni no es correcto
        $data1 = array(
            'dni'               => "99999999A",
            'clave_publica'    => "-----BEGIN PUBLIC KEY-----\n".
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv/o5yAj66uNNebWGIf1DIg/ssIymosxQ\n".
                "csfQIAr7vrsQXIclOMPC21ctuVrbVC3wOE5rI+mKYZU7dZheaZecJDWyv+/CyYgNrOo1yliyit2O\n".
                "KkvxX/vCTwO2rprci4b1Qosf+63PdwgRgOoM/RYmE18hZmUKYtzYozPTcEmHzSfgqMfOgJO09hzz\n".
                "8ycXncklPBO6gZctrk/1RHY3C/adBdWEPRocXlIurd/iETw2TJ2izTXhyZEIj8TKidbi5bDMJhAf\n".
                "FoKNnM1t4dC8JUYZ0hdwCc2UR0mt0ECkJ3/IvIqY61APUwNPv5Lku1bHeRjnJWcRltbbX/rD51in\n".
                "5NUC4QIDAQAB\n".
                "-----END PUBLIC KEY-----\n",
        );

        $client = static::createClient(array(), array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::ENTREGAR_CLAVE_PUBLICA,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data1)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"code":400,"message":"No existe usuario con dni 99999999A en el sistema."}',
            $client->getResponse()->getContent(),
            '',
            true
        );

        // La clave publica no es correcta
        $data2 = array(
            'dni'               => "78676137z",
            'clave_publica'    => "-----BEGIN PUBLIC KEY-----\n".
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv/o5yAj66uNNebWGIf1DIg/ssIymosxQ\n".
                "csfQIAr7vrsQXIclOMPC21ctuVrbVC3wOE5rI+mKYZU7dZheaZecJDWyv+/CyYgNrOo1yliyit2O\n".
                "KkvxX/vCTwO2rprci4b1Qosf+63PdwgRgOoM/RYmE18hZmUKYtzYozPTcEmHzSfgqMfOgJO09hzz\n".
                "8ycXncklPBO6gZctrk/1RHY3C/adBdWEPRocXlIurd/iETw2TJ2izTXhyZEIj8TKidbi5bDMJhAf\n".
                "FoKNnM1t4dC8JUYZ0hdwCc2UR0mt0ECkJ3/IvIqY61APUwNPv5Lku1bHeRjnJWcRltbbX/rD51in\n".
                "FoKNnM1t4dC8JUYZ0hdwCc2UR0mt0ECkJ3/IvIqY61APUwNPv5Lku1bHeRjnJWcRltbbX/rD51in\n".
                "5NUC4QIDAQAB\n".
                "-----END PUBLIC KEY-----\n",
        );

        $client = static::createClient(array(), array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::ENTREGAR_CLAVE_PUBLICA,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data2)
        );

        $this->assertJsonResponse($client->getResponse(), 400);

        $this->assertContains(
            '{"code":400,"message":"La clave p\u00fablica no es v\u00e1lida: -----BEGIN PUBLIC KEY-----\nMIIBIjANBgk',
            $client->getResponse()->getContent(),
            '',
            true
        );

        // Correcto. Pero no se llega a enviar el mensaje push
        $data1 = array(
            'dni'               => "78676137z",
            'clave_publica'     => "-----BEGIN PUBLIC KEY-----\n".
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv/o5yAj66uNNebWGIf1DIg/ssIymosxQ\n".
                "csfQIAr7vrsQXIclOMPC21ctuVrbVC3wOE5rI+mKYZU7dZheaZecJDWyv+/CyYgNrOo1yliyit2O\n".
                "KkvxX/vCTwO2rprci4b1Qosf+63PdwgRgOoM/RYmE18hZmUKYtzYozPTcEmHzSfgqMfOgJO09hzz\n".
                "8ycXncklPBO6gZctrk/1RHY3C/adBdWEPRocXlIurd/iETw2TJ2izTXhyZEIj8TKidbi5bDMJhAf\n".
                "FoKNnM1t4dC8JUYZ0hdwCc2UR0mt0ECkJ3/IvIqY61APUwNPv5Lku1bHeRjnJWcRltbbX/rD51in\n".
                "5NUC4QIDAQAB\n".
                "-----END PUBLIC KEY-----\n",
        );

        $client = static::createClient(array(), array('HTTPS' => true));
        $crawler = $client->request(
            'POST',
            SELF::ENTREGAR_CLAVE_PUBLICA,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($data1)
        );

        $this->assertJsonResponse($client->getResponse(), 200);

    }

    private function loadUsuarios()
    {
        $fixtures = array(
            'AppBundle\DataFixtures\ORM\RolesLoader',
            'AppBundle\DataFixtures\ORM\UsuariosLoader'
        );
        $this->loadFixtures($fixtures);
    }

    private function loadSolicitudesApp()
    {
        $fixtures = array(
            'AppBundle\DataFixtures\ORM\RolesLoader',
            'AppBundle\DataFixtures\ORM\UsuariosLoader',
            'AppBundle\DataFixtures\ORM\SolicitudesAppLoader',
            'AppBundle\DataFixtures\ORM\AppLoader'
        );
        $this->loadFixtures($fixtures);
    }

}
