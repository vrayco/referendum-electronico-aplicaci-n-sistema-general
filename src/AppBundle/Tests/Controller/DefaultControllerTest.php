<?php

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    CONST NUMERO_DNI = '78676137Z';

    private $em;

    private $parametrosServidorFnmt = array(
        "HTTPS" => "on",
        "SSL_CLIENT_S_DN_O" => "FNMT",
        "SSL_CLIENT_S_DN_OU" => "FNMT Clase 2 CA",
        "SSL_CLIENT_S_DN_OU_1" => "701007413",
        "SSL_CLIENT_S_DN_CN" => "NOMBRE VELAZQUEZ GONZALEZ RAYCO - NIF 78676137Z",
        "SSL_CLIENT_I_DN_C" => "ES",
        "SSL_CLIENT_I_DN_O" => "FNMT",
        "SSL_CLIENT_I_DN_OU" => "FNMT Clase 2 CA",
        "SSL_CLIENT_VERIFY" => "SUCCESS",
        "SSL_CLIENT_S_DN" => "CN=NOMBRE VELAZQUEZ GONZALEZ RAYCO - NIF 78676137Z,OU=701007413,OU=FNMT Clase 2 CA,O=FNMT,C=ES",
    );

    private $parametrosServidorDni = array(
        "HTTPS" => "on",
        "SSL_CLIENT_S_DN_CN" => "VELAZQUEZ GONZALEZ, RAYCO (AUTENTICACIÓN)",
        "SSL_CLIENT_I_DN_C" => "ES",
        "SSL_CLIENT_I_DN_O" => "DIRECCION GENERAL DE LA POLICIA",
        "SSL_CLIENT_I_DN_OU" => "DNIE",
        "SSL_CLIENT_VERIFY" => "SUCCESS",
        "SSL_CLIENT_S_DN" => "CN=VELAZQUEZ GONZALEZ\, RAYCO (AUTENTICACIÓN),GN=RAYCO,SN=VELAZQUEZ,serialNumber=78676137Z,C=ES",
    );

    public function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
    }

    public function tearDown()
    {
        $this->em->close();
    }

    // Sin certificado no se permite el acceso
    public function testAccesoSinCertificado()
    {
        $_SERVER['SSL_CLIENT_VERIFY'] = 'NONE';
        $client = static::createClient(array(),array('HTTPS' => true));

        $crawler = $client->request('GET', '/backend/');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals($client->getResponse()->headers->get('location'), '/certificado/no/valido');

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("No se te permite el acceso porque tu certificado no es válido")')->count() > 0);
    }

    // Con certificado no válido
    public function testAccesoCertificadoInvalido()
    {
        $this->loadRoles();
        foreach($this->parametrosServidorFnmt as $key => $p)
            $_SERVER[$key] = $p;
        $_SERVER['SSL_CLIENT_VERIFY'] = 'FAILED';

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'GET',
            '/backend/',
            array(),
            array(),
            $this->parametrosServidorFnmt
        );

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals($client->getResponse()->headers->get('location'), '/certificado/no/valido');

        $crawler = $client->followRedirect();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("No se te permite el acceso porque tu certificado no es válido")')->count() > 0);
    }

    // Con certificado válido. El usuario existe
    public function testCertificadoValido() {
        $this->loadUsuarios();

        $this->certificadoValido($this->parametrosServidorFnmt);
        $this->certificadoValido($this->parametrosServidorDni);
    }


    private function certificadoValido($parametrosServidor)
    {
        foreach($parametrosServidor as $key => $p)
            $_SERVER[$key] = $p;

        $client = static::createClient(array(),array('HTTPS' => true));
        $crawler = $client->request(
            'GET',
            '/backend/',
            array(),
            array(),
            $parametrosServidor
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Tu cuenta está lista para usar el sistema.")')->count() > 0);
    }


    // Con certificado válido. El usuario no existe por lo que se crea, se añade número de telefono y se valida
    public function testIndexOk2()
    {
        $this->loadRoles();

        // El usuario no existe en el sistema
        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>SELF::NUMERO_DNI));
        $this->assertNull($usuario);

        foreach($this->parametrosServidorFnmt as $key => $p)
            $_SERVER[$key] = $p;

        $client = static::createClient(array(),array('HTTPS' => true));

        $crawler = $client->request(
            'GET',
            '/backend/',
            array(),
            array(),
            $this->parametrosServidorFnmt
        );

        // El sistema ha creado el usuario
        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>SELF::NUMERO_DNI));
        $this->assertNotNull($usuario);

        // El sistema nos redirige al formulario para introducir num teléfono
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $crawler = $client->followRedirect();
        $this->assertTrue($crawler->filter('html:contains("Por favor, introduce tu número de teléfono")')->count() > 0);

        // Enviamos el formulario vacio, mensaje de error porque el número de telefono es obligatorio
        $form = $crawler->selectButton('Guardar')->form();
        $client->setServerParameters($this->parametrosServidorFnmt);
        $crawler = $client->submit($form);
        $this->assertTrue($crawler->filter('html:contains("Por favor, ingrese su teléfono")')->count() > 0);

        // Enviamos el formulario correctamente. Comprobamos que se envía el sms.
        $form = $crawler->selectButton('Guardar')->form();
        $form['telefono_movil[numeroTelefono]'] = '699999999';
        $client->setServerParameters($this->parametrosServidorFnmt);
        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Para poder ser un votante activo el sistema debe validar tu DNI y tu número de teléfono.")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("¡Success! Hemos enviado tu código de validación por sms con éxito.")')->count() > 0);

        // Obtenemos el código de validación generado
        $usuario = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>SELF::NUMERO_DNI));
        $codigo = $usuario->getTelefonoMovil()->getCodigo();

        // Accedemos al formulario para validar el telefono
        $link = $crawler->selectLink('Introducir código')->link();
        $crawler = $client->click($link);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Gestión de usuario: Validar teléfono")')->count() > 0);

        // Introducimos un código de validación erróneo
        $form = $crawler->selectButton('Guardar')->form();
        $client->setServerParameters($this->parametrosServidorFnmt);
        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertTrue($crawler->filter('html:contains("¡Danger! El código que has introducido no es correcto")')->count() > 0);

        // Introducimos un código de validación correcto. Comprobamos que la cuenta está correctamente validada
        $link = $crawler->selectLink('Introducir código')->link();
        $crawler = $client->click($link);
        $form = $crawler->selectButton('Guardar')->form();
        $form['form[codigo]'] = $codigo;
        $client->setServerParameters($this->parametrosServidorFnmt);
        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertTrue($crawler->filter('html:contains("Tu cuenta está lista para usar el sistema.")')->count() > 0);
    }

    private function loadRoles()
    {
        $fixtures = array(
            'AppBundle\DataFixtures\ORM\RolesLoader',
        );
        $this->loadFixtures($fixtures);
    }

    private function loadUsuarios()
    {
        $fixtures = array(
            'AppBundle\DataFixtures\ORM\RolesLoader',
            'AppBundle\DataFixtures\ORM\UsuariosLoader'
        );
        $this->loadFixtures($fixtures);
    }

}
