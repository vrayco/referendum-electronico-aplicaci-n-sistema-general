<?php

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class EventoElectoralControllerTest extends WebTestCase
{

    private $parametrosServidor = array(
        "HTTPS" => "on",
        "SSL_CLIENT_S_DN_O" => "FNMT",
        "SSL_CLIENT_S_DN_OU" => "FNMT Clase 2 CA",
        "SSL_CLIENT_S_DN_OU_1" => "701007413",
        "SSL_CLIENT_S_DN_CN" => "NOMBRE VELAZQUEZ GONZALEZ RAYCO - NIF 78676137z",
        "SSL_CLIENT_I_DN_C" => "ES",
        "SSL_CLIENT_I_DN_O" => "FNMT",
        "SSL_CLIENT_I_DN_OU" => "FNMT Clase 2 CA",
        "SSL_CLIENT_VERIFY" => "SUCCESS",
        "SSL_CLIENT_S_DN" => "CN=NOMBRE VELAZQUEZ GONZALEZ RAYCO - NIF 78676137Z,OU=701007413,OU=FNMT Clase 2 CA,O=FNMT,C=ES",
        "PHP_AUTH_USER"     => "78676137z"                  // Para simular a este usuario
    );

    public function testCompleteScenario()
    {
        $this->loadUsuarios();
//        $em = $this->getContainer()->get('doctrine')->getManager();
//        $usuarios = $em->getRepository('AppBundle:Usuario')->findAll();
//        foreach($usuarios as $u) {
//            $roles = $u->getRoles();
//            echo "\nUsuario: ".$u->getDni();
//            foreach($roles as $r)
//                echo " ".$r->getName();
//        }

        foreach($this->parametrosServidor as $key => $p)
            $_SERVER[$key] = $p;

        // Create a new client to browse the application
        $client = static::createClient(array(),array('HTTPS' => true));

        // Create a new entry in the database
        $crawler = $client->request(
            'GET',
            '/backend/eventoelectoral/',
            array(),
            array(),
            $this->parametrosServidor
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /backend/eventoelectoral/");
        $this->assertEquals(1, $crawler->filter('h1:contains("Gestión de Eventos Electorales")')->count());

        // Creo un evento
        $client->setServerParameters($this->parametrosServidor);
        $crawler = $client->click($crawler->selectLink('Nuevo evento')->link());

        $form = $crawler->selectButton('Guardar')->form(array(
            'appbundle_eventoelectoral[nombre]'                 => 'TEST Elecciones Generales 2020',
            'appbundle_eventoelectoral[descripcion]'            => 'TEST Descripcion Elecciones Generales 2020',
            'appbundle_eventoelectoral[inicio][date][day]'      => '1',
            'appbundle_eventoelectoral[inicio][date][month]'    => '1',
            'appbundle_eventoelectoral[inicio][date][year]'     => '2020',
            'appbundle_eventoelectoral[fin][date][day]'         => '1',
            'appbundle_eventoelectoral[fin][date][month]'       => '1',
            'appbundle_eventoelectoral[fin][date][year]'        => '2020',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertEquals(1, $crawler->filter('h1:contains("TEST Elecciones Generales 2020")')->count());

        // Edito un evento
        $client->setServerParameters($this->parametrosServidor);
        $crawler = $client->click($crawler->selectLink('Editar')->link());


        $form = $crawler->selectButton('Guardar')->form(array(
            'appbundle_eventoelectoral[fin][date][year]'        => '2019',
        ));

        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('html:contains("La fecha 01/01/2019 00:00 debe ser posterior a 01/01/2020 00:00")')->count());

        $form = $crawler->selectButton('Guardar')->form(array(
            'appbundle_eventoelectoral[fin][date][day]'         => '2',
            'appbundle_eventoelectoral[fin][date][year]'        => '2020',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        $this->assertEquals(1, $crawler->filter('html:contains(" Se ha actualizado el evento Electoral: TEST Elecciones Generales 2020.")')->count());

        // Volvemos a show evento
        $crawler = $client->click($crawler->selectLink('Volver')->link());
        $this->assertEquals(1, $crawler->filter('h1:contains("TEST Elecciones Generales 2020")')->count());
//        var_dump($client->getResponse()->getContent());


        // Check data in the show view
        //$this->assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

//        // Edit the entity
//        $crawler = $client->click($crawler->selectLink('Edit')->link());
//
//        $form = $crawler->selectButton('Update')->form(array(
//            'appbundle_eventoelectoral[field_name]'  => 'Foo',
//            // ... other fields to fill
//        ));
//
//        $client->submit($form);
//        $crawler = $client->followRedirect();
//
//        // Check the element contains an attribute with value equals "Foo"
//        $this->assertGreaterThan(0, $crawler->filter('[value="Foo"]')->count(), 'Missing element [value="Foo"]');
//
//        // Delete the entity
//        $client->submit($crawler->selectButton('Delete')->form());
//        $crawler = $client->followRedirect();
//
//        // Check the entity has been delete on the list
//        $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }

    private function loadUsuarios()
    {
        $fixtures = array(
            'AppBundle\DataFixtures\ORM\RolesLoader',
            'AppBundle\DataFixtures\ORM\UsuariosLoader'
        );
        $this->loadFixtures($fixtures);
    }
}
