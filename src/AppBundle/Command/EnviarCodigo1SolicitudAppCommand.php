<?php

namespace AppBundle\Command;

use AppBundle\Entity\EventoElectoral;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnviarCodigo1SolicitudAppCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('referendumelectronico:solicitud_app:enviar_codigo1')
            ->setDescription('Envía notificación con el CODIGO_1 de registro')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $solicitudes = $em->getRepository('AppBundle:SolicitudApp')->findBy(array('validadoCodigo1' => NULL));

        $notificaciones = 0;
        $contador = 0;
        foreach ($solicitudes as $solicitud) {
            // Enviamos el codigo via GCM
            $mensaje = array(
                "data" => array(
                    "registro" => array(
                        "codigo_1" => $solicitud->getCodigo1()
                    )
                )
            );

            $push = $this->getContainer()->get('push_notification');
            $push->setPlataforma($solicitud->getSistemaOperativo());
            $push->setRegistrationIds(array($solicitud->getRegistrationId()));
            $push->setData($mensaje);
            $resultado = $push->send();

            if($resultado->success == "1") {
                $notificaciones++;
            }
            $contador++;
        }
        
        $em->flush();

        $output->writeln(
            sprintf("[SOLICITUD APP][CODIGO_1] Se han enviado %s de %s notificaciones.", $notificaciones, $contador)
        );

    }
}