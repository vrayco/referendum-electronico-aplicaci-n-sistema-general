<?php

namespace AppBundle\Command;

use AppBundle\Controller\Backend\EventoElectoralController;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\L1;
use AppBundle\Entity\Papeleta;
use AppBundle\Entity\VotacionListasAbiertas;
use AppBundle\Entity\VotacionListasCerradas;
use AppBundle\Entity\VotacionReferendum;
use AppBundle\Entity\Voto;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EscrutrinioEventoElectoralCommand extends ContainerAwareCommand
{
    const ANALIZAR                  = "ANALIZAR";
    const ANALIZAR_Y_CONTABILIZAR   = "ANALIZAR_Y_CONTABILIZAR";

    private $em;
    private $logger;
    private $rsa;
    private $mcrypt;

    protected function configure()
    {
        $this
            ->setName('referendumelectronico:eventoelectoral:escutrinio')
            ->setDescription('Realiza el escrutinio.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em       = $this->getContainer()->get('doctrine')->getManager();
        $this->logger   = $this->getContainer()->get('monolog.logger.auditoria');
        $this->rsa      = $this->getContainer()->get('rsa');
        $this->mcrypt   = $this->getContainer()->get('mcrypt');

        $eventos = $this->em->getRepository('AppBundle:EventoElectoral')->findBy(array('estado' => EventoElectoral::ESTADO_CERRADO));

        foreach ($eventos as $evento) {

            $now = new \DateTime('now');
            $this->logger->info(
                sprintf("[EVENTO %d] Comienza el escutrinio a las %s", $evento->getId(), $now->format('d/m/y h:i:s'))
            );

            // Clave privada del EVENTO ELECTORAL
            $priKey = $this->rsa->getPrivateKey(
                sprintf(EventoElectoralController::PREFIX_CLAVE_RSA, $evento->getId())
            );

            // INICIALIZO TODAS LAS VOTACIONES, NUMERO DE VOTOS = 0
            $this->ponerEnCero($evento);

            $papeletas = $this->getContainer()->get('api_sistema_electoral')->getPapeletas($evento);
            $numNulos = 0;

            if($papeletas !== false) {
                foreach ($papeletas as $p) {
                    $resultado = $this->analizarPapeleta($p, $evento, $priKey);
                    if($resultado !== false)
                        $this->contabilizarPapeleta($p, $evento, $priKey);
                    else
                        $numNulos++;
                }
            }

            $this->logger->info(
                sprintf("[EVENTO %d] Número total de papeletas = %d", $evento->getId(), count($papeletas))
            );

            $this->logger->info(
                sprintf("[EVENTO %d] Número total de papeletas nulas = %d", $evento->getId(), $numNulos)
            );

            $now = new \DateTime('now');
            $this->logger->info(
                sprintf("[EVENTO %d] Finalización del escutrinio a las %s", $evento->getId(), $now->format('d/m/y h:i:s'))
            );

            $evento->setNumeroPapeletas(count($papeletas));
            $evento->setNumeroPapeletasNulas($numNulos);
            $evento->setEstado(EventoElectoral::ESTADO_FINALIZADO);
        }

        $this->em->flush();
    }

    private function ponerEnCero(EventoElectoral $eventoElectoral)
    {
        $eventoElectoral->setNumeroPapeletas(0);
        $eventoElectoral->setNumeroPapeletasNulas(0);
        $votaciones = $eventoElectoral->getVotacion();
        foreach ($votaciones as $v) {
            $v->setNumeroVotos(0);
            if ($v->getClass() == "VotacionListasCerradas")
                $this->ponerEnCeroVotacionListasCerradas($v);
            elseif ($v->getClass() == "VotacionListasAbiertas")
                $this->ponerEnCeroVotacionListasAbiertas($v);
            elseif ($v->getClass() == "VotacionReferendum")
                $this->ponerEnCeroVotacionReferendum($v);
        }

    }

    private function ponerEnCeroVotacionListasCerradas(VotacionListasCerradas $votacion)
    {
        $votacion->setNumeroVotosEnBlanco(0);
        $listas = $votacion->getListas();
        foreach ($listas as $l)
            $l->setNumeroVotos(0);
    }

    private function ponerEnCeroVotacionListasAbiertas(VotacionListasAbiertas$votacion)
    {
        $votacion->setNumeroVotosEnBlanco(0);
        $listas = $votacion->getListas();
        foreach ($listas as $l) {
            $candidatos = $l->getCandidatos();
            foreach ($candidatos as $c)
                $c->setNumeroVotos(0);
        }

    }

    private function ponerEnCeroVotacionReferendum(VotacionReferendum $votacion)
    {
        $preguntas = $votacion->getPreguntas();
        foreach ($preguntas as $p) {
            $p->setNumeroVotosEnBlanco(0);
            $respuestas = $p->getRespuestas();
            foreach ($respuestas as $r)
                $r->setNumeroVotos(0);
        }
    }

    private function analizarPapeleta($papeleta, $evento, $priKey)
    {
        return $this->procesarPapeleta(self::ANALIZAR,$papeleta, $evento, $priKey);
    }

    private function contabilizarPapeleta($papeleta, $evento, $priKey)
    {
        return $this->procesarPapeleta(self::ANALIZAR_Y_CONTABILIZAR,$papeleta, $evento, $priKey);
    }

    private function procesarPapeleta($accion = self::ANALIZAR, $papeleta, $evento, $priKey)
    {
        try {
            if(!isset($papeleta->id) or !(isset($papeleta->v_prima)) or !(isset($papeleta->votaciones)))
                throw new Exception(sprintf("Formato de la papeleta incorrecto. Papeleta = id + V'"));

            $papeletaId = $papeleta->id;
            $votaciones = $papeleta->votaciones;

            $json = json_decode($papeleta->v_prima);
            if(!isset($json->iv) or !isset($json->key) or !isset($json->v))
                throw new Exception(sprintf("[PAPELETA %d] V' no tiene el formato adecuado V'=iv+key+v", $papeletaId));

            $iv = $json->iv;
            $keyCifrada = $json->key;
            $vCifrado = $json->v;

            // Desciframos la key para descrifrar la papeleta con la clave privada del evento electoral
            $key = $this->rsa->privateDecrypt($keyCifrada,$priKey);
            if($key === false)
                throw new Exception(sprintf("[PAPELETA %d] Problemas descifrando key", $papeletaId));

            $this->mcrypt->setKey($key);
            $this->mcrypt->setIV($iv);
            $v = $this->mcrypt->decrypt($vCifrado);

            $json = json_decode($v);
            if($json === null)
                throw new Exception(sprintf("[PAPELETA %d] Problema descifrando v", $papeletaId));

            if($json->evento_electoral !== $evento->getId())
                throw new Exception(sprintf("[PAPELETA %d] No coincide el id del evento.", $papeletaId));

            if(!isset($json->votacion_referendums) or !isset($json->votacion_listas_cerradas) or !isset($json->votacion_listas_abiertas))
                throw new Exception(sprintf("[PAPELETA %d] El contenido no es correcto, no tiene el formato correcto de votaciones", $papeletaId));

            // PARA LISTAS CERRADAS
            foreach ($json->votacion_listas_cerradas as $v) {
                if(!isset($v->votacion_id) or !isset($v->listas))
                    throw new Exception(sprintf("[PAPELETA %d] No tiene el formato correcto en votaciones listas cerradas.", $papeletaId));

                $votacion = $this->em->getRepository('AppBundle:VotacionListasCerradas')->find($v->votacion_id);
                if(!$votacion)
                    throw new Exception(sprintf("[PAPELETA %d] No existe la votación con id=%s", $papeletaId, $v->votacion_id));
                if($votacion->getEventoElectoral() !== $evento)
                    throw new Exception(sprintf("[PAPELETA %d] La votacion=%s no forma parte del evento", $papeletaId, $votacion->getId()));

                // Comprobamos que el elector puede votar en esta votacion
                if(!in_array($votacion->getId(), $votaciones))
                    throw new Exception(sprintf("[PAPELETA %d] El elector no esta convocado a esta votacion=%s", $papeletaId, $votacion->getId()));

                $listas = $v->listas;
                if(count($listas) > $votacion->getVotosPorElector())
                    throw new Exception(sprintf("[PAPELETA %d][VOTACION %d] Más votos de los permitidos por elector", $papeleta->id, $votacion->getId()));

                $numVotos = 0;
                foreach ($listas as $l) {
                    $lista = $this->em->getRepository('AppBundle:Lista')->find($l);
                    if(!$lista)
                        throw new Exception(sprintf("[VOTACION %d] No existe la lista con id=%s",$l->getId(), $votacion->getId()));
                    if($lista->getVotacion() !== $votacion) // Compruebo que la lista forme parte de la votación
                        throw new Exception(sprintf("[VOTACION %d] La lista=%s no forma parte la votación", $l->getId(), $votacion->getId()));

                    $numVotos++;
                    if($accion === self::ANALIZAR_Y_CONTABILIZAR)
                        $lista->incNumeroVotos();
                }

                if($accion === self::ANALIZAR_Y_CONTABILIZAR) {
                    $votacion->incNumeroVotos($numVotos);
                    $votacion->incNumeroVotosEnBlanco($votacion->getVotosPorElector() - $numVotos);
                }
            }

            // PARA LISTAS ABIERTAS
            foreach ($json->votacion_listas_abiertas as $v) {
                if(!isset($v->votacion_id) or !isset($v->candidatos))
                    throw new Exception(sprintf("[PAPELETA %d] No tiene el formato correcto en votaciones listas abiertas.", $papeletaId));

                $votacion = $this->em->getRepository('AppBundle:VotacionListasAbiertas')->find($v->votacion_id);
                if(!$votacion)
                    throw new Exception(sprintf("[PAPELETA %d] No existe la votación con id=%s",$papeletaId, $v->votacion_id));
                if($votacion->getEventoElectoral() !== $evento)
                    throw new Exception(sprintf("[PAPELETA %d] La votacion=%s no forma parte del evento",$papeletaId, $votacion->getId()));

                // Comprobamos que el elector puede votar en esta votacion
                if(!in_array($votacion->getId(), $votaciones))
                    throw new Exception(sprintf("[PAPELETA %d] El elector no esta convocado a esta votacion=%s", $papeletaId, $votacion->getId()));

                $candidatos = $v->candidatos;
                if(count($candidatos) > $votacion->getVotosPorElector())
                    throw new Exception(sprintf("[PAPELETA %d][VOTACION %d] Más votos de los permitidos por elector", $papeleta->id, $votacion->getId()));

                $numVotos = 0;
                foreach ($candidatos as $c) {
                    $candidato = $this->em->getRepository('AppBundle:Candidato')->find($c);
                    if(!$candidato)
                        throw new Exception(sprintf("[PAPELETA %d] No existe el candidato con id=%s",$papeletaId, $c->getId()));
                    if($candidato->getLista()->getVotacion() !== $votacion) // Compruebo que la lista forme parte de la votación
                        throw new Exception(sprintf("[PAPELETA %d] La lista=%s no forma parte la votación",$papeletaId, $l->getId()));

                    $numVotos++;
                    if($accion === self::ANALIZAR_Y_CONTABILIZAR)
                        $candidato->incNumeroVotos();
                }

                if($accion === self::ANALIZAR_Y_CONTABILIZAR) {
                    $votacion->incNumeroVotos($numVotos);
                    $votacion->incNumeroVotosEnBlanco($votacion->getVotosPorElector() - $numVotos);
                }
            }

            // PARA REFERENDUM
            foreach ($json->votacion_referendums as $v) {
                if(!isset($v->votacion_id) or !isset($v->preguntas))
                    throw new Exception(sprintf("[PAPELETA %d] No tiene el formato correcto en votaciones referendums.", $papeletaId));

                $votacion = $this->em->getRepository('AppBundle:VotacionReferendum')->find($v->votacion_id);
                if(!$votacion)
                    throw new Exception(sprintf("[PAPELETA %d] No existe la votación con id=%s", $papeletaId, $v->votacion_id));
                if($votacion->getEventoElectoral() !== $evento)
                    throw new Exception(sprintf("[PAPELETA %d] La votacion=%s no forma parte del evento", $papeletaId, $votacion->getId()));

                // Comprobamos que el elector puede votar en esta votacion
                if(!in_array($votacion->getId(), $votaciones))
                    throw new Exception(sprintf("[PAPELETA %d] El elector no esta convocado a esta votacion=%s", $papeletaId, $votacion->getId()));
                
                $preguntas = $v->preguntas;

                $numVotosPorElectorEnReferendum = 1; // La preguntas por ahora no son multirespuesta
                foreach ($preguntas as $preguntaId => $respuestas) {
                    $numVotos = 0;
                    $pregunta = $this->em->getRepository('AppBundle:Pregunta')->find($preguntaId);
                    if(!$pregunta)
                        throw new Exception(sprintf("[PAPELETA %d] No existe la pregunta con id=%s",$papeletaId, $pregunta->getId()));
                    if($pregunta->getReferendum() !== $votacion) // Compruebo que la pregunta forme parte de la votación
                        throw new Exception(sprintf("[PAPELETA %d] La pregunta=%s no forma parte la votación", $papeletaId, $pregunta->getId()));

                    foreach($respuestas as $r) {  // Como no son multirespuesta, este bucle solo tiene una iteracion
                        $respuesta = $this->em->getRepository('AppBundle:Respuesta')->find($r);
                        if(!$respuesta)
                            throw new Exception(sprintf("[PAPELETA %d] No existe la respuesta con id=%s",$papeletaId, $respuesta->getId()));
                        if($respuesta->getPregunta() !== $pregunta) // Compruebo que la respuesta forme parte de la pregunta
                            throw new Exception(sprintf("[PAPELETA %d] La respuesta=%s no forma parte la pregunta=%s", $papeletaId, $pregunta->getId(), $pregunta->getId()));

                        $numVotos++;
                        if($accion === self::ANALIZAR_Y_CONTABILIZAR)
                            $respuesta->incNumeroVotos();
                    }
                    
                    if($accion === self::ANALIZAR_Y_CONTABILIZAR)
                        $pregunta->incNumeroVotosEnBlanco($numVotosPorElectorEnReferendum - $numVotos);
                }

                if($accion === self::ANALIZAR_Y_CONTABILIZAR)
                    $votacion->incNumeroVotos();
            }

            if($accion === self::ANALIZAR_Y_CONTABILIZAR)
                $this->logger->info(
                    sprintf("[EVENTO %d] Contabilizada papeleta ID: %s ", $evento->getId(), $papeletaId)
                );

        } catch (Exception $ex) {
            // Detectado voto nulo
            $this->logger->info(
                sprintf("[EVENTO %d]%s [PAPELETA NULA]", $evento->getId(), $ex->getMessage())
            );

            return false;
        }

        return true;
    }
}