<?php

namespace AppBundle\Command;

use AppBundle\Entity\EventoElectoral;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificarCierreEventoElectoralCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('referendumelectronico:eventoelectoral:cerrar')
            ->setDescription('Envía notificación a las aplicaciones móviles sobre el cierre del proceso electoral')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $eventos = $em->getRepository('AppBundle:EventoElectoral')->findBy(array('estado' => EventoElectoral::ESTADO_CERRADO));

        $notificaciones = 0;
        $contador = 0;
        foreach ($eventos as $evento) {

            $censoElectoral = $evento->getCensoElectoral();
            foreach ($censoElectoral as $elector) {
                if ($elector->getNotificacionCierre() === null) {
                    $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $elector->getDni()));
                    if ($usuario and $usuario->isValid()) {
                        $contador++;
                        $mensaje = array(
                            "data" => array(
                                "cierre_evento_electoral" => array(
                                    "id" => $evento->getId()
                                )
                            )
                        );

                        $push = $this->getContainer()->get('push_notification');
                        $push->setPlataforma($usuario->getApp()->getSistemaOperativo());
                        $push->setRegistrationIds(array($usuario->getApp()->getRegistrationId()));
                        $push->setData($mensaje);
                        $resultado = $push->send();

                        if ($resultado->success == "1") {
                            $notificaciones++;
                            $elector->setNotificacionCierre(new \DateTime('now'));
                        }
                    }
                }

            }


        }
        $em->flush();

        $output->writeln(
            sprintf("[NOTIFICACIONES CIERRE EVENTO ELECTORAL] Se han enviado %s de %s notificaciones.", $notificaciones, $contador)
        );
    }
}