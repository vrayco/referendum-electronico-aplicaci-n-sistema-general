<?php

namespace AppBundle\Command;

use AppBundle\Entity\EventoElectoral;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificarConvocarEventoElectoralCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('referendumelectronico:eventoelectoral:convocar')
            ->setDescription('Envía notificación a las aplicaciones móviles sobre la convocatoria del evento electoral')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $eventos = $em->getRepository('AppBundle:EventoElectoral')->findBy(array('estado' => EventoElectoral::ESTADO_CONVOCADO));

        $notificaciones = 0;
        $contador = 0;
        foreach ($eventos as $evento) {
            $censoElectoral = $evento->getCensoElectoral();
            foreach ($censoElectoral as $elector) {
                if ($elector->getNotificacionConvocatoria() === null) {
                    $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $elector->getDni()));
                    if ($usuario and $usuario->isValid()) {
                        $contador++;
                        $mensaje = array(
                            "data" => array(
                                "convocatoria_evento_electoral" => array(
                                    "id" => $evento->getId()
                                )
                            )
                        );

                        $push = $this->getContainer()->get('push_notification');
                        $push->setPlataforma($usuario->getApp()->getSistemaOperativo());
                        $push->setRegistrationIds(array($usuario->getApp()->getRegistrationId()));
                        $push->setData($mensaje);
                        $resultado = $push->send();

                        if ($resultado->success == "1") {
                            $notificaciones++;
                            $elector->setNotificacionConvocatoria(new \DateTime('now'));
                        }
                    }
                }
            }
            $em->flush();

            $output->writeln(
                sprintf("[CONVOCAR EVENTO ELECTORAL %d] Se han enviado %s de %s notificaciones.", $evento->getId(), $notificaciones, $contador)
            );
        }
    }
}