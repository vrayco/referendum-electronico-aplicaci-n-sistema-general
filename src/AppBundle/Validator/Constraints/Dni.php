<?php
	
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Dni extends Constraint
{
    public $message_blank = 'Por favor, ingrese su DNI';
    public $message = 'El DNI "%string%" que ha introducido no es correcto';

    
    public function validatedBy()
	{
    	return get_class($this).'Validator';
	}
}