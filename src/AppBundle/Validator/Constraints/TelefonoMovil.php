<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TelefonoMovil extends Constraint
{
    public $message = 'El Número de teléfono "%string%" que ha introducido no es correcto';
    public $format  = '/[67]\d{8}/';
    
    public function requiredOptions()
    {
        return array();
    }
    
    public function defaultOption()
    {
        return '';
    }
    
    public function validatedBy()
    {
    	return get_class($this).'Validator';
    }
}