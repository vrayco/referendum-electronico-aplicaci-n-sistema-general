<?php
	

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Checks if given phone number matches with constraint format.
 * Note that this validator does not validate blank fields, use NotBlank assert instead.
 *
 * @author Moisés Maciá <moises.macia@ideup.com>
 */
class TelefonoMovilValidator extends ConstraintValidator
{
    /**
     * @param string $value
     * @param Constraint $constraint
     * @return bool
     */
    public function validate($value, Constraint $constraint)
    {
        if (empty($value)) {
           return true;
        }
        $ret = $this->validateNumber($value, $constraint->format);
        if (!$ret) {
        	$this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }

    }
    
    /**
     * @param string $value
     * @param string $format Regular expression format
     * @return bool
     */
    protected function validateNumber($value, $format)
    {
        $ret = preg_match($format, trim($value));
        return ($ret !== false && $ret >= 1);
    }
}