<?php

namespace AppBundle\Security\Authorization\Voter;

use AppBundle\Entity\EventoElectoral;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class EventoElectoralVoter implements VoterInterface
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const ABRIR= 'abrir';
    const CERRAR= 'cerrar';

    public function supportsAttribute($attribute)
    {
        return in_array($attribute, array(
            self::VIEW,
            self::EDIT,
            self::ABRIR,
            self::CERRAR
        ));
    }

    public function supportsClass($class)
    {
        $supportedClass = 'AppBundle\Entity\EventoElectoral';

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }


    public function vote(TokenInterface $token, $eventoElectoral, array $attributes)
    {
        // check if class of this object is supported by this voter
        if (!$this->supportsClass(get_class($eventoElectoral))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // check if the voter is used correct, only allow one attribute
        // this isn't a requirement, it's just one easy way for you to
        // design your voter
        if (1 !== count($attributes)) {
            throw new \InvalidArgumentException(
                'Only one attribute is allowed for VIEW or EDIT'
            );
        }

        // set the attribute to check against
        $attribute = $attributes[0];

        // check if the given attribute is covered by this voter
        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // get current logged in user
        $user = $token->getUser();

        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        switch($attribute) {
            case self::VIEW:
                $administradores = $eventoElectoral->getAdministrador();
                foreach($administradores as $a)
                    if($a->getDni() == $user->getUsername())
                        return VoterInterface::ACCESS_GRANTED;

                break;
            case self::EDIT:
                if($eventoElectoral->getEstado() == EventoElectoral::ESTADO_EDITABLE) {
                    $administradores = $eventoElectoral->getAdministrador();
                    foreach ($administradores as $a)
                        if ($a->getDni() == $user->getUsername())
                            return VoterInterface::ACCESS_GRANTED;

                }
                break;
            case self::ABRIR:
                if($eventoElectoral->getEstado() == EventoElectoral::ESTADO_CONVOCADO) {
                    $administradores = $eventoElectoral->getAdministrador();
                    foreach ($administradores as $a)
                        if ($a->getDni() == $user->getUsername())
                            return VoterInterface::ACCESS_GRANTED;

                }
                break;
            case self::CERRAR:
                if($eventoElectoral->getEstado() == EventoElectoral::ESTADO_ABIERTO) {
                    $administradores = $eventoElectoral->getAdministrador();
                    foreach ($administradores as $a)
                        if ($a->getDni() == $user->getUsername())
                            return VoterInterface::ACCESS_GRANTED;

                }
                break;
        }

        return VoterInterface::ACCESS_DENIED;
    }
}