<?php
	
namespace AppBundle\Security\User;

use AppBundle\Utils\UsuariosService;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Util\StringUtils;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\ValidacionDniAuto;
use AppBundle\Utils\UsuariosService as ServicioUsuario;

use AppBundle\Security\User\UserX509;

class UserProviderX509 implements UserProviderInterface
{
	
	const POLICIA	= "DIRECCION GENERAL DE LA POLICIA";
	const FNMT		= "FNMT";
	const FNMTRCM	= "FNMT-RCM";
	const PAIS 		= "ES";

	protected $em;
    protected $servicios_usuario;
	
	public function __construct(EntityManager $entityManager, ServicioUsuario $servicioUsuario) {
	    $this->em = $entityManager;
        $this->servicios_usuario = $servicioUsuario;
	}   
	
    public function loadUserByUsername($username)
    {
		if(isset($_SERVER['SSL_CLIENT_VERIFY']))
			if($_SERVER['SSL_CLIENT_VERIFY'] === 'SUCCESS' or $_SERVER['SSL_CLIENT_VERIFY'] === 'GENEROUS') {
	    	//$certClient = $_SERVER['SSL_CLIENT_CERT'];
	    	//$infoUsuario = $this->getX509Data($username, $certClient);
            $infoUsuario = $this->getInfoUsuario($_SERVER['SSL_CLIENT_I_DN_O'],$_SERVER['SSL_CLIENT_S_DN'], $_SERVER['SSL_CLIENT_S_DN_CN']);
	        if ($infoUsuario) {
		        $user = $this->em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $infoUsuario['usuario']['nif']));
		        // Si el usuario no existe, lo añado a la BD
		        if(!$user) {
			        $user = new Usuario();
			        $user->setDni($infoUsuario['usuario']['nif']);
			        $user->setEmail($infoUsuario['usuario']['email']);
			        $user->setNombre($infoUsuario['usuario']['nombre']);
					$user->setApellidos($infoUsuario['usuario']['apellidos']);
                    $user = $this->servicios_usuario->addRole($user, 'Usuario');
                    $user = $this->servicios_usuario->addRole($user, 'Administrador');
					$this->em->persist($user);
					// Guardamos la validación del dni del usuario
					$validacionDni = new ValidacionDniAuto();
					$validacionDni->setUsuario($user);
					$this->em->persist($validacionDni);
					$this->em->flush();
		        } elseif($user->getValidacionDniAuto() === null) {  // Si existe pero aún no ha sido validado el dni
					// Guardamos la validación del dni del usuario
					$validacionDni = new ValidacionDniAuto();
					$validacionDni->setUsuario($user);
					$this->em->persist($validacionDni);
					$this->em->flush();
				}

                $numeroTelefono = null;
                if($user->getTelefonoMovil()) {
                    $numeroTelefono = $user->getTelefonoMovil()->getNumeroTelefono();
                }

	            return new UserX509(
	            	$user->getDni(),
	            	null,
	            	null,
	            	$user->getRole(),
	            	$user->getNombre(),
	            	$user->getApellidos(),
	            	$user->getEmail(),
	            	$user->isValidDni(),
	            	$user->isValidTelefono(),
                    $numeroTelefono
	            );
	        }

	        throw new UsernameNotFoundException(
	            sprintf('Username "%s" does not exist.', $username)
	        );
        }

        throw new UsernameNotFoundException(
	        sprintf('Certificado no válido.')
	    );
        
    }

    public function getInfoUsuario($SSL_CLIENT_I_DN_O, $SSL_CLIENT_S_DN, $SSL_CLIENT_S_DN_CN)
    {
        $info = array();
        $policia = StringUtils::equals($SSL_CLIENT_I_DN_O, SELF::POLICIA);
        $fnmt = StringUtils::equals($SSL_CLIENT_I_DN_O, SELF::FNMT);
		$fnmtrcm = StringUtils::equals($SSL_CLIENT_I_DN_O, SELF::FNMTRCM);

        if($policia) {
            preg_match("/[.]*[\/|,]serialNumber=(\w*)[\/|,][.]*/", $SSL_CLIENT_S_DN, $output_array);
            $info['usuario']['nif'] = $output_array[1];
            preg_match("/[.]*,\s(\w*)[\s(AUTENTICA][.]*/", $SSL_CLIENT_S_DN_CN, $output_array);
            $info['usuario']['nombre']= $output_array[1];
            preg_match("/([\w|\s]+)[,\s][.]*/", $SSL_CLIENT_S_DN_CN, $output_array);
            $info['usuario']['apellidos'] = $output_array[1];
            $info['usuario']['email'] = null;
        } elseif($fnmt) {
            preg_match("/[.]*-\sNIF\s(\w*)[.]*/", $SSL_CLIENT_S_DN_CN, $output_array);
            $info['usuario']['nif'] = $output_array[1];
            preg_match("/NOMBRE\s([\w|\s]*)-\sNIF\s[.]*/", $SSL_CLIENT_S_DN_CN, $output_array);
            $info['usuario']['nombre']= $output_array[1];
            $info['usuario']['apellidos'] = null;
            $info['usuario']['email'] = null;
        } elseif($fnmtrcm) {
			preg_match("/[.]*,serialNumber=(\w*),[.]*/", $SSL_CLIENT_S_DN, $output_array);
			$info['usuario']['nif'] = $output_array[1];
			preg_match("/[.]*,GN=([\w|\s]+),[.]*/", $SSL_CLIENT_S_DN, $output_array);
			$info['usuario']['nombre']= $output_array[1];
			preg_match("/[.]*,SN=([\w|\s]+),[.]*/", $SSL_CLIENT_S_DN, $output_array);
			$info['usuario']['apellidos'] = $output_array[1];
			$info['usuario']['email'] = null;

		}

        return $info;
    }

    public function getX509Data($username, $certificado)
    {
	    $data = array();
		$matches = array();

        try {

       	    $certData = openssl_x509_parse($certificado);
	        // Extraigo los datos de CA y compruebo que es válida
 	        $data['C'] = $certData['issuer']['C'];
 	        if(strcmp($data['C'],SELF::PAIS) != 0)
 	        	throw new BadCredentialsException("El certificado no es válido. La CA debe ser FNMT o dnie");
 	        $data['O'] = $certData['issuer']['O'];

			// Datos del usuario
			$data['usuario']['SSL_CLIENT_S_DN'] = $username;
			if(strcmp($data['O'],SELF::POLICIA) == 0) {
				$data['usuario']['email'] = null;
				$data['usuario']['nif'] = $certData['subject']['serialNumber'];
				$data['usuario']['nombre'] = $certData['subject']['GN'];
				preg_match("/([\w\s]*),[.]*/", $certData['subject']['CN'], $matches);
				$data['usuario']['apellidos'] = $matches[1];

			} elseif (strcmp($data['O'],SELF::FNMT) == 0) {
				$subjectAltName = $certData['extensions']['subjectAltName'];
				// Dirección de email
				preg_match("/email:(.*), DirName:[.]*/", $subjectAltName, $matches);
				$data['usuario']['email'] = $matches[1];
				// NIF
				preg_match("/[.]*, DirName: 1.3.6.1.4.1.5734.1.4 = (\d*[a-zA-Z]),[.]*/", $subjectAltName, $matches);
				$data['usuario']['nif'] = $matches[1];
				// Apellidos
				preg_match("/[.]*, 1.3.6.1.4.1.5734.1.3 = ([\w\s]*),[.]*/", $subjectAltName, $matches);
				$segundoApellido = $matches[1];
				preg_match("/[.]*, 1.3.6.1.4.1.5734.1.2 = ([\w\s]*),[.]*/", $subjectAltName, $matches);
				$primerApellido = $matches[1];
				$data['usuario']['apellidos'] = $primerApellido.' '.$segundoApellido;
				// Nombre
				preg_match("/[.]*, 1.3.6.1.4.1.5734.1.1 = ([\w\s]*)$/", $subjectAltName, $matches);
				$data['usuario']['nombre'] = $matches[1];

			} else {
				throw new BadCredentialsException("El certificado no es válido. La CA debe ser FNMT o dnie");
			}

	        // Numero de serie del certificado
	        $data['serialNumber'] = $certData['serialNumber'];
	        // Periodo de validez
			$data['validFrom'] = $certData['validFrom'];
			$data['validTo'] = $certData['validTo'];

        } catch(\Exception $ex) {

	        throw new BadCredentialsException(
            	sprintf('Hay problemas con tu certificado')
			);
			return null;
        }

        return $data;

    }

    public function refreshUser(UserInterface $user)
    {

        if (!$user instanceof UserX509) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'AppBundle\Security\User\UserX509';
    }
}