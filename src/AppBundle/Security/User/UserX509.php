<?php

namespace AppBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class UserX509 implements UserInterface, EquatableInterface
{

    private $username;
    private $password;
    private $salt;
    private $roles;
    private $nombre;
    private $apellidos;
    private $email;
    private $validacionDni;
    private $validacionTelefono;
    private $numeroTelefono;
    

    public function __construct($username, $password, $salt, array $roles, $nombre, $apellidos, $email, $validacionDni, $validacionTelefono, $numeroTelefono)
    {
        $this->username             = $username;
        $this->password             = $password;
        $this->salt                 = $salt;
        $this->roles                = $roles;
		$this->nombre               = $nombre;
		$this->apellidos            = $apellidos;
		$this->email                = $email;
		$this->validacionDni        = $validacionDni;
		$this->validacionTelefono   = $validacionTelefono;
        $this->numeroTelefono       = $numeroTelefono;
    }
    
	public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->username;
    }
    
	public function getNombre()
    {
        return $this->nombre;
    }
	
	public function getApellidos()
    {
        return $this->apellidos;
    }
    
	public function getEmail()
    {
        return $this->email;
    }

	public function getValidacionDni()
    {
        return $this->validacionDni;
    }

	public function getValidacionTelefono()
    {
        return $this->validacionTelefono;
    }

    public function getNumeroTelefono()
    {
        return $this->numeroTelefono;
    }

    public function eraseCredentials()
    {
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof UserX509) {
            return false;
        }
        
        if($this->username !== $user->getUsername()) {
        	return false;	
        }

        return true;
    }
}