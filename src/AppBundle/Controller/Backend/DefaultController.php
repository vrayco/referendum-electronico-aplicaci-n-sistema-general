<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="backend")
     */
    public function indexAction()
    {
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirect($this->generateUrl('certificado_no_valido'));
        }
        else {
            if($this->getUser()->getNumeroTelefono())
                return $this->render('Backend/index.html.twig');
            else
                return $this->redirect($this->generateUrl('usuario_numero_telefono'));
        }
    }

    /**
     * @Route("/{dni}/perfil", name="perfil")
     * @Security("has_role('ROLE_USER')")
     */
    public function perfilAction(Usuario $usuario)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('owner', $usuario)) {
            throw new AccessDeniedException('Acceso no autorizado!');
        }

        return $this->render('Backend/perfil.html.twig', array(
            'usuario'   => $usuario,
        ));
    }

}
