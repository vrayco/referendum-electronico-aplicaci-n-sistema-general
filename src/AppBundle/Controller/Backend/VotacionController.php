<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Votacion;
use AppBundle\Form\VotacionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * EventoElectoral controller.
 *
 * @Route("/votacion")
 * @Security("has_role('ROLE_ADMIN')")
 */
class VotacionController extends Controller
{


//    /**
//     * Creates a form to create a Votacion entity.
//     *
//     * @param Votacion $entity The entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createCreateForm(Votacion $entity)
//    {
//        $form = $this->createForm(new VotacionType(), $entity, array(
//            'action' => $this->generateUrl('votacion_new'),
//            'method' => 'POST',
//        ));
//
//        return $form;
//    }
//
//    /**
//     * Displays a form to create a new EventoElectoral entity.
//     *
//     * @Route("/new", name="votacion_new")
//     * @Method({"GET","POST"})
//     * @Template("Backend/votacion/new.html.twig")
//     */
//    public function newAction(Request $request)
//    {
//        $entity = new Votacion();
//        $form   = $this->createCreateForm($entity);
//        $form->handleRequest($request);
//
//        if($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//
//            // El usuario que crea el evento es siempre administrador del mismo
//            $me = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>$this->getUser()->getUsername()));
//            $entity->addAdministrador($me);
//
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl(
//                'eventoelectoral_show',
//                array('id'  => $entity->getId())
//            ));
//
//        }
//
//        return array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
//        );
//    }

//    /**
//     * Finds and displays a EventoElectoral entity.
//     *
//     * @Route("/{id}", name="eventoelectoral_show")
//     * @Method("GET")
//     * @Template("Backend/eventoElectoral/show.html.twig")
//     */
//    public function showAction($id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
//        }
//
//        if (false === $this->get('security.authorization_checker')->isGranted('view', $entity)) {
//            throw new AccessDeniedException('Unauthorised access!');
//        }
//
//        $deleteForm = $this->createDeleteForm($id);
//
//        return array(
//            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),
//        );
//    }
//
//    /**
//     * Displays a form to edit an existing EventoElectoral entity.
//     *
//     * @Route("/{id}/edit", name="eventoelectoral_edit")
//     * @Method({"GET","PUT"})
//     * @Template("Backend/eventoElectoral/edit.html.twig")
//     */
//    public function editAction(Request $request, $id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
//        }
//        $editForm = $this->createEditForm($entity);
//        $editForm->handleRequest($request);
//
//        if ($editForm->isValid()) {
//            $em->flush();
//
//            $request->getSession()->getFlashBag()->add(
//                'success',
//                sprintf("Se ha actualizado el evento Electoral: %s.", $entity->getNombre())
//            );
//
//            return $this->redirect($this->generateUrl('eventoelectoral_edit', array('id' => $id)));
//        }
//
//
//        //$deleteForm = $this->createDeleteForm($id);
//
//        return array(
//            'entity'      => $entity,
//            'edit_form'   => $editForm->createView(),
//        );
//    }
//
//    /**
//     * Creates a form to edit a EventoElectoral entity.
//     *
//     * @param EventoElectoral $entity The entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createEditForm(EventoElectoral $entity)
//    {
//        $form = $this->createForm(new EventoElectoralType(), $entity, array(
//            'action' => $this->generateUrl('eventoelectoral_edit', array('id' => $entity->getId())),
//            'method' => 'PUT',
//        ));
//
//
//        return $form;
//    }
//
//    /**
//     * Deletes a EventoElectoral entity.
//     *
//     * @Route("/{id}", name="eventoelectoral_delete")
//     * @Method("DELETE")
//     */
//    public function deleteAction(Request $request, $id)
//    {
//        $form = $this->createDeleteForm($id);
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);
//
//            if (!$entity) {
//                throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
//            }
//
//            $em->remove($entity);
//            $em->flush();
//        }
//
//        return $this->redirect($this->generateUrl('eventoelectoral'));
//    }
//
//    /**
//     * Creates a form to delete a EventoElectoral entity by id.
//     *
//     * @param mixed $id The entity id
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createDeleteForm($id)
//    {
//        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('eventoelectoral_delete', array('id' => $id)))
//            ->setMethod('DELETE')
//            ->add('submit', 'submit', array('label' => 'Sí, eliminar', 'attr' => array('class' => 'btn btn-danger')))
//            ->getForm()
//            ;
//    }
}
