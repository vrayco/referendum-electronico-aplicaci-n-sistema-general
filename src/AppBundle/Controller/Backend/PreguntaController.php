<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Pregunta;
use AppBundle\Form\PreguntaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 *
 * @Route("/pregunta")
 * @Security("has_role('ROLE_ADMIN')")
 *
 */
class PreguntaController extends Controller
{

    /**
     * @Route("/{id}/show", name="pregunta_index")
     * @Method({"GET","PUT"})
     * @Template("Backend/pregunta/index.html.twig")
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Votacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votacion entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('view', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $className = $em->getClassMetadata(get_class($entity))->getName();
        if($className != "AppBundle\\Entity\\VotacionReferendum")
            throw $this->createNotFoundException('Las votaciones de listas no tienen preguntas asociadas.');

        $entities = $em->getRepository('AppBundle:Pregunta')->findBy(array("referendum" => $entity), array('orden' => 'ASC'));

        return array(
            'entity'    => $entity,
            'entities'  => $entities,
        );
    }


    private function createCreateForm(Pregunta $entity, $votacion_id)
    {
        $form = $this->createForm(new PreguntaType(), $entity, array(
            'action'        => $this->generateUrl('pregunta_new', array('id'=>$votacion_id)),
            'method'        => 'POST'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Pregunta entity.
     *
     * @Route("/{id}/new", name="pregunta_new")
     * @Method({"GET","POST"})
     * @Template("Backend/pregunta/new.html.twig")
     */
    public function newAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $votacion = $em->getRepository('AppBundle:VotacionReferendum')->find($id);

        if (!$votacion) {
            throw $this->createNotFoundException('Unable to find VotacionReferendum entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $votacion->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $entity = new Pregunta();
        $entity->setReferendum($votacion);

        $form   = $this->createCreateForm($entity, $votacion->getId());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'pregunta_index',
                array('id'  => $votacion->getId())
            ));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Pregunta entity.
     *
     * @Route("/{id}/edit", name="pregunta_edit")
     * @Method({"GET","PUT"})
     * @Template("Backend/pregunta/edit.html.twig")
     */
    public function editAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Pregunta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pregunta entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getReferendum()->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha actualizado la pregunta: %s.", $entity->getPregunta())
            );

            return $this->redirect($this->generateUrl('pregunta_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     *
     * @param Pregunta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pregunta $entity)
    {
        $form = $this->createForm(new PreguntaType(), $entity, array(
            'action' => $this->generateUrl('pregunta_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * @Route("/{id}", name="pregunta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $votacion_id = null;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Pregunta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pregunta entity.');
            }

            if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getReferendum()->getEventoElectoral())) {
                throw new AccessDeniedException('Unauthorised access!');
            }

            $votacion_id = $entity->getReferendum()->getId();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pregunta_index', array('id' => $votacion_id)));
    }

    /**
     * @Route("/{id}/predelete", name="pregunta_predelete")
     * @Method("GET")
     * @Template("Backend/pregunta/delete.html.twig")
     */
    public function preDeleteAction($id)
    {
        $form = $this->createDeleteForm($id);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Pregunta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pregunta entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getReferendum()->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        return array(
            'entity'        => $entity,
            'delete_form'   => $form->createView(),
        );

    }

    /**
     * Creates a form to delete a EventoElectoral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pregunta_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Sí, eliminar', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
            ;
    }
}
