<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Form\UserRoleType;
use AppBundle\Form\UsuarioDatosContactoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\TelefonoMovil;
use AppBundle\Entity\ValidacionDni;
use AppBundle\Form\UsuarioType;
use AppBundle\Form\TelefonoMovilType;


class UsuarioController extends Controller
{
	const TEXTO_CODIGO_SMS                          = "TU CODIGO DE VALIDACION ES: %s";
    const MENSAJE_FLASH_CODIGO_SMS_OK               = "El envío del código de validación para el usuario con dni %s se ha realizado con éxito.";
    const MENSAJE_FLASH_CODIGO_SMS_NO_OK            = "El envío del código de validación para el usuario con dni %s no se ha realizado.";
    const MENSAJE_FLASH_USUARIO_CODIGO_SMS_OK       = "Hemos enviado tu código de validación por sms con éxito.";
    const MENSAJE_FLASH_USUARIO_CODIGO_SMS_NO_OK    = "¡Ups! No te hemos podido enviar por sms tu código de validación";

	private $rol_inicial = 'Usuario';
	
    /**
     * @Route("/usuarios", name="usuario")
	 * @Security("has_role('ROLE_ADMIN')")     
     */
    public function indexAction(Request $request)
    {
	    $em = $this->getDoctrine()->getManager();
	    $entities = $em->getRepository('AppBundle:Usuario')->findBy(
            array(),
            array('id' => 'ASC')
        );
	    
        return $this->render('Backend/usuario/index.html.twig', array(
	        'users'	=>	$entities
        ));
    }
    	
    /**
     * @Route("/usuarios/nuevo", name="usuario_new")
	 * @Security("has_role('ROLE_ADMIN')")       
     */
    public function newAction(Request $request)
    {
	    $usuario = new Usuario();
	    $telefono = new TelefonoMovil();
 	    $usuario->setTelefonoMovil($telefono);
		$telefono->setUsuario($usuario);
	    $form = $this->createForm(new UsuarioType(), $usuario);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() && $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        
	        $me = $em->getRepository('AppBundle:Usuario')->findOneBy(array(
	        	'dni'	=> $this->getUser()->getUsername()
	        	)
	        );
	        
	        // Validacion del dni
	        $validacion = new ValidacionDni();
	        $validacion->setUsuario($usuario);
	        $validacion->setRegistrador($me);
	        $usuario->setValidacionDni($validacion);
	        
	        // Añado rol al usuario
	        $usuario = $this->get('usuario')->addRole($usuario, $this->rol_inicial);

	        $em->persist($usuario);
	        $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Usuario con dni %s creado con éxito.", $usuario->getDni())
            );
	        
	        // Envío sms con el código de validación del número de teléfono
            $this->get('usuario')->enviarCodigo(
                $usuario->getTelefonoMovil(),
                array(
                    'texto_codigo_sms'      => SELF::TEXTO_CODIGO_SMS,
                    'flash_bag'             => $request->getSession()->getFlashBag(),
                    'mensaje_flash_ok'      => sprintf(SELF::MENSAJE_FLASH_CODIGO_SMS_OK, $usuario->getDni()),
                    'mensaje_flash_no_ok'   => sprintf(SELF::MENSAJE_FLASH_CODIGO_SMS_NO_OK, $usuario->getDni())
                ));
	 
	        return $this->redirect($this->generateUrl('usuario'));
	    }
	    
		return $this->render(
			'Backend/usuario/nuevo.html.twig',
			array('form'	=> $form->createView())
		);
    }

    /**
     * @Route("/usuarios/{id}/editar", name="usuario_edit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, Usuario $usuario)
    {
        // Solo los usuarios registrados que no hayan sido validados a través de certificado digital puede ser editados
        if($usuario->getValidacionDniAuto())
            throw new BadRequestHttpException(sprintf("El usuario %s fue validado por certificado electrónico y no es editable", $usuario->getDni()));

        $telefono_old = $usuario->getTelefonoMovil()->getNumeroTelefono();
        $form = $this->createForm(new UsuarioType(), $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Si el numero de telefono ha cambiado. Inicializamos el proceso de validación
            if($telefono_old !== $usuario->getTelefonoMovil()->getNumeroTelefono()) {
                $this->get('usuario')->inicializarValidacionTelefono($usuario, $request, "usuario");
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Usuario con dni %s editado con éxito.", $usuario->getDni())
            );

            return $this->redirect($this->generateUrl('usuario'));
        }

        return $this->render(
            'Backend/usuario/editar.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/usuarios/{dni}/auto/editar", name="usuario_auto_edit")
     * @Security("has_role('ROLE_USER')")
     */
    public function autoEditAction(Request $request, Usuario $usuario)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('owner', $usuario)) {
            throw new AccessDeniedException('Acceso no autorizado!');
        }

        $telefono_old = $usuario->getTelefonoMovil()->getNumeroTelefono();
        $form = $this->createForm(new UsuarioDatosContactoType(), $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Si el numero de telefono ha cambiado. Inicializamos el proceso de validación
            if($telefono_old !== $usuario->getTelefonoMovil()->getNumeroTelefono()) {
                $this->get('usuario')->inicializarValidacionTelefono($usuario, $request, "usuario");
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Usuario con dni %s editado con éxito.", $usuario->getDni())
            );

            return $this->redirect($this->generateUrl('perfil', array('dni' => $usuario->getDni())));
        }

        return $this->render(
            'Backend/usuario/auto_editar.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/usuarios/{id}/cambiar/telefono", name="usuario_cambiar_telefono")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function cambiarTelefonoAction(Request $request, Usuario $usuario)
    {
        $telefono_old = $usuario->getTelefonoMovil()->getNumeroTelefono();
        $form = $this->createForm(new TelefonoMovilType(), $usuario->getTelefonoMovil());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Si el numero de telefono ha cambiado. Inicializamos el proceso de validación
            if($telefono_old !== $usuario->getTelefonoMovil()->getNumeroTelefono()) {
                if($telefono_old !== $usuario->getTelefonoMovil()->getNumeroTelefono()) {
                    $this->get('usuario')->inicializarValidacionTelefono($usuario, $request, "usuario");
                }

                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("Se ha actualizado el número de teléfono del usuario con dni %s.", $usuario->getDni())
                );
            }

            return $this->redirect($this->generateUrl('usuario'));
        }

        return $this->render(
            'Backend/usuario/numero_telefono.html.twig',
            array('form' => $form->createView(),
            'titulo_pagina' => 'Gestión de usuario: Cambiar número de teléfono'
            )
        );
    }

    /**
     * @Route("/usuarios/{id}/validar/telefono/usuario", name="usuario_validar_telefono_usuario")
	 * @Security("has_role('ROLE_ADMIN')")       
     */
    public function validarTelefonoUsuarioAction(Request $request, Usuario $usuario)
    {
	    $defaultData = array('message' => 'Escribe el código');
	    $form = $this->createFormBuilder($defaultData)
	        ->add('codigo', 'text')
	        ->getForm();
	
	    $form->handleRequest($request);
	
	    if ($form->isSubmitted() && $form->isValid()) {
		    $data = $form->getData();
		    
		    if($usuario->getTelefonoMovil()->getCodigo() == $data['codigo']) {
			    $usuario->getTelefonoMovil()->setValidado(true);
			    $em = $this->getDoctrine()->getManager();
			    $em->persist($usuario);
			    $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("El teléfono móvil del usuario con dni %s ha sido validado.", $usuario->getDni())
                );
			}
			else {
                $request->getSession()->getFlashBag()->add(
                    'danger',
                    sprintf("El teléfono móvil del usuario con dni %s NO ha sido validado.", $usuario->getDni())
                );
			}
			
	        return $this->redirect($this->generateUrl('usuario'));
			
	    }
	    
		return $this->render(
			'Backend/usuario/validar_telefono.html.twig',
			array('form'	=> $form->createView())
		);
    }

    /**
     * @Route("/usuarios/{id}/reenviar/codigo", name="usuario_reenviar_codigo")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function reenviarCodigoAction(Request $request, Usuario $usuario)
    {
        // Envío sms con el código de validación del número de teléfono
        $this->get('usuario')->enviarCodigo(
            $usuario->getTelefonoMovil(),
            array(
                'texto_codigo_sms'      => SELF::TEXTO_CODIGO_SMS,
                'flash_bag'             => $request->getSession()->getFlashBag(),
                'mensaje_flash_ok'      => sprintf(SELF::MENSAJE_FLASH_CODIGO_SMS_OK, $usuario->getDni()),
                'mensaje_flash_no_ok'   => sprintf(SELF::MENSAJE_FLASH_CODIGO_SMS_NO_OK, $usuario->getDni())
            ));

        return $this->redirect($this->generateUrl('usuario'));
    }

    /**
     * @Route("/usuarios/{id}/eliminar", name="usuario_eliminar")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function eliminarAction(Request $request, Usuario $usuario)
    {
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
            ->add('confirmacion', 'checkbox', array(
                'label'     => sprintf('¿Estás seguro de eliminar al usuario %s?', $usuario->getDni()),
                'required'  => true,
                ))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if($data['confirmacion']) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($usuario);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("El usuario ha sido eliminado con éxito.")
                );

                return $this->redirect($this->generateUrl('usuario'));
            } else {
                $request->getSession()->getFlashBag()->add(
                    'warning',
                    sprintf("Debes marcar la casilla de confirmación para proceder a la eliminación del usuario con dni %s.", $usuario->getDni())
                );
            }
        }

        return $this->render(
            'Backend/usuario/eliminar.html.twig',
            array('form'	=> $form->createView())
        );

    }

    /**
     * @Route("/usuarios/{dni}/auto/eliminar", name="usuario_auto_eliminar")
     * @Security("has_role('ROLE_USER')")
     */
    public function autoEliminarAction(Request $request, Usuario $usuario)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('owner', $usuario)) {
            throw new AccessDeniedException('Acceso no autorizado!');
        }

        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
            ->add('confirmacion', 'checkbox', array(
                'label'     => sprintf('¿Estás de que quieres eliminar tu cuenta?'),
                'required'  => true,
            ))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if($data['confirmacion']) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($usuario);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("El usuario ha sido eliminado con éxito.")
                );

                return $this->redirect($this->generateUrl('backend'));
            } else {
                $request->getSession()->getFlashBag()->add(
                    'warning',
                    sprintf("Debes marcar la casilla de confirmación para proceder a la eliminación de tu cuenta.")
                );
            }
        }

        return $this->render(
            'Backend/usuario/auto_eliminar.html.twig',
            array('form'	=> $form->createView())
        );

    }

    /**
     * @Route("/usuarios/{id}/rol", name="usuario_rol")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function rolAction(Request $request, Usuario $usuario)
    {
        $form = $this->createForm(new UserRoleType(), $usuario);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Los roles del usuario han sido actualizados.")
            );

           return $this->redirect($this->generateUrl('usuario'));
        }

        return $this->render('Backend/usuario/role.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * @Route("/usuarios/numero/telefono", name="usuario_numero_telefono")
    * @Security("has_role('ROLE_USER')")
    */
    public function numeroTelefonoAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $me = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $this->getUser()->getUsername()));
        if($me->getTelefonoMovil()) {
            throw new BadRequestHttpException(sprintf("El usuario %s ya tiene configurado el número de teléfono.", $me->getDni()));
        }

        $telefonoMovil = new TelefonoMovil();
        $telefonoMovil->setUsuario($me);
        $me->setTelefonoMovil($telefonoMovil);
        $form = $this->createForm(new TelefonoMovilType(), $telefonoMovil);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($telefonoMovil);
            $em->flush();
            // Envío sms con el código de validación del número de teléfono
            $this->get('usuario')->enviarCodigo(
                $me->getTelefonoMovil(),
                array(
                    'texto_codigo_sms'      => SELF::TEXTO_CODIGO_SMS,
                    'flash_bag'             => $request->getSession()->getFlashBag(),
                    'mensaje_flash_ok'      => sprintf(SELF::MENSAJE_FLASH_USUARIO_CODIGO_SMS_OK),
                    'mensaje_flash_no_ok'   => sprintf(SELF::MENSAJE_FLASH_USUARIO_CODIGO_SMS_NO_OK)
                ));

            return $this->redirect($this->generateUrl('backend'));
        }

        return $this->render('Backend/usuario/numero_telefono.html.twig', array(
            'form'          => $form->createView(),
            'titulo_pagina' => 'Por favor, introduce tu número de teléfono',
        ));
    }

    /**
     * @Route("/usuarios/validar/telefono", name="usuario_validar_telefono")
     * @Security("has_role('ROLE_USER')")
     */
    public function validarTelefonoAction(Request $request)
    {
        $defaultData = array('message' => 'Escribe el código');
        $form = $this->createFormBuilder($defaultData)
            ->setAction($this->generateUrl('usuario_validar_telefono'))
            ->add('codigo', 'text')
            ->getForm();

        $em = $this->getDoctrine()->getManager();
        $me = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $this->getUser()->getUsername()));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if($me->getTelefonoMovil()->getCodigo() == $data['codigo']) {
                $me->getTelefonoMovil()->setValidado(true);
                $em = $this->getDoctrine()->getManager();
                $em->persist($me);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("Tu número de teléfono móvil ha sido validado.")
                );
            }
            else {
                $request->getSession()->getFlashBag()->add(
                    'danger',
                    sprintf("El código que has introducido no es correcto")
                );
            }

            return $this->redirect($this->generateUrl('backend'));

        }

        return $this->render(
            'Backend/usuario/validar_telefono.html.twig',
            array('form'	=> $form->createView())
        );
    }

}
