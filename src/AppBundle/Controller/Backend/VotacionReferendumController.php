<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\VotacionReferendum;
use AppBundle\Form\VotacionReferendumType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * EventoElectoral controller.
 *
 * @Route("/votacion/referendum")
 * @Security("has_role('ROLE_ADMIN')")
 */
class VotacionReferendumController extends Controller
{


    /**
     * Creates a form to create a VotacionReferendum entity.
     *
     * @param VotacionReferendum $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(VotacionReferendum $entity)
    {
        $form = $this->createForm(new VotacionReferendumType(), $entity, array(
            'action' => $this->generateUrl('votacion_referendum_new', array('evento_electoral_id' => $entity->getEventoElectoral()->getId() )),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new VotacionReferendum entity.
     *
     * @Route("/{evento_electoral_id}/new", name="votacion_referendum_new")
     * @Method({"GET","POST"})
     * @Template("Backend/votacion/new.html.twig")
     */
    public function newAction(Request $request, $evento_electoral_id)
    {
        $em = $this->getDoctrine()->getManager();

        $eventoElectoral = $em->getRepository('AppBundle:EventoElectoral')->find($evento_electoral_id);
        if (!$eventoElectoral) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $eventoElectoral)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $entity = new VotacionReferendum();
        $entity->setEventoElectoral($eventoElectoral);
        $form = $this->createCreateForm($entity);
        //$form->get('eventoElectoral')->setData($eventoElectoral);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'eventoelectoral_show',
                array('id'  => $eventoElectoral->getId())
            ));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to edit a EventoElectoral entity.
     *
     * @param EventoElectoral $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(VotacionReferendum $entity)
    {
        $form = $this->createForm(new VotacionReferendumType(), $entity, array(
            'action' => $this->generateUrl('votacion_referendum_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * Displays a form to edit an existing EventoElectoral entity.
     *
     * @Route("/{id}/edit", name="votacion_referendum_edit")
     * @Method({"GET","PUT"})
     * @Template("Backend/votacion/edit.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:VotacionReferendum')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha actualizado la votación: %s.", $entity->getNombre())
            );

            return $this->redirect($this->generateUrl('votacion_referendum_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Deletes a EventoElectoral entity.
     *
     * @Route("/{id}", name="votacion_referendum_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:VotacionReferendum')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $eventoElectoralId = $entity->getEventoElectoral()->getId();

        if ($form->isValid()) {
            $em->remove($entity);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha eliminado la votación")
            );

        }

        return $this->redirect($this->generateUrl('eventoelectoral_show', array('id' => $eventoElectoralId)));
    }

    /**
     * Deletes a EventoElectoral entity.
     *
     * @Route("/{id}/predelete", name="votacion_referendum_predelete")
     * @Method("GET")
     * @Template("Backend/votacion/delete.html.twig")
     */
    public function preDeleteAction($id)
    {
        $form = $this->createDeleteForm($id);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:VotacionReferendum')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        return array(
            'entity'        => $entity,
            'delete_form'   => $form->createView(),
        );

    }

    /**
     * Creates a form to delete a EventoElectoral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('votacion_referendum_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Sí, eliminar', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
            ;
    }
}
