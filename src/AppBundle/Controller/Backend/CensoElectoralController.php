<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormError;

/**
 * CensoElectoral controller.
 *
 * @Route("/censo-electoral")
 * @Security("has_role('ROLE_ADMIN')")
 */
class CensoElectoralController extends Controller
{

    private function createLoadForm(Votacion $entity)
    {


    }

    /**
     * @Route("/{id}/load", name="censoelectoral_load")
     * @Method({"GET","POST"})
     * @Template("Backend/censoElectoral/load.html.twig")
     */
    public function loadAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Votacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votacion entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $defaultData = array('message' => 'Copia y pega el texto');
        $form = $this->createFormBuilder($defaultData)
            ->add('texto', 'textarea', array(
                'constraints' => array(
                    new NotBlank()),
                'label' => "Copia y pega el censo electoral",
                'required' => true,
                'attr'  => array(
                    'class'    => 'form-control',
                    'style'    => ' height: 600px;')
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            $censoElectoralTool = $this->get('censo_electoral');

            $censoElectoralTool->setTexto($data['texto']);
            $censoElectoralTool->setVotacion($entity);

            if($censoElectoralTool->load()) {
                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("Se ha cargado el censo electoral")
                );
            } else {
                $error = new FormError($censoElectoralTool->getMensajeError());
                $form->get('texto')->addError($error);
            }

        }

        return array(
            'entity'        => $entity,
            'form'          => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/clean", name="censoelectoral_clean")
     * @Method({"GET","POST"})
     * @Template("Backend/censoElectoral/clean.html.twig")
     */
    public function cleanAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Votacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votacion entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $defaultData = array('message' => 'Copia y pega el texto');
        $form = $this->createFormBuilder($defaultData)
            ->add('confirmar', 'checkbox', array(
                'constraints' => array(
                    new NotBlank()),
                'label' => "Vaciar el censo electoral.",
                'required' => true,
                'attr'  => array(
                    //'class'    => 'form-control',
                    //'style'    => ' height: 600px;'
                    )
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('censo_electoral')->clean($entity);
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha eliminado el censo electoral")
            );
            return $this->redirect($this->generateUrl('eventoelectoral_show', array('id' => $entity->getEventoElectoral()->getId())));

        }

        return array(
            'entity'        => $entity,
            'form'          => $form->createView(),
        );
    }

}
