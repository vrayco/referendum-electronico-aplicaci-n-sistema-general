<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Form\EventoElectoralAbrirType;
use AppBundle\Form\EventoElectoralCerrarType;
use AppBundle\Form\EventoElectoralConvocarType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Form\EventoElectoralType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * EventoElectoral controller.
 *
 * @Route("/eventoelectoral")
 * @Security("has_role('ROLE_ADMIN')")
 */
class EventoElectoralController extends Controller
{
    const PREFIX_CLAVE_RSA = "EVENTO_%d_";

    /**
     * Lists all EventoElectoral entities.
     *
     * @Route("/", name="eventoelectoral")
     * @Method("GET")
     * @Template(":Backend/eventoElectoral:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $me = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $this->getUser()->getUsername()));

        $entities = $em->getRepository('AppBundle:EventoElectoral')->findAllByUser($me);

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a form to create a EventoElectoral entity.
     *
     * @param EventoElectoral $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventoElectoral $entity)
    {
        $form = $this->createForm(new EventoElectoralType(), $entity, array(
            'action' => $this->generateUrl('eventoelectoral_new'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new EventoElectoral entity.
     *
     * @Route("/new", name="eventoelectoral_new")
     * @Method({"GET","POST"})
     * @Template("Backend/eventoElectoral/new.html.twig")
     */
    public function newAction(Request $request)
    {
        $entity = new EventoElectoral();
        $form   = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // El usuario que crea el evento es siempre administrador del mismo
            $me = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>$this->getUser()->getUsername()));
            $entity->addAdministrador($me);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'eventoelectoral_show',
                array('id'  => $entity->getId())
            ));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a EventoElectoral entity.
     *
     * @Route("/{id}", name="eventoelectoral_show")
     * @Method("GET")
     * @Template("Backend/eventoElectoral/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('view', $entity)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $deleteForm = $this->createDeleteForm($id);

        $censo = $em->getRepository('AppBundle:EventoElectoral')->getCensoElectoral($entity);

        return array(
            'entity'        => $entity,
            'delete_form'   => $deleteForm->createView(),
            'censo'         => $censo,
        );
    }

    /**
     * Displays a form to edit an existing EventoElectoral entity.
     *
     * @Route("/{id}/edit", name="eventoelectoral_edit")
     * @Method({"GET","PUT"})
     * @Template("Backend/eventoElectoral/edit.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha actualizado el evento Electoral: %s.", $entity->getNombre())
            );

            return $this->redirect($this->generateUrl('eventoelectoral_edit', array('id' => $id)));
        }


        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a EventoElectoral entity.
     *
     * @param EventoElectoral $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventoElectoral $entity)
    {
        $form = $this->createForm(new EventoElectoralType(), $entity, array(
            'action' => $this->generateUrl('eventoelectoral_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * @Route("/{id}/convocar", name="eventoelectoral_convocar")
     * @Method({"GET","PUT"})
     * @Template("Backend/eventoElectoral/convocar.html.twig")
     */
    public function convocarAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $form = $this->createConvocarForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            if($data->getEstado() == EventoElectoral::ESTADO_CONVOCADO) {
                // Generamos las claves RSA del evento electoral
                $resultadoRSA = $this->get('rsa')->generarClaves(
                    sprintf(self::PREFIX_CLAVE_RSA, $entity->getId())
                );
                if ($resultadoRSA) {

                    // Informamos al Sistema Electoral de la convocatoria
                    if ($this->get('api_sistema_electoral')->convocarEventoElectoral($entity)) {
                        $entity->setConvocado(new \DateTime('now'));
                        $em->flush();
                        $request->getSession()->getFlashBag()->add(
                            'success',
                            sprintf("Se ha convocado el evento Electoral: %s. Ya es oficial y no es editable.", $entity->getNombre())
                        );

                        $logger = $this->get('monolog.logger.auditoria');
                        $logger->info(
                            sprintf('[EVENTO %s] Evento electoral CONVOCADO.', $entity->getId())
                        );

                        return $this->redirect($this->generateUrl('eventoelectoral_show', array('id' => $id)));

                    } else {
                        $request->getSession()->getFlashBag()->add(
                            'danger',
                            sprintf("Se ha producido un error al convocar el evento electoral en el Sistema Electoral.")
                        );
                    }
                } else {
                    $request->getSession()->getFlashBag()->add(
                        'danger',
                        sprintf("Se ha producido un error generando las claves RSA del evento electoral.")
                    );
                }
            } else {
                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("No se ha realizado ninguna accción.")
                );
            }
        }

        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }

    private function createAbrirForm(EventoElectoral $entity)
    {
        $form = $this->createForm(new EventoElectoralAbrirType(), $entity, array(
            'action' => $this->generateUrl('eventoelectoral_abrir', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * @Route("/{id}/abrir", name="eventoelectoral_abrir")
     * @Method({"GET","PUT"})
     * @Template("Backend/eventoElectoral/abrir.html.twig")
     */
    public function abrirAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('abrir', $entity)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $form = $this->createAbrirForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            if($data->getEstado() == EventoElectoral::ESTADO_ABIERTO) {
                // Informamos al Sistema Electoral de que se abre el proceso electoral
                if ($this->get('api_sistema_electoral')->abrirProcesoElectoral($entity)) {
                    $entity->setInicio(new \DateTime('now'));
                    $em->flush();

                    $request->getSession()->getFlashBag()->add(
                        'success',
                        sprintf("Se ha abierto el proceso Electoral: %s. Ya es oficial y no es editable.", $entity->getNombre())
                    );

                    $logger = $this->get('monolog.logger.auditoria');
                    $logger->info(
                        sprintf('[EVENTO %s] Evento electoral ABIERTO.', $entity->getId())
                    );

                    return $this->redirect($this->generateUrl('eventoelectoral_show', array('id' => $id)));

                } else {
                    $request->getSession()->getFlashBag()->add(
                        'danger',
                        sprintf("Se ha producido un error al abrir el proceso electoral en el Sistema Electoral.")
                    );
                }
            } else {
                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("No se ha realizado ninguna accción.")
                );
            }
        }

        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }

    private function createCerrarForm(EventoElectoral $entity)
    {
        $form = $this->createForm(new EventoElectoralCerrarType(), $entity, array(
            'action' => $this->generateUrl('eventoelectoral_cerrar', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * @Route("/{id}/cerrar", name="eventoelectoral_cerrar")
     * @Method({"GET","PUT"})
     * @Template("Backend/eventoElectoral/cerrar.html.twig")
     */
    public function cerrarAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('cerrar', $entity)) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $form = $this->createCerrarForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            if($data->getEstado() == EventoElectoral::ESTADO_CERRADO) {
                // Informamos al Sistema Electoral de que se cierra el proceso electoral
                if ($this->get('api_sistema_electoral')->cerrarProcesoElectoral($entity)) {
                    $entity->setFin(new \DateTime('now'));
                    $em->flush();
                    
                    $request->getSession()->getFlashBag()->add(
                        'success',
                        sprintf("Se ha cerrado el proceso Electoral: %s.", $entity->getNombre())
                    );

                    $logger = $this->get('monolog.logger.auditoria');
                    $logger->info(
                        sprintf('[EVENTO %s] Evento electoral CERRADO.', $entity->getId())
                    );

                    return $this->redirect($this->generateUrl('eventoelectoral_show', array('id' => $id)));

                } else {
                    $request->getSession()->getFlashBag()->add(
                        'danger',
                        sprintf("Se ha producido un error al cerrar el proceso electoral en el Sistema Electoral.")
                    );
                }
            } else {
                $request->getSession()->getFlashBag()->add(
                    'success',
                    sprintf("No se ha realizado ninguna accción.")
                );
            }
        }

        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }    

    private function createConvocarForm(EventoElectoral $entity)
    {
        $form = $this->createForm(new EventoElectoralConvocarType(), $entity, array(
            'action' => $this->generateUrl('eventoelectoral_convocar', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * Deletes a EventoElectoral entity.
     *
     * @Route("/{id}", name="eventoelectoral_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:EventoElectoral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EventoElectoral entity.');
            }

            if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity)) {
                throw new AccessDeniedException('Unauthorised access!');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('eventoelectoral'));
    }

    /**
     * Creates a form to delete a EventoElectoral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('eventoelectoral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Sí, eliminar', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
            ;
    }
}
