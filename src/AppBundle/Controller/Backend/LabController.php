<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\CensoElectoral;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\VotacionListasAbiertas;
use AppBundle\Entity\VotacionListasCerradas;
use AppBundle\Entity\VotacionReferendum;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Sinner\Phpseclib\Crypt\Crypt_RSA as RSA;

class LabController extends Controller
{
    /**
     * @Route("/lab", name="lab")
     * @Security("has_role('ROLE_USER')")
     */
    public function labAction()
    {
        return $this->render('Backend/lab.html.twig', array(

        ));
    }

    /**
     * @Route("/lab/gcm", name="lab_gcm")
     */
    public function GcmAction(Request $request)
    {
        $data = array("data" => "hola gcm");
        $registrationIds = array("APA91bE28FQKCPA79Xs1w6P0XBiBLg0_wZWc0lL3bOw3boYJWKIhmRDT1MdOLfN23A7LgR3GcFWhFWdMgbqvkX6Iqc4W4uT-Qz3x_3YXxUdqohpY0ChLAJDPfClc810xSGU6tTMNLj3oXN6H4c8c6P5MIX6SNMNN7g");

        $push = $this->get('push_notification');
        $push->setPlataforma("ANDROID");
        $push->setRegistrationIds($registrationIds);
        $push->setData($data);
        $resultado = $push->send();

        if($resultado->success == "1") {
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha enviado el mensaje gcm.")
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'danger',
                sprintf("No se ha enviado el mensaje gcm. Motivo: %s.", $resultado->results[0]->error)
            );
        }

        return $this->render('Backend/lab.html.twig');
    }

    private function encryptData($dataToEncrypt) {

        // Will hold the encrypted data
        $pubKey =   "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqp4FF9vZWjU1zhw5lb9XTKk/aL7VD0vw\n1AJVw1rGvM7UMFVyeGY/FovSb1YpuGErMWg99zksPDrixP/FxNhA5igA5JuAdGROpDdwyumregf0\nVgb+29eiS7aKLhK5MATrg9NLi65Pg3YPvwsY50T6JVb1KD/hNGJ+oHpckz8x7eN241I4W0wNVVzi\nhMPjCth29l134nRvQlI37pgR0bJSxBhDy7lSuOL3DipEF0JJdvmby/+XEeWe8h6qGQT0m5YCWf73\nab9WSEZjIbUK8s9m/EGKsutvjiNl+aquzPn2pNovMbtLHbCkoCs89Eg92bDentH7X5Y35ltJsfev\nmqLo3wIDAQAB\n-----END PUBLIC KEY-----";//openssl_pkey_get_public( file_get_contents( "public.pem" ) );
        $PK = openssl_get_publickey($pubKey);
        openssl_public_encrypt($dataToEncrypt,$secret,$PK);

        return base64_encode($secret);
    }


    /**
     * @Route("/lab/rsa", name="lab_rsa")
     */
    public function MensajeRSAAction(Request $request)
    {
        // Obtengo la clave publica RSA
        $me = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $rayco = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $me->getUsername()));
        $app = $rayco->getApp();

        if(!$app)
            throw new NotFoundHttpException("No se ha validado la aplicación móvil");


        $rsa = $this->get("rsa");
        $ciphertext = $rsa->publicEncrypt("Hola Holita RSA service!Hola Holita RSA service!Hola Holita RSA service!Hola Holita RSA service!Hola Holita RSA service!Hola Holita RSA service! Hola Holita RSA service!", $app->getPublicKey());

        // Envío mensaje GCM
        $data = array("data" => array("cifrado" => array("mensaje" => $ciphertext)));
        $registrationIds = array("APA91bE28FQKCPA79Xs1w6P0XBiBLg0_wZWc0lL3bOw3boYJWKIhmRDT1MdOLfN23A7LgR3GcFWhFWdMgbqvkX6Iqc4W4uT-Qz3x_3YXxUdqohpY0ChLAJDPfClc810xSGU6tTMNLj3oXN6H4c8c6P5MIX6SNMNN7g");

        $push = $this->get('push_notification');
        $push->setPlataforma("ANDROID");
        $push->setRegistrationIds($registrationIds);
        $push->setData($data);
        $resultado = $push->send();

        if($resultado->success == "1") {
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha enviado el mensaje gcm.")
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'danger',
                sprintf("No se ha enviado el mensaje gcm. Motivo: %s.", $resultado->results[0]->error)
            );
        }

        return $this->render('Backend/lab.html.twig');
    }

    /**
     * @Route("/lab/rsa2", name="lab_rsa2")
     */
    public function MensajeRSA2Action(Request $request) {
        $me = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $rayco = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $me->getUsername()));
        $app = $rayco->getApp();

        if(!$app)
            throw new NotFoundHttpException("No se ha validado la aplicación móvil");

        $rsa = $this->get("rsa");

        // Genero un par de claves
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];

        $data = '¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ¡Hola holita RSA, cifrado con pubKey del server! ';

        // Cifro mensaje con key privada (usando servicio RSA)
        $rsa = $this->get('rsa');
        $encrypted2 = $rsa->privateEncrypt($data, $privKey);
        dump($encrypted2);


        // TEST Descifro con clave publica del server
        $decrypted2 = $rsa->publicDecrypt($encrypted2, $pubKey);
        dump($decrypted2);

        // Envio mensaje cifrado y clave pública

        // Envío mensaje GCM
        $data = array("data" => array(
            "cifrado2" => array(
                "mensaje"   => $encrypted2,
                "pubKey"    => $pubKey
            )));
        $registrationIds = array("APA91bE28FQKCPA79Xs1w6P0XBiBLg0_wZWc0lL3bOw3boYJWKIhmRDT1MdOLfN23A7LgR3GcFWhFWdMgbqvkX6Iqc4W4uT-Qz3x_3YXxUdqohpY0ChLAJDPfClc810xSGU6tTMNLj3oXN6H4c8c6P5MIX6SNMNN7g");

        $push = $this->get('push_notification');
        $push->setPlataforma("ANDROID");
        $push->setRegistrationIds($registrationIds);
        $push->setData($data);
        $resultado = $push->send();

        if($resultado->success == "1") {
            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha enviado el mensaje gcm.")
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'danger',
                sprintf("No se ha enviado el mensaje gcm. Motivo: %s.", $resultado->results[0]->error)
            );
        }

        return $this->render('Backend/lab.html.twig');
    }

    /**
     * @Route("/lab/prueba/herencia", name="lab_prueba_herencia")
     */
    public function PruebaHerenciaAction() {
        $em = $this->getDoctrine()->getEntityManager();

        $evento = new EventoElectoral();
        $evento->setNombre("Elecciones 2015");
        $evento->setDescripcion("Bla Bla");
        $evento->setFecha(new \DateTime('now'));


        $lista = new VotacionListasCerradas();

        $lista->setDescripcion("bla bla bla");
        $lista->setNombre("Votacion listas cerradas");
        $lista->setMaximoOpciones(0);
        $lista->setMinimoOpciones(0);
        $lista->setEventoElectoral($evento);

        $lista2 = new VotacionListasAbiertas();

        $lista2->setDescripcion("bla bla bla");
        $lista2->setNombre("Votacion listas abiertas");
        $lista2->setMaximoOpciones(0);
        $lista2->setMinimoOpciones(0);
        $lista2->setEventoElectoral($evento);

        $lista3 = new VotacionReferendum();

        $lista3->setDescripcion("bla bla bla");
        $lista3->setNombre("Votacion Referendum");
        $lista3->setMaximoOpciones(0);
        $lista3->setMinimoOpciones(0);
        $lista3->setEventoElectoral($evento);

        $em->persist($lista);
        $em->persist($lista2);
        $em->persist($lista3);
        $evento->addVotacion($lista);
        $evento->addVotacion($lista2);
        $evento->addVotacion($lista3);
        $em->persist($evento);
        $em->flush();

        dump($lista);
        dump($lista2);
        dump($lista3);
        dump($evento);

        dump($evento->getVotacion());


        for($i = 0; $i < 100; $i++) {
            $censo = new CensoElectoral();

            $censo->setDni($i);
            $censo->setNombreCompleto("Nombre ".$i." Apellidos Apellidos");
            $censo->addVotacion($lista);
            $censo->addVotacion($lista2);
            $censo->addVotacion($lista3);

            $em->persist($censo);
        }

        $em->flush();


    }
}