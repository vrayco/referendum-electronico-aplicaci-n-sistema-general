<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Candidato;
use AppBundle\Entity\Lista;
use AppBundle\Form\ListaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 *
 * @Route("/lista")
 * @Security("has_role('ROLE_ADMIN')")
 *
 */
class ListaController extends Controller
{

    /**
     * @Route("/{id}/show", name="lista_index")
     * @Method({"GET","PUT"})
     * @Template("Backend/lista/index.html.twig")
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Votacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votacion entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('view', $entity->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $className = $em->getClassMetadata(get_class($entity))->getName();
        if($className == "AppBundle\\Entity\\VotacionReferendum")
            throw $this->createNotFoundException('Las votaciones de referendum no tienen listas asociadas.');

        $entities = $em->getRepository('AppBundle:Lista')->getListas($entity);

        return array(
            'entity'    => $entity,
            'entities'  => $entities,
        );
    }

    /**
     * Creates a form to create a Lista entity.
     *
     * @param Lista $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Lista $entity, $votacion_id, $seccionLista)
    {
        $form = $this->createForm(new ListaType(), $entity, array(
            'action'        => $this->generateUrl('lista_new', array('id'=>$votacion_id)),
            'method'        => 'POST',
            'seccion_lista' => $seccionLista            // si es true, muestra la sección para el candidato. Si es false siempre es titular
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Lista entity.
     *
     * @Route("/{id}/new", name="lista_new")
     * @Method({"GET","POST"})
     * @Template("Backend/lista/new.html.twig")
     */
    public function newAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $votacion = $em->getRepository('AppBundle:Votacion')->find($id);

        if (!$votacion) {
            throw $this->createNotFoundException('Unable to find Votacion entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $votacion->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $entity = new Lista();
        $seccionLista = null;
        $className = $em->getClassMetadata(get_class($votacion))->getName();
        if($className == "AppBundle\\Entity\\VotacionListasCerradas") {
            $entity->setVotacionListasCerradas($votacion);
            $seccionLista = true;
        }
        elseif($className == "AppBundle\\Entity\\VotacionListasAbiertas") {
            $entity->setVotacionListasAbiertas($votacion);
            $seccionLista = false;
        }

        $form   = $this->createCreateForm($entity, $votacion->getId(), $seccionLista);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'lista_index',
                array('id'  => $votacion->getId())
            ));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EventoElectoral entity.
     *
     * @Route("/{id}/edit", name="lista_edit")
     * @Method({"GET","PUT"})
     * @Template("Backend/lista/edit.html.twig")
     */
    public function editAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Lista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lista entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getVotacion()->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                sprintf("Se ha actualizado la lista: %s.", $entity->getNombreCompleto())
            );

            return $this->redirect($this->generateUrl('lista_edit', array('id' => $id)));
        }


        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     *
     * @param Lista $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Lista $entity)
    {
        $form = $this->createForm(new ListaType(), $entity, array(
            'action' => $this->generateUrl('lista_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     *
     * @Route("/{id}", name="lista_show")
     * @Method("GET")
     * @Template("Backend/lista/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Lista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lista entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('view', $entity->getVotacion()->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        $candidatos = $em->getRepository('AppBundle:Candidato')->getCandidatos($entity, 'titular');
        $suplentes = $em->getRepository('AppBundle:Candidato')->getCandidatos($entity, 'suplente');

        return array(
            'entity'                => $entity,
            'candidatos'            => $candidatos,
            'candidatosSuplentes'   => $suplentes,
        );
    }

    /**
     * @Route("/{id}", name="lista_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $votacion_id = null;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Lista')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Lista entity.');
            }

            if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getVotacion()->getEventoElectoral())) {
                throw new AccessDeniedException('Unauthorised access!');
            }

            $votacion_id = $entity->getVotacion()->getId();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('lista_index', array('id' => $votacion_id)));
    }

    /**
     * @Route("/{id}/predelete", name="lista_predelete")
     * @Method("GET")
     * @Template("Backend/lista/delete.html.twig")
     */
    public function preDeleteAction($id)
    {
        $form = $this->createDeleteForm($id);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Lista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lista entity.');
        }

        if (false === $this->get('security.authorization_checker')->isGranted('edit', $entity->getVotacion()->getEventoElectoral())) {
            throw new AccessDeniedException('Unauthorised access!');
        }

        return array(
            'entity'        => $entity,
            'delete_form'   => $form->createView(),
        );

    }

    /**
     * Creates a form to delete a EventoElectoral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lista_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Sí, eliminar', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
            ;
    }
}
