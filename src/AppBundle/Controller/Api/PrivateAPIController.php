<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\ElectorValidacion;
use AppBundle\Entity\EventoElectoral;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class PrivateAPIController extends FOSRestController
{
    /**
     *
     *
     * @ApiDoc(
     * description = "Validación de CV del elector.",
     * statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors",
     *     409 = "Fallo el envio del código de validación por GCM"
     * }
     * )
     *
     * @Post("/validar/cv")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="cv", nullable=false, strict=true, description="CV del elector")
     * @RequestParam(name="l1", nullable=false, strict=true, description="L1")
     *
     *
     * @return View
     */
    public function validarCVAction(ParamFetcher $paramFetcher)
    {

        $cv = $paramFetcher->get('cv');
        $l1 = $paramFetcher->get('l1');

        $em = $this->getDoctrine()->getManager();

        // Compruebo que CV existe
        $elector = $em->getRepository('AppBundle:Elector')->findOneBy(array('tokenVotacion' => $cv));
        if(!$elector) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("No existe el elector en el sistema.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que el proceso electoral está abierto
        if($elector->getEventoElectoral()->getEstado() !== EventoElectoral::ESTADO_ABIERTO) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El proceso electoral no está abierto.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que el elector esta registrado en el sistema
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $elector->getDni()));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("No existe el usuario en el sistema.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que el elector esta correctamente validado
        if(!$usuario->isValid()) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El usuario no está correctamente validado.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $electorValidacion = new ElectorValidacion();
        $electorValidacion->setElector($elector);
        $electorValidacion->setL1($l1);

        // Envio el codigo de validación y L1 por GCM
        $codigoValidacionCifrado = $this->get('rsa')->publicEncrypt($electorValidacion->getCodigoValidacion(), $usuario->getApp()->getClavePublica()->getClavePublica());
        $data = array(
            'data' => array(
                'validar_voto'  => array(
                    'codigo_validacion' => $codigoValidacionCifrado,
                    'l1'                => $l1
                )
            )
        );

        $push = $this->get('push_notification');
        $push->setPlataforma($usuario->getApp()->getSistemaOperativo());
        $push->setRegistrationIds(array($usuario->getApp()->getRegistrationId()));
        $push->setData($data);
        $resultado = $push->send();

        if($resultado->success == "0") {
            $data = array(
                "code"      => Response::HTTP_CONFLICT,
                "message"   => sprintf('Problemas enviando codigo de validacion vía mensaje PUSH.')
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $em->persist($electorValidacion);
        $em->flush();

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf("[EVENTO %s][VOTO EN ESPERA DE VALIDACION] elector=%s    L1=%s", $elector->getEventoElectoral()->getId(), $electorValidacion->getElector()->getDni(), $l1)
        );

        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_OK);
        $response->setContent(json_encode(array(
            "mensaje"   => "¡Ok! Esperando a recibir el código de validación por parte del usuario.",
            "evento_id" => $elector->getEventoElectoral()->getId()
        )));

        return $response;
    }
}
