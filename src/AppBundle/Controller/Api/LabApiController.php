<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\App;
use AppBundle\Entity\SolicitudApp;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Util\StringUtils;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Sinner\Phpseclib\Crypt\Crypt_RSA as Crypt_RSA;


class LabApiController extends FOSRestController
{


    /**
     * Recibe un mensaje cifrado con la clave privada del dispositivo y lo descrifa con su pública.<br/>
     *
     * @ApiDoc(
     * description = "Recibe un mensaje cifrado con la clave privada del dispositivo y lo descrifa con su pública.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * 409 = "Se devuelve cuando no se pudo enviar el codigo 1 vía GCM"
     * }
     * )
     *
     * @Post("/mensaje_cifrado")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="dni", nullable=false, strict=true, description="Dni.")
     * @RequestParam(name="mensaje_cifrado", nullable=false, strict=true, description="Mensaje cifrado.")
     *
     * @return View
     */
    public function postMensajeCifradoAction(ParamFetcher $paramFetcher)
    {
        $data = array(
            'data' => array(
                'dni'               => $paramFetcher->get('dni'),
                'mensaje_cifrado'   => $paramFetcher->get('mensaje_cifrado')
            )
        );

        $view = View::create();
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $data['data']['dni']));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe usuario con dni %s en el sistema.', $data['data']['dni'])
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $app = $usuario->getApp();

        $mensaje_cifrado = $data['data']['mensaje_cifrado'];

        $rsa = $this->get("rsa");
        $mensaje = $rsa->publicDecrypt($mensaje_cifrado, $app->getPublicKey());

        // Enviamos el codigo via GCM
        $mensaje = array(
            "data" => array(
                "info" => array(
                    "mensaje" => $mensaje
                )
            )
        );

        $push = $this->get('push_notification');
        $push->setPlataforma($app->getSistemaOperativo());
        $push->setRegistrationIds(array($app->getRegistrationId()));
        $push->setData($mensaje);

        $resultado = $push->send();

        if($resultado->success == "0") { // Si no se consigue enviar el codigo1, no se guarda la solicitud
            $data = array(
                "code"      => Response::HTTP_CONFLICT,
                "message"   => sprintf('Problemas enviando codigo 1 vía mensaje PUSH. No se ha guardado la solicitud.')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $view->setData($mensaje)->setStatusCode(200);


        return $view;
    }

    /**
     * Recibe un mensaje cifrado con la clave pública del servidor y lo descrifa con la privada.<br/>
     *
     * @ApiDoc(
     * description = "Recibe un mensaje cifrado con la clave pública del servidor y lo descrifa con la privada.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * 409 = "Se devuelve cuando no se pudo enviar el codigo 1 vía GCM"
     * }
     * )
     *
     * @Post("/mensaje_cifrado2")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="mensaje_cifrado", nullable=false, strict=true, description="Mensaje cifrado.")
     *
     * @return View
     */
    public function postMensajeCifrado2Action(ParamFetcher $paramFetcher)
    {
        $data = array(
            'data' => array(
                'mensaje_cifrado'   => $paramFetcher->get('mensaje_cifrado')
            )
        );

        $view = View::create();
        $response = new JsonResponse();

        $mensaje_cifrado = "vQgUjkK\/Qg+PPUBeyQXxOlDhhVc6B7YyGBCpSN+68pK575STLeKqrLR8t5dyLgMBtPHXMCbYMkg2\nKsbDBpltypLaFWg3aDi0zLQ8EJ5fV9MMmZnSM6qHJUx96eSKeA3kdkSD18VAMVsNZSpbjBjaGbu8\nLC7xLkZV6Fl+Y++ZErc=";//$data['data']['mensaje_cifrado'];

        $str_privatekey =   "-----BEGIN RSA PRIVATE KEY-----\n".
            "MIICXQIBAAKBgQDYPkHLGUcEHrZQqBZdqvIA6PEn3GHksmMVTNPTUmKQqQVT3qk8\n".
            "4lKvkE5wYgbh0fgA0rNiRmXtS2tbbpkFktlPQ1bTuIHmr0ZObfN3rZ2HeK4FexFm\n".
            "+r1QniIZuwbTfHkGS9OAYGfS64JWt2kzYBNqryYQf3Ra/HmsD58edghdCwIDAQAB\n".
            "AoGBANSzECZE28yapPvbCfziW543gpWRuZypNrrJ0gWnoJnoFvxzJIZlm61ixTTX\n".
            "xysyS74LgjTlR/bL23KY691Gn4X/o/Vx/Dcpk524zETvaBYf993oEZ7wqoIf2NYz\n".
            "Y7tD6GWW3eaNvXt/6CxEJASDKUp0sEmaKqRFZfvlYOm/bJMxAkEA7/P0qeiXWw88\n".
            "VqF5r1iv9YMP+C7Mx5YQe9TQ95aWzKGHptGLR/NAzyeMSDDOJTkH5cZAK0axf0PT\n".
            "676xFOPPkwJBAOa0YqEg0CFHze43uSgSBv8jlLY5QWOf90+3ragYtCTldKnpBCLb\n".
            "ILq898LjzgXnp8mbCkeeyC7SBnasqEOdd6kCQAVmhTQpwvZOsnn0qmty4IrzBzH7\n".
            "w6oeS4Kt+IIKBpzWadosTykwZV4EMtkRGCKoVPAr9RfEQIxvt15P7WZd9jcCQEn8\n".
            "BTk5BaX2mgLQGe/QwC+oIHPDrsWdMjl9ZRJoQbUnZJ9sm6f17+wS173HQnXAUZ2k\n".
            "u8o2EGp7bhk2y4tLf2kCQQCSHwwmGlSAvd1IVbPoHU2aGJwFZ0wTfVUFou1wH0pY\n".
            "O1KVyxrtAGNGOX70SNi2A0VOC6l4x5xPXxZpGPMm47PI\n".
            "-----END RSA PRIVATE KEY-----\n";

        $rsa = $this->get("rsa");
        $mensaje = $rsa->privateDecrypt($mensaje_cifrado, $str_privatekey);

        // Enviamos el codigo via GCM
        $mensaje = array(
            "data" => array(
                "info" => array(
                    "mensaje" => $mensaje
                )
            )
        );

        $push = $this->get('push_notification');
        $push->setPlataforma("ANDROID");
        $push->setRegistrationIds(array("APA91bE28FQKCPA79Xs1w6P0XBiBLg0_wZWc0lL3bOw3boYJWKIhmRDT1MdOLfN23A7LgR3GcFWhFWdMgbqvkX6Iqc4W4uT-Qz3x_3YXxUdqohpY0ChLAJDPfClc810xSGU6tTMNLj3oXN6H4c8c6P5MIX6SNMNN7g"));
        $push->setData($mensaje);

        $resultado = $push->send();

        if($resultado->success == "0") { // Si no se consigue enviar el codigo1, no se guarda la solicitud
            $data = array(
                "code"      => Response::HTTP_CONFLICT,
                "message"   => sprintf('PROBLEMA ENVIANDO MENSAJE GCM.')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $view->setData($mensaje)->setStatusCode(200);


        return $view;
    }


    /**
     * Pruebas de criptografía simétrica.<br/>
     *
     * @ApiDoc(
     * description = "Pruebas de criptografía simétrica.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * 409 = "Se devuelve cuando no se pudo enviar el codigo 1 vía GCM"
     * }
     * )
     *
     * @Post("/criptografia/simetrica")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="mensaje_cifrado", nullable=false, strict=true, description="Mensaje cifrado.")
     * @RequestParam(name="key", nullable=false, strict=true, description="key.")
     * @RequestParam(name="iv", nullable=false, strict=true, description="IV.")
     *
     * @return View
     */
    public function postCriptografiaSimetricaAction(ParamFetcher $paramFetcher)
    {
        $data = array(
            'data' => array(
                'key'       => $paramFetcher->get('key'),
                'iv'        => $paramFetcher->get('iv'),
                'mensaje'   => $paramFetcher->get('mensaje_cifrado')
            )
        );

//        $mensaje_cifrado = $this->get('aes')->encrypt($data['data']['mensaje'],$data['data']['key']);
//        $mensaje_descifrado = $this->get('aes')->decrypt($mensaje_cifrado,$data['data']['key']);

        $mcrypt = $this->get('mcrypt');
        $mcrypt->setKey($data['data']['key']);
        $mcrypt->setIV($data['data']['iv']);
        $mensaje_cifrado2 = $this->get('mcrypt')->encrypt($data['data']['mensaje']);
        $mensaje_descifrado2 = $this->get('mcrypt')->decrypt($mensaje_cifrado2);

        $mensaje_descifrado3 = $this->get('mcrypt')->decrypt($data['data']['mensaje']);
        
        $view = View::create();

        $result = array(
//            'mensaje_cifrado'       => $mensaje_cifrado,
//            'mensaje_descrifrado'   => $mensaje_descifrado,
            'mensaje_cifrado2'      => $mensaje_cifrado2,
            'mensaje_descrifrado2'  => $mensaje_descifrado2,
            'mensaje_descrifrado3'  => $mensaje_descifrado3
        );

        $view->setData($result)->setStatusCode(200);


        return $view;
    }
    

}