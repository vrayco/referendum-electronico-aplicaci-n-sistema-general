<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\Backend\EventoElectoralController;
use AppBundle\Entity\App;
use AppBundle\Entity\ClavePublica;
use AppBundle\Entity\ElectorValidacion;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\SolicitudApp;
use AppBundle\Entity\TokenConvocatoriaElectoral;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Util\StringUtils;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;


class PublicApiController extends FOSRestController
{

    const TEXTO_SMS_CODIGO2 = "CODIGO REFERENDUM-ELECTRONICO.COM=";   // Texto para el SMS que se le envía al telefóno

    /**
     * Inicia el proceso de validación de la aplicación para el usuario.<br/>
     *
     * @ApiDoc(
     * description = "Inicia el proceso de validación de la aplicación para el usuario.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * 409 = "Se devuelve cuando no se pudo enviar el codigo 1 vía GCM"
     * }
     * )
     *
     * @Post("/solicitud/app")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="dni", nullable=false, strict=true, description="Dni.")
     * @RequestParam(name="sistema_operativo", nullable=false, strict=true, description="Sistema Operativo del dispositivo.")
     * @RequestParam(name="registration_id", nullable=false, strict=true, description="Registration ID.")
     * @RequestParam(name="app_version", nullable=false, strict=true, description="Versión de la App.")
     *
     * @return View
     */
    public function postSolicitudAppAction(ParamFetcher $paramFetcher)
    {
        $data = array(
            'solicitudApp' => array(
                'dni'               => $paramFetcher->get('dni'),
                'sistema_operativo' => strtoupper($paramFetcher->get('sistema_operativo')),
                'registration_id'   => $paramFetcher->get('registration_id'),
                'app_version'       => $paramFetcher->get('app_version')
            )
        );

        $view = View::create();
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $data['solicitudApp']['dni']));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe usuario con dni %s en el sistema.', $data['solicitudApp']['dni'])
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if(!$usuario->isValidDni() || !$usuario->isValidTelefono()) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('El usuario con dni %s no tiene la cuenta validad (dni o número de teléfono).',$data['solicitudApp']['dni'])
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $entity = new SolicitudApp();
        $entity->setUsuario($usuario);
        $entity->setSistemaOperativo($data['solicitudApp']['sistema_operativo']);
        $entity->setRegistrationId($data['solicitudApp']['registration_id']);
        $entity->setAppVersion($data['solicitudApp']['app_version']);
        $em->persist($entity);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if(count($errors) > 0) {
            $error = "";
            if($errors[0])
                if($errors[0]->getMessage())
                    $error = (string) $errors[0]->getMessage();
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => $error
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Enviamos el codigo via GCM
        $mensaje = array(
            "data" => array(
                "registro" => array(
                    "codigo_1" => $entity->getCodigo1()
                )
            )
        );

        $push = $this->get('push_notification');
        $push->setPlataforma($data['solicitudApp']['sistema_operativo']);
        $push->setRegistrationIds(array($entity->getRegistrationId()));
        $push->setData($mensaje);
        $resultado = $push->send();

        if($resultado->success == "0") { // Si no se consigue enviar el codigo1, no se guarda la solicitud
            $data = array(
                "code"      => Response::HTTP_CONFLICT,
                "message"   => sprintf('Problemas enviando codigo 1 vía mensaje PUSH. No se ha guardado la solicitud.')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $em->flush($entity);
        $view->setData($entity)->setStatusCode(200);

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf('[REGISTRO APP - 1/3][%s] Solicita alta (SO=%s  RegistrationId=%s  AppVersion=%s)', $data['solicitudApp']['dni'], $data['solicitudApp']['sistema_operativo'], $data['solicitudApp']['registration_id'],  $data['solicitudApp']['app_version'])
        );

        return $view;
    }

    /**
     * Segundo paso del proceso de registro de la App. Vuelta del codigo1.<br/>
     *
     * @ApiDoc(
     * description = "Segundo paso del proceso de registro de la App. Vuelta del codigo1.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * 409 = "Se devuelve cuando no se pudo enviar el codigo 2 vía sms",
     * }
     * )
     *
     * @Post("/solicitud/app/codigo1")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="registration_id", nullable=false, strict=true, description="Registration ID.")
     * @RequestParam(name="codigo_1", nullable=false, strict=true, description="Código 1.")
     *
     * @return View
     */
    public function postValidacionCodigo1Action(ParamFetcher $paramFetcher)
    {

        $registration_id = $paramFetcher->get('registration_id');
        $codigo_1 = $paramFetcher->get('codigo_1');

        $view = View::create();
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();

        $solicitud = $em->getRepository('AppBundle:SolicitudApp')->findOneBy(array(
            'registrationId'    => $registration_id,
            'codigo1'           => $codigo_1
        ));

        if(!$solicitud) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe la solicitud en el sistema')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que no haya expirado la solicitud
        if($solicitud->getExpiracion() < new \DateTime('now')) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La solicitud ha expirado')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if(!StringUtils::equals($solicitud->getCodigo1(), $codigo_1)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('Código_1 "%s" incorrecto.', $codigo_1)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $solicitud->setValidadoCodigo1(new \DateTime('now'));

        $em->persist($solicitud);
        $em->flush();

        // Comienza la fase 2: Envío codigo2 via sms
        $telefono = $solicitud->getUsuario()->getTelefonoMovil();
        $envio = $this->get('bulk_sms')->sendSms(array(
            'numeroTelefono'	=> $telefono->getPrefijoPais().$telefono->getNumeroTelefono(),
            'mensaje'			=> SELF::TEXTO_SMS_CODIGO2.$solicitud->getCodigo2()
        ));

        if($envio) {
            $solicitud->setEnvioSmsCodigo(new \DateTime('now'));
            $em->persist($solicitud);
            $em->flush();
        } else {
            // Si no se consiguió realizar el envío, la app puede volver a realizar la misma petición al servidor.
            $data = array(
                "code"      => Response::HTTP_PRECONDITION_FAILED,
                "message"   => sprintf('Problemas enviando codigo 2 vía SMS. Vuelve a intentarlo.')
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $view->setData($solicitud)->setStatusCode(200);

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf('[REGISTRO APP - 2/3][%s] Recibido CODIGO_1. CODIGO_2 enviado por SMS.', $solicitud->getUsuario()->getDni())
        );

        return $view;
    }

    /**
     * Tercer paso del proceso de registro de la App. Vuelta del codigo2.<br/>
     *
     * @ApiDoc(
     * description = "Tercer paso del proceso de registro de la App. Vuelta del codigo2.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Post("/solicitud/app/codigo2")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="dni", nullable=false, strict=true, description="Dni.")
     * @RequestParam(name="codigo_2", nullable=false, strict=true, description="Código 2.")
     *
     * @return View
     */
    public function postValidacionCodigo2Action(ParamFetcher $paramFetcher)
    {
        $dni = $paramFetcher->get('dni');
        $codigo_2 = $paramFetcher->get('codigo_2');

        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $dni));

        $response = new JsonResponse();

        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe usuario con dni %s en el sistema.',$dni)
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $solicitud = $em->getRepository('AppBundle:SolicitudApp')->findOneBy(array(
            'usuario'           => $usuario,
            'codigo2'           => $codigo_2
        ));

        if(!$solicitud) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe la solicitud en el sistema')
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que no haya expirado la solicitud
        if($solicitud->getExpiracion() < new \DateTime('now')) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La solicitud ha expirado')
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que el codigo1 ha sido validado anteriormente
        if($solicitud->getValidadoCodigo1() === null) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La solicitud no ha valido el codigo 1')
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que el codigo2 no haya sido validado
        if($solicitud->getValidadoCodigo2() !== null) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La validación de la solicitud ya ha sido completada con anterioridad')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if(!StringUtils::equals($solicitud->getCodigo2(), $codigo_2)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('Código_2 "%s" incorrecto.', $codigo_2)
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $solicitud->setValidadoCodigo2(new \DateTime('now'));

        $em->persist($solicitud);
        $em->flush();

        // Si la validación se completa, y el usuario tenía una app previamente registrada, el nuevo registrationId será el válido y el anterior será descartado.
        $usuario = $solicitud->getUsuario();
        if($usuario->getApp() === null) {
            $entity = new App();
            $entity->setSolicitudApp($solicitud);
            $entity->setSistemaOperativo($solicitud->getSistemaOperativo());
            $entity->setRegistrationId($solicitud->getRegistrationId());
            $entity->setUsuario($solicitud->getUsuario());
            $solicitud->setApp($entity);
            $usuario->setApp($entity);
            $em->persist($solicitud);
            $em->persist($entity);
        } else {  // El usuario tenía una app previamente registrada, el nuevo registrationId será el válido y el anterior será descartado.
            $app = $usuario->getApp();

            if($app->getSolicitudApp()) { // Elimino la solicitud anterior
                $oldSolicitud = $usuario->getApp()->getSolicitudApp();
                $em->remove($oldSolicitud);
                $em->flush();
            }

            if($app->getClavePublica()) {  // Elimino la clave publica anterior
                $clavePublica = $app->getClavePublica();
                $app->setClavePublica(null);
                $em->remove($clavePublica);
                $em->flush($clavePublica);
            }

            $app->setSistemaOperativo($solicitud->getSistemaOperativo());
            $app->setRegistrationId($solicitud->getRegistrationId());
            $app->setSolicitudApp($solicitud);
            $solicitud->setApp($app);
            $em->persist($app);
            $em->persist($solicitud);
        }

        $em->flush();

        $view->setData($solicitud)->setStatusCode(200);

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf('[REGISTRO APP 3/3][%s] Alta de app finalizada correctamente.', $solicitud->getUsuario()->getDni())
        );

        return $view;
    }


    /**
     * Inicia el proceso para entregar la clave pública del dispositivo con el Sistema General.<br/>
     *
     * @ApiDoc(
     * description = "Inicia el proceso para compartir la clave pública del dispositivo con el Sistema General.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * 409 = "Se devuelve cuando no se pudo enviar el codigo 1 vía GCM"
     * }
     * )
     *
     * @Post("/clave-publica/entregar")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="dni", nullable=false, strict=true, description="Dni.")
     * @RequestParam(name="registration_id", nullable=false, strict=true, description="RegistrationId.")
     * @RequestParam(name="clave_publica", nullable=false, strict=true, description="Clave pública del dispositivo.")
     *
     * @return View
     */
    public function postEntregarClavePublicaPaso1Action(ParamFetcher $paramFetcher)
    {
        $dni = $paramFetcher->get('dni');
        $registrationId = $paramFetcher->get('registration_id');
        $publicKey = $paramFetcher->get('clave_publica');

        $view = View::create();
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $dni));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe usuario con dni %s en el sistema.', $dni)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if(!$usuario->isValidDni() || !$usuario->isValidTelefono()) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('El usuario con dni %s no tiene la cuenta validad (dni o número de teléfono).', $dni)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if(!$usuario->isValidApp($registrationId)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La aplicación móvil con registrationId=%s no está validada', $registrationId)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Valido la clave publica
        $rsa = $this->get("rsa");
        if(!$rsa->checkPublicKey($publicKey)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La clave pública no es válida: %s', $publicKey)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $app = $usuario->getApp();

        $entity = new ClavePublica();
        $entity->setClavePublica($publicKey);
        $entity->setUsuario($usuario);

        $em = $this->getDoctrine()->getManager();

        $em->persist($app);
        $em->persist($entity);
        $em->flush();

        // Cifro el codigo con la clave publica
        $codigoCifrado = $rsa->publicEncrypt($entity->getCodigo(), $entity->getClavePublica());

        // Enviamos el codigo via GCM
        $mensaje = array(
            "data" => array(
                "compartir_clave_publica" => array(
                    "codigo_cifrado" => $codigoCifrado
                )
            )
        );

        $push = $this->get('push_notification');
        $push->setPlataforma($app->getSistemaOperativo());
        $push->setRegistrationIds(array($app->getRegistrationId()));
        $push->setData($mensaje);
        $resultado = $push->send();

        if($resultado->success == "0") { // Si no se consigue enviar el codigo1, no se guarda la solicitud
            $data = array(
                "code"      => Response::HTTP_CONFLICT,
                "message"   => sprintf('Problemas enviando codigo 1 vía mensaje PUSH. No se ha guardado la solicitud.')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $em->flush($entity);
        $view->setData($entity)->setStatusCode(200);

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf('[COMPARTIR CLAVE PUBLICA 1/2][%s] La App entrego clave pública RSA.', $usuario->getDni())
        );

        return $view;
    }

    /**
     * Segundo paso del proceso de entrega de clave pública por parte del cliente.<br/>
     *
     * @ApiDoc(
     * description = "Segundo paso del proceso de entrega de clave pública por parte del cliente.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors",
     * }
     * )
     *
     * @Post("/clave-publica/confirmar")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="dni", nullable=false, strict=true, description="Dni.")
     * @RequestParam(name="registration_id", nullable=false, strict=true, description="RegistrationId.")
     * @RequestParam(name="codigo", nullable=false, strict=true, description="Código recibido en el paso 1.")
     *
     * @return View
     */
    public function postEntregarClavePublicaPaso2Action(ParamFetcher $paramFetcher)
    {

        $dni = $paramFetcher->get('dni');
        $registrationId = $paramFetcher->get('registration_id');
        $codigo = $paramFetcher->get('codigo');

        $view = View::create();
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array(
            'dni'    => $dni
        ));

        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe usuario con dni %s en el sistema.', $dni)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // El usuario tiene que tener el dni y el telefono (número y app) validados para poder confirmar la clave pública
        if(!$usuario->isValidDni() || !$usuario->isValidTelefono() || !$usuario->isValidApp($registrationId)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('El usuario con dni %s no tiene la cuenta validad (dni + número de teléfono + app).', $dni)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $clavePublica = $em->getRepository('AppBundle:ClavePublica')->findOneBy(array(
            'usuario'           => $usuario,
            'codigo'            => $codigo,
            'validadoCodigo'    => null
        ));

        if(!$clavePublica) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe la clave pública en el sistema')
            );
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo que no haya expirado la solicitud
        if($clavePublica->getExpiracion() < new \DateTime('now')) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('La clave publica ha expirado')
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if(!StringUtils::equals($clavePublica->getCodigo(), $codigo)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('Código "%s" incorrecto.', $codigo)
            );

            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Validamos clave pública y se la asignamos a la app, eliminamos si había una anterior
        $clavePublica->setValidadoCodigo(new \DateTime('now'));

        $app = $usuario->getApp();

        if($app->getClavePublica()) {
            $clavePublicaVieja = $app->getClavePublica();
            $app->setClavePublica(null);
            $em->remove($clavePublicaVieja);
        }
        $app->setClavePublica($clavePublica);
        $clavePublica->setApp($app);
        $em->persist($clavePublica);
        $em->persist($app);

        $em->flush();

        $view->setData($clavePublica)->setStatusCode(200);

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf('[COMPARTIR CLAVE PUBLICA 2/2][%s] Clave pública RSA validada.', $usuario->getDni())
        );

        return $view;
    }

    /**
     * Devuelve el estado de la validación del usuario.<br/>
     *
     * @ApiDoc(
     * description = "Devuelve el estado de la validación del usuario.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Get("/validacion/usuario/{dni}/estado")
     *
     * @param Request $request the request object
     * @param string     $dni      Dni
     *
     *
     * @return View
     */
    public function getValidacionUsuarioEstadoAction($dni) {

        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $dni));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_NOT_FOUND,
                "message"   => sprintf("No existe usuario con dni %s en el sistema.", $dni)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        $usuarioTool = $this->get('usuario');
        $data = $usuarioTool->getValidacionUsuario($usuario);

        $view->setData($data)->setStatusCode(200);

        return $view;
    }

    /**
     * Devuelve el estado de la validación de la app del usuario.<br/>
     *
     * @ApiDoc(
     * description = "Devuelve el estado de la validación de la app del usuario.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Get("/validacion/app/{dni}/{registrationId}/estado")
     *
     * @param Request $request the request object
     * @param string     $dni      Dni
     *
     *
     * @return View
     */
    public function getValidacionAppEstadoAction($dni, $registrationId) {

        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $dni));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_NOT_FOUND,
                "message"   => sprintf("No existe usuario con dni %s en el sistema.", $dni)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        $usuarioTool = $this->get('usuario');
        $data = $usuarioTool->getValidacionAplicacion($usuario, $registrationId);

        $view->setData($data)->setStatusCode(200);

        return $view;
    }

    /**
     * Devuelve la clave pública RSA del sistema.<br/>
     *
     * @ApiDoc(
     * description = "Devuelve la clave pública RSA del sistema.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Get("/clave-publica/servidor")
     *
     * @param Request $request the request object
     * @param string     $dni      Dni
     *
     *
     * @return View
     */
    public function getClavePublicaAction() {

        $view = View::create();

        $data = array(
            'public_key' => $this->get('rsa')->getPublicKey()
        );

        $view->setData($data)->setStatusCode(200);

        return $view;
    }

    /**
     * Devuelve las convocatorias electorales
     *
     * @ApiDoc(
     * description = "Devuelve las convocatorias electorales.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Devuelto cuando el usuario o la aplicación no están correctamente válidadas",
     * 404 = "No Existe el usuario en el sistema"
     * }
     * )
     *
     * @Get("/evento-electoral/convocatorias/{dni}/electorales")
     *
     * @return View
     */
    public function getConvocatoriasElectoralesAction($dni) {

        $view = View::create();
        $em = $this->getDoctrine()->getManager();

        // Compruebo que el usuario existe
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $dni));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_NOT_FOUND,
                "message"   => sprintf("No existe usuario con dni %s en el sistema.", $dni)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo si es usuario valido
        $usuarioTool = $this->get("usuario");
        if(!$usuarioTool->checkValidacionUsuario($usuario)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El usuario con el dni %s no está validado correctamente en el sistema.", $dni)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        // Compruebo si la aplicación del usuario es valida
        if(!$usuarioTool->checkValidacionAplicacion($usuario)) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("La aplicación no tiene validada la clave RSA.")
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        // Obtengo los eventos electorales del usuario
        $eventosElectorales = $em->getRepository('AppBundle:EventoElectoral')->getConvocatoriasUsuario($usuario);

        // Genero un token para cada evento electoral
        foreach($eventosElectorales as $evento) {
            $tokenViejo = $em->getRepository('AppBundle:TokenConvocatoriaElectoral')->findOneBy(array(
                'eventoElectoral'   => $evento,
                'usuario'           => $usuario
            ));
            if($tokenViejo)
                $em->remove($tokenViejo);
            $entity = new TokenConvocatoriaElectoral();
            $entity->setEventoElectoral($evento);
            $entity->setUsuario($usuario);
            $em->persist($entity);
        }

        $em->flush();

        // Devuelvo la información
        $tokensConvocatoriasElectorales = $em->getRepository('AppBundle:TokenConvocatoriaElectoral')->findBy(array('usuario' => $usuario));

        $data = array();
        $clavePublica = $usuario->getApp()->getClavePublica()->getClavePublica();
        $rsa = $this->get("rsa");
        foreach($tokensConvocatoriasElectorales as $token) {
            // Cifro el codigo con la clave publica
            $tokenCifrado = $rsa->publicEncrypt($token->getToken(), $clavePublica);
            $temp = array(
                'id_evento'     => $token->getEventoElectoral()->getId(),
                'token_cifrado' => $tokenCifrado
            );

            array_push($data, $temp);
        }

        $view->setData($data)->setStatusCode(200);

        return $view;
    }

    /**
     * Devuelve el evento electoral
     *
     * @ApiDoc(
     * description = "Devuelve las convocatorias electorales del usuario.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Get("/evento-electoral/evento/{dni}/{token}/electoral")
     *
     * @return View
     */
    public function getEventoElectoralAction($dni, $token)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $dni));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_NOT_FOUND,
                "message"   => sprintf("No existe usuario con dni %s en el sistema.", $dni)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        $tokenConvocatoriaElectoral = $em->getRepository('AppBundle:TokenConvocatoriaElectoral')->findOneBy(array('token' => $token, 'usuario' => $usuario));

        if(!$tokenConvocatoriaElectoral) {
            $data = array(
                "code"      => Response::HTTP_NOT_FOUND,
                "message"   => sprintf("El token %s no es válido.", $token)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        if($tokenConvocatoriaElectoral->getUsado())
        {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El token %s no es válido porque ya fue usando anteriormente.", $token)
            );
            $response = new Response();
            $response->setStatusCode($data['code']);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));

            return $response;
        }

        // Actualizo el token a usado
        $now = new \DateTime("now");
        $tokenConvocatoriaElectoral->setUsado($now);
        $em->persist($tokenConvocatoriaElectoral);
        $em->flush();

        // Obtengo la papeleta de votación del usuario
        $papeleta = $this->get('papeleta')->getPapeletaElector($tokenConvocatoriaElectoral->getEventoElectoral(), $tokenConvocatoriaElectoral->getUsuario());

        $view->setData($papeleta)->setStatusCode(200);

        // Dejo constancia en el log, la descarga del evento
        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf('[EVENTO %s] El elector %s se ha descargado la papeleta.', $tokenConvocatoriaElectoral->getEventoElectoral()->getId(), $usuario->getDni())
        );

        return $view;
    }

    /**
     *
     *
     * @ApiDoc(
     * description = "Validación del voto, recibe el codigo de validación enviado por GCM para validar el voto.",
     * statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Post("/validacion/voto")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="codigo", nullable=false, strict=true, description="Codigo")
     *
     *
     * @return View
     */
    public function validarVotoAction(ParamFetcher $paramFetcher)
    {
        $codigo = $paramFetcher->get('codigo');

        $em = $this->getDoctrine()->getManager();

        $electorValidacion = $em->getRepository('AppBundle:ElectorValidacion')->findOneBy(array('codigoValidacion' => $codigo));
        if(!$electorValidacion) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("No existe el voto en el sistema.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if($electorValidacion->getValidacion()) {
            $data = array(
                "code"      => Response::HTTP_OK,
                "message"   => sprintf("El voto ya ha sido validado.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if($electorValidacion->getElector()->getEventoElectoral()->getEstado() !== EventoElectoral::ESTADO_ABIERTO) {
            $data = array(
                "code"      => Response::HTTP_NOT_ACCEPTABLE,
                "message"   => sprintf("El proceso electoral no está abierto.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        // Descartamos los votos anteriores, si existen
        $electorValidaciones = $em->getRepository('AppBundle:ElectorValidacion')->findBy(array('elector' => $electorValidacion->getElector()));
        $l1s = array();
        foreach ($electorValidaciones as $e) {
            if($e->getL1() !== $electorValidacion->getL1()) {
                array_push($l1s, $e->getL1());
                $e->setEstado(ElectorValidacion::ESTADO_DESCARTADO);
            }
        }

        // Informamos al Sistema Electoral de la validacion del voto
        $votacionesIds = array();
        $votaciones = $electorValidacion->getElector()->getVotacion();
        foreach ($votaciones as $v)
            $votacionesIds[] = $v->getId();
        
        $resultado = false;
        $contador = 0;
        while(!$resultado) {
            $contador++;
            if($contador > 15) {
                $data = array(
                    "code"      => Response::HTTP_CONFLICT,
                    "message"   => sprintf("Problema notificando al Sistema Electoral la validez del voto.")
                );

                $response = new JsonResponse();
                $response->setStatusCode($data['code']);
                $response->setContent(json_encode($data));

                return $response;
            }
            $resultado = $this->get('api_sistema_electoral')->respuestaValidarCV($electorValidacion->getElector()->getTokenVotacion(), $electorValidacion->getL1(), $l1s, $votacionesIds);
        }

        $electorValidacion->setValidacion(new \DateTime('now'));
        $electorValidacion->setEstado(ElectorValidacion::ESTADO_VALIDADO);
        $em->flush();

        // Ciframos L1 como comprobante del voto para la app
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('dni' => $electorValidacion->getElector()->getDni()));
        if(!$usuario) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("No existe el usuario en el sistema.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $clavePrivadaSistemaGeneral = $this->get('rsa')->getPrivateKey();
        $l1Cifrado = $this->get('rsa')->privateEncrypt($electorValidacion->getL1(), $clavePrivadaSistemaGeneral);
        $data = array(
            'data' => array(
                'resguardo_voto'  => array(
                    'l1_cifrado' => $l1Cifrado,
                )
            )
        );
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_OK);
        $response->setContent(json_encode($data));

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf("[EVENTO %s][VOTO VALIDADO] EL elector=%s    L1= %s", $electorValidacion->getElector()->getEventoElectoral()->getId(), $electorValidacion->getElector()->getDni(), $electorValidacion->getL1())
        );

        return $response;
    }
}