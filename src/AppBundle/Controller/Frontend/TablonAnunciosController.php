<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Controller\Backend\EventoElectoralController;
use AppBundle\Entity\EventoElectoral;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * CensoElectoral controller.
 *
 * @Route("/tablon-anuncios")
 */
class TablonAnunciosController extends Controller
{
    /**
     * @Route("/", name="tablon_anuncios_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $eventos = $em->getRepository('AppBundle:EventoElectoral')->getConvocados();

        return $this->render(':Frontend/TablonAnuncios:index.html.twig', array(
            'eventos'   => $eventos
        ));
    }

    /**
     * @Route("/evento/{id}/show", name="tablon_anuncios_show")
     * @Method("GET")
     */
    public function showAction(EventoElectoral $evento)
    {
        if($evento->getEstado() === EventoElectoral::ESTADO_EDITABLE)
            throw $this->createNotFoundException('No es posible mostrar este evento.');

        // Solo si el evento está cerrado o finalizado publicamos la clave privada
        $priKey = null;
        if($evento->getEstado() === EventoElectoral::ESTADO_CERRADO or $evento->getEstado() === EventoElectoral::ESTADO_FINALIZADO)
            $priKey = $this->get('rsa')->getPrivateKey(
                sprintf(EventoElectoralController::PREFIX_CLAVE_RSA, $evento->getId())
            );

        $em = $this->getDoctrine()->getManager();

        // Datos de los electores
        $electorado = $em->getRepository('AppBundle:Elector')->getInfoAuditoriaElectores($evento);

        // Ordenamos votaciones abiertas, el resto ya estan ordenadas
        $escrutinioVotacionesAbiertas = array();
        foreach ($evento->getVotacion() as $v) {
            if($v->getClass() == 'VotacionListasAbiertas') {
                $temp = array();
                foreach ($v->getListas() as $l)
                    foreach ($l->getCandidatos() as $c)
                        array_push($temp, $c);

                // Ordeno por numero de votos
                $keys = array();
                foreach ($temp as $t)
                    $keys[] = $t->getNumeroVotos();
                array_multisort($keys, SORT_DESC, $temp, SORT_DESC);

                $escrutinioVotacionesAbiertas[$v->getId()] = $temp;
            }
        }


        return $this->render(':Frontend/TablonAnuncios:show.html.twig', array(
            'evento'                        => $evento,
            'clavePrivada'                  => $priKey,
            'electorado'                    => $electorado,
            'escrutinioVotacionesAbiertas'  => $escrutinioVotacionesAbiertas
        ));
    }
}
