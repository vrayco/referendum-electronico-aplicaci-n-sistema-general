<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use	Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
	    return $this->render('Frontend/homepage.html.twig');
    }

    /**
     * @Route("/certificado/no/valido", name="certificado_no_valido")
     */
    public function certificadoNoValidoAction()
    {
        return $this->render('Frontend/certificado_no_valido.html.twig');
    }
}
