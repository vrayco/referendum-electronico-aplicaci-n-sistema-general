<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * @Route("/secure/area/", name="validacion_automatica_dni")
     */
    public function validarDniAutoAction()
    {

		$cert_client = $_SERVER['SSL_CLIENT_CERT'];
    	
        $userData = openssl_x509_parse($cert_client);
        
        return $this->render('default/validar_auto_dni.html.twig', array(
	        'userData'	=> $userData,
        ));
        
	}
	
	/**
     * @Route("/secure/", name="login_secure")
     */
    public function loginAction() {
	    
	    return $this->redirect($this->generateUrl('homepage'));
    }
	
	
}
