<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\Table(name="votacion")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\DiscriminatorMap({"votacion_listas_cerradas"="VotacionListasCerradas", "votacion_listas_abiertas"="VotacionListasAbiertas", "votacion_referendum"="VotacionReferendum"})
 * @UniqueEntity(
 *     fields={"eventoElectoral", "nombre"},
 *     errorPath="nombre",
 *     message="No pueden existir dos votaciones con el mismo nombre en un Evento Electoral."
 * )
 * @ExclusionPolicy("all")
 */
class Votacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre")
     * @Expose
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     * @Expose
     */
    private $descripcion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="voto_en_blanco", type="boolean")
     * @Expose
     */
    private $votoEnBlanco;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EventoElectoral", inversedBy="votacion")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id")
     */
    private $eventoElectoral;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Elector", mappedBy="votacion", cascade={"persist", "remove"})
     */
    private $censoElectoral;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\VotacionLogotipo", mappedBy="votacion", cascade={"persist", "remove"})
     * @Expose
     */
    private $logotipo;

    /**
     * @ORM\Column(name="numero_votos", type="integer", nullable=true)
     */
    private $numeroVotos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->censoElectoral   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->votoEnBlanco     = true;
        $this->numeroVotos      = 0;
    }

    public function __toString()
    {
        return $this->getNombre();
    }


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Votacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }


    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Votacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     * @return Votacion
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral 
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }

    /**
     * Add censoElectoral
     *
     * @param \AppBundle\Entity\Elector $censoElectoral
     * @return Votacion
     */
    public function addCensoElectoral(\AppBundle\Entity\Elector $censoElectoral)
    {
        $this->censoElectoral[] = $censoElectoral;

        return $this;
    }

    /**
     * Remove censoElectoral
     *
     * @param \AppBundle\Entity\Elector $censoElectoral
     */
    public function removeCensoElectoral(\AppBundle\Entity\Elector $censoElectoral)
    {
        $this->censoElectoral->removeElement($censoElectoral);
    }

    /**
     * Get censoElectoral
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCensoElectoral()
    {
        return $this->censoElectoral;
    }

    /**
     * Set votoEnBlanco
     *
     * @param boolean $votoEnBlanco
     * @return Votacion
     */
    public function setVotoEnBlanco($votoEnBlanco)
    {
        $this->votoEnBlanco = $votoEnBlanco;

        return $this;
    }

    /**
     * Get votoEnBlanco
     *
     * @return boolean 
     */
    public function getVotoEnBlanco()
    {
        return $this->votoEnBlanco;
    }

    /**
     * Set logotipo
     *
     * @param \AppBundle\Entity\VotacionLogotipo $logotipo
     * @return Votacion
     */
    public function setLogotipo(\AppBundle\Entity\VotacionLogotipo $logotipo = null)
    {
        $logotipo->setVotacion($this);
        $this->logotipo = $logotipo;

        return $this;
    }

    /**
     * Get logotipo
     *
     * @return \AppBundle\Entity\VotacionLogotipo 
     */
    public function getLogotipo()
    {
        return $this->logotipo;
    }

    public function incNumeroVotos($valor = 1)
    {
        $this->numeroVotos += $valor;

        return $this;
    }

    /**
     * Set numeroVotos
     *
     * @param integer $numeroVotos
     * @return Votacion
     */
    public function setNumeroVotos($numeroVotos)
    {
        $this->numeroVotos = $numeroVotos;

        return $this;
    }

    /**
     * Get numeroVotos
     *
     * @return integer 
     */
    public function getNumeroVotos()
    {
        return $this->numeroVotos;
    }
}
