<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;

/**
 * EventoVotacion
 *
 * @ORM\Table(name="evento_electoral")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\EventoElectoralRepository")
 * @ExclusionPolicy("all")
 */
class EventoElectoral
{
    const ESTADO_EDITABLE   = "EDITABLE";
    const ESTADO_NOTIFICANDO= "NOTIFICANDO";
    const ESTADO_CONVOCADO  = "CONVOCADO";
    const ESTADO_ABIERTO    = "ABIERTO";
    const ESTADO_CERRADO    = "CERRADO";
    const ESTADO_FINALIZADO = "FINALIZADO";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre")
     * @Expose
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     * @Expose
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="convocado", type="datetime", nullable=true)
     * @Expose
     */
    private $convocado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="datetime", nullable=true)
     * @Expose
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="datetime", nullable=true)
     * @Expose
     */
    private $fin;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=16)
     * @Assert\Choice(choices = {"EDITABLE", "CONVOCADO", "ABIERTO", "CERRADO"}, message = "Elige una opción correcta.")
     * @Expose
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Votacion", mappedBy="eventoElectoral", cascade={"persist", "remove"})
     * @Expose
     */
    private $votacion;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\EventoElectoralLogotipo", mappedBy="eventoElectoral", cascade={"persist", "remove"})
     *
     * @Expose
     */
    private $logotipo;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Usuario", inversedBy="eventosElecorales")
     * @ORM\JoinTable(name="eventoelectoral_usuario",
     *      joinColumns={@ORM\JoinColumn(name="evento_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="administrador_id", referencedColumnName="id")}
     * )
     */
    private $administrador;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Elector", mappedBy="eventoElectoral", cascade={"persist", "remove"})
     */
    private $censoElectoral;

    //TODO eliminar clave publica
    /**
     * @ORM\Column(name="public_key", type="string", length=4096, nullable=true)
     */
    private $clavePublica;

    /**
     * @ORM\Column(name="numero_papeletas", type="integer")
     */
    private $numeroPapeletas;

    /**
     * @ORM\Column(name="numero_papeletas_nulas", type="integer")
     */
    private $numeroPapeletasNulas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->estado = self::ESTADO_EDITABLE;
        $this->votacionListasCerradas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->numeroPapeletas = 0;
        $this->numeroPapeletasNulas = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EventoVotacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return EventoVotacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add votacion
     *
     * @param \AppBundle\Entity\Votacion $votacion
     * @return EventoElectoral
     */
    public function addVotacion(\AppBundle\Entity\Votacion $votacion)
    {
        $this->votacion[] = $votacion;

        return $this;
    }

    /**
     * Remove votacion
     *
     * @param \AppBundle\Entity\Votacion $votacion
     */
    public function removeVotacion(\AppBundle\Entity\Votacion $votacion)
    {
        $this->votacion->removeElement($votacion);
    }

    /**
     * Get votacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVotacion()
    {
        return $this->votacion;
    }
    

    /**
     * Add administrador
     *
     * @param \AppBundle\Entity\Usuario $administrador
     * @return EventoElectoral
     */
    public function addAdministrador(\AppBundle\Entity\Usuario $administrador)
    {
        $this->administrador[] = $administrador;

        return $this;
    }

    /**
     * Remove administrador
     *
     * @param \AppBundle\Entity\Usuario $administrador
     */
    public function removeAdministrador(\AppBundle\Entity\Usuario $administrador)
    {
        $this->administrador->removeElement($administrador);
    }

    /**
     * Get administrador
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdministrador()
    {
        return $this->administrador;
    }

    /**
     * Add censoElectoral
     *
     * @param \AppBundle\Entity\Elector $censoElectoral
     * @return EventoElectoral
     */
    public function addCensoElectoral(\AppBundle\Entity\Elector $censoElectoral)
    {
        $this->censoElectoral[] = $censoElectoral;

        return $this;
    }

    /**
     * Remove censoElectoral
     *
     * @param \AppBundle\Entity\Elector $censoElectoral
     */
    public function removeCensoElectoral(\AppBundle\Entity\Elector $censoElectoral)
    {
        $this->censoElectoral->removeElement($censoElectoral);
    }

    /**
     * Get censoElectoral
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCensoElectoral()
    {
        return $this->censoElectoral;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return EventoElectoral
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return EventoElectoral
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;

        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return EventoElectoral
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime 
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set clavePublica
     *
     * @param string $clavePublica
     * @return ClavePublica
     */
    public function setClavePublica($clavePublica)
    {
        $this->clavePublica = $clavePublica;

        return $this;
    }

    /**
     * Get clavePublica
     *
     * @return string
     */
    public function getClavePublica()
    {
        return $this->clavePublica;
    }

    /**
     * Set logotipo
     *
     * @param \AppBundle\Entity\EventoElectoralLogotipo $logotipo
     * @return EventoElectoral
     */
    public function setLogotipo(\AppBundle\Entity\EventoElectoralLogotipo $logotipo = null)
    {
        $logotipo->setEventoElectoral($this);
        $this->logotipo = $logotipo;

        return $this;
    }

    /**
     * Get logotipo
     *
     * @return \AppBundle\Entity\EventoElectoralLogotipo 
     */
    public function getLogotipo()
    {
        return $this->logotipo;
    }

    /**
     * Set convocado
     *
     * @param \DateTime $convocado
     * @return EventoElectoral
     */
    public function setConvocado($convocado)
    {
        $this->convocado = $convocado;

        return $this;
    }

    /**
     * Get convocado
     *
     * @return \DateTime 
     */
    public function getConvocado()
    {
        return $this->convocado;
    }

    /**
     * Set numeroPapeletas
     *
     * @param integer $numeroPapeletas
     * @return EventoElectoral
     */
    public function setNumeroPapeletas($numeroPapeletas)
    {
        $this->numeroPapeletas = $numeroPapeletas;

        return $this;
    }

    /**
     * Get numeroPapeletas
     *
     * @return integer 
     */
    public function getNumeroPapeletas()
    {
        return $this->numeroPapeletas;
    }

    /**
     * Set numeroPapeletasNulas
     *
     * @param integer $numeroPapeletasNulas
     * @return EventoElectoral
     */
    public function setNumeroPapeletasNulas($numeroPapeletasNulas)
    {
        $this->numeroPapeletasNulas = $numeroPapeletasNulas;

        return $this;
    }

    /**
     * Get numeroPapeletasNulas
     *
     * @return integer 
     */
    public function getNumeroPapeletasNulas()
    {
        return $this->numeroPapeletasNulas;
    }
}
