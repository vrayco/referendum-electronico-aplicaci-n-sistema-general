<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\File as BaseFile;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * EventoElectoralLogotipo
 *
 * @ORM\Table(name="evento_electoral_logotipo")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"isImagenExtensionValid"})
 */
class EventoElectoralLogotipo extends BaseFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\EventoElectoral", inversedBy="logotipo")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Exclude()
     */
    private $eventoElectoral;

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/imagenes/evento_electoral_logotipo';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        parent::preUpload();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        parent::upload();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        parent::removeUpload();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return ListaLogotipo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set mime_type
     *
     * @param string $mimeType
     * @return ListaLogotipo
     */
    public function setMimeType($mimeType)
    {
        $this->mime_type = $mimeType;

        return $this;
    }

    /**
     * Get mime_type
     *
     * @return string 
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     * @return EventoElectoralLogotipo
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral 
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }
}
