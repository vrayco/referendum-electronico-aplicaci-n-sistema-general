<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Candidato
 *
 * @ORM\Table(name="candidato")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CandidatoRepository")
 * @UniqueEntity(
 *     fields={"posicion", "seccionLista", "lista"},
 *     errorPath="posicion",
 *     message="No pueden existir dos candidatos en la misma posición de la lista."
 * )
 * @ExclusionPolicy("all")
 */
class Candidato
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_completo", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre completo")
     * @Expose
     */
    private $nombreCompleto;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="votos_por_persona", type="integer")
     * @Assert\NotBlank(message="Por favor, ingrese la posicion")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     * @Expose
     */
    private $posicion;

    /**
     * @var string
     *
     * @ORM\Column(name="seccion_lista", type="string", length=9)
     * @Assert\Choice(choices = {"titular", "suplente"}, message = "Elige una opción correcta.")
     * @Expose
     */
    private $seccionLista;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\CandidatoFoto", mappedBy="candidato", cascade={"persist", "remove"})
     * @Expose
     */
    private $foto;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lista", inversedBy="candidatos")
     * @ORM\JoinColumn(name="lista_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $lista;

    /**
     * @ORM\Column(name="numero_votos", type="integer")
     */
    private $numeroVotos;

    public function __construct()
    {
        $this->numeroVotos = 0;
        $this->setSeccionLista("titular");
    }

    /**
     * Set posicion
     *
     * @param integer $posicion
     * @return Candidato
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return integer 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Set nombreCompleto
     *
     * @param string $nombreCompleto
     * @return Candidato
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;

        return $this;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seccionLista
     *
     * @param string $seccionLista
     * @return Candidato
     */
    public function setSeccionLista($seccionLista)
    {
        $this->seccionLista = $seccionLista;

        return $this;
    }

    /**
     * Get seccionLista
     *
     * @return string 
     */
    public function getSeccionLista()
    {
        return $this->seccionLista;
    }

    /**
     * Set lista
     *
     * @param \AppBundle\Entity\Lista $lista
     * @return Candidato
     */
    public function setLista(\AppBundle\Entity\Lista $lista = null)
    {
        $this->lista = $lista;

        return $this;
    }

    /**
     * Get lista
     *
     * @return \AppBundle\Entity\Lista 
     */
    public function getLista()
    {
        return $this->lista;
    }

    /**
     * Set foto
     *
     * @param \AppBundle\Entity\CandidatoFoto $foto
     * @return Candidato
     */
    public function setFoto(\AppBundle\Entity\CandidatoFoto $foto = null)
    {
        $foto->setCandidato($this);
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return \AppBundle\Entity\CandidatoFoto 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set numeroVotos
     *
     * @param integer $numeroVotos
     * @return Candidato
     */
    public function setNumeroVotos($numeroVotos)
    {
        $this->numeroVotos = $numeroVotos;

        return $this;
    }

    /**
     * Get numeroVotos
     *
     * @return integer 
     */
    public function getNumeroVotos()
    {
        return $this->numeroVotos;
    }

    public function incNumeroVotos()
    {
        $this->numeroVotos++;
        return $this;
    }
}
