<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

use Doctrine\ORM\Mapping as ORM;

/**
 * SolicitudApp
 *
 * @ORM\Table(name="solicitud_app")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class SolicitudApp
{
    const CODIGO1_LENGTH = 32;
    const CODIGO2_LENGTH = 6;
    const PERIODO_EXPIRACION = "now +1days";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sistema_operativo", type="string", length=16)
     * @Assert\Choice(choices = {"IOS", "ANDROID"}, message = "Sistema operativo no válido.")
     */
    private $sistemaOperativo;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_id", type="string", length=1024)
     */
    private $registrationId;

    /**
     * @var string
     *
     * @ORM\Column(name="app_version", type="string", length=32)
     * @Expose
     */
    private $appVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_1", type="string", length=256)
     */
    private $codigo1;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_2", type="string", length=256)
     */
    private $codigo2;

    /**
     * @var datetime
     *
     * @ORM\Column(name="validado_codigo_1", type="datetime", nullable=true)
     * @Expose
     */
    private $validadoCodigo1;

    /**
     * @var datetime
     *
     * @ORM\Column(name="validado_codigo_2", type="datetime", nullable=true)
     * @Expose
     */
    private $validadoCodigo2;

    /**
     * @var datetime
     *
     * @ORM\Column(name="envio_sms_codigo", type="datetime", nullable=true)
     * @Expose
     */
    private $envioSmsCodigo;

    /**
     * @var datetime
     *
     * @ORM\Column(name="expiracion", type="datetime", nullable=false)
     * @Expose
     */
    private $expiracion;

    /**
     * @ORM\OneToOne(targetEntity="App", inversedBy="solicitudApp")
     * @ORM\JoinColumn(name="android_app_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $app;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", cascade={"persist"})
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     * @Expose
     */
    private $usuario;


    public function __construct()
    {
        $this->setCodigo1($this->generateCodigo(SELF::CODIGO1_LENGTH));
        $this->setCodigo2($this->generateCodigo(SELF::CODIGO2_LENGTH));
        $this->setExpiracion(new \DateTime(SELF::PERIODO_EXPIRACION));
    }

    private function generateCodigo($numDigitos = 6)
    {
        $numDigitos *= -1;
        return $this->codigo = substr((md5(uniqid(rand(10,1000), true))),$numDigitos);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registrationId
     *
     * @param string $registrationId
     * @return SolicitudApp
     */
    public function setRegistrationId($registrationId)
    {
        $this->registrationId = $registrationId;

        return $this;
    }

    /**
     * Get registrationId
     *
     * @return string 
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }

    /**
     * Set appVersion
     *
     * @param string $appVersion
     * @return SolicitudApp
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;

        return $this;
    }

    /**
     * Get appVersion
     *
     * @return string 
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * Set codigo1
     *
     * @param string $codigo1
     * @return SolicitudApp
     */
    public function setCodigo1($codigo1)
    {
        $this->codigo1 = $codigo1;

        return $this;
    }

    /**
     * Get codigo1
     *
     * @return string 
     */
    public function getCodigo1()
    {
        return $this->codigo1;
    }

    /**
     * Set codigo2
     *
     * @param string $codigo2
     * @return SolicitudApp
     */
    public function setCodigo2($codigo2)
    {
        $this->codigo2 = $codigo2;

        return $this;
    }

    /**
     * Get codigo2
     *
     * @return string 
     */
    public function getCodigo2()
    {
        return $this->codigo2;
    }

    /**
     * Set validadoCodigo1
     *
     * @param \DateTime $validadoCodigo1
     * @return SolicitudApp
     */
    public function setValidadoCodigo1($validadoCodigo1)
    {
        $this->validadoCodigo1 = $validadoCodigo1;

        return $this;
    }

    /**
     * Get validadoCodigo1
     *
     * @return \DateTime 
     */
    public function getValidadoCodigo1()
    {
        return $this->validadoCodigo1;
    }

    /**
     * Set validadoCodigo2
     *
     * @param \DateTime $validadoCodigo2
     * @return SolicitudApp
     */
    public function setValidadoCodigo2($validadoCodigo2)
    {
        $this->validadoCodigo2 = $validadoCodigo2;

        return $this;
    }

    /**
     * Get validadoCodigo2
     *
     * @return \DateTime 
     */
    public function getValidadoCodigo2()
    {
        return $this->validadoCodigo2;
    }

    /**
     * Set envioSmsCodigo
     *
     * @param \DateTime $envioSmsCodigo
     * @return SolicitudApp
     */
    public function setEnvioSmsCodigo($envioSmsCodigo)
    {
        $this->envioSmsCodigo = $envioSmsCodigo;

        return $this;
    }

    /**
     * Get envioSmsCodigo
     *
     * @return \DateTime 
     */
    public function getEnvioSmsCodigo()
    {
        return $this->envioSmsCodigo;
    }

    /**
     * Set expiracion
     *
     * @param \DateTime $expiracion
     * @return SolicitudApp
     */
    public function setExpiracion($expiracion)
    {
        $this->expiracion = $expiracion;

        return $this;
    }

    /**
     * Get expiracion
     *
     * @return \DateTime 
     */
    public function getExpiracion()
    {
        return $this->expiracion;
    }

    /**
     * Set app
     *
     * @param \AppBundle\Entity\App $app
     * @return App
     */
    public function setApp(\AppBundle\Entity\App $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \AppBundle\Entity\App
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return SolicitudApp
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set sistemaOperativo
     *
     * @param string $sistemaOperativo
     * @return SolicitudApp
     */
    public function setSistemaOperativo($sistemaOperativo)
    {
        $this->sistemaOperativo = $sistemaOperativo;

        return $this;
    }

    /**
     * Get sistemaOperativo
     *
     * @return string 
     */
    public function getSistemaOperativo()
    {
        return $this->sistemaOperativo;
    }
}
