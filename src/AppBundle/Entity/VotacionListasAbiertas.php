<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * ListasAbiertas
 *
 * @ORM\Table(name="votacion_listas_abiertas")
 * @ORM\Entity
 * @Assert\Callback(methods={"configuracionCandidatosValido"})
 * @ExclusionPolicy("all")
 */
class VotacionListasAbiertas extends Votacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="votos_por_elector", type="integer")
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     * @Expose
     */
    private $votosPorElector;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimo_candidatos", type="integer")
     * @Assert\NotNull(message="Debe introducir un valor")
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     */
    private $minimoCandidatos;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximo_candidatos", type="integer")
     * @Assert\NotNull(message="Debe introducir un valor")
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     */
    private $maximoCandidatos;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Lista", mappedBy="votacionListasAbiertas", cascade={"persist", "remove"})
     * @Expose
     */
    private $listas;

    /**
     * @ORM\Column(name="numero_votos_en_blanco", type="integer")
     */
    private $numeroVotosEnBlanco;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->votosPorElector = 1;
        $this->numeroVotosEnBlanco = 0;
        $this->listas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function configuracionCandidatosValido(ExecutionContextInterface $context)
    {
        if($this->getMaximoCandidatos() < $this->getMinimoCandidatos())
        {
            $context->buildViolation('El valor {{ value }} es menor del valor de MINIMO CANDIDATOS.')
                ->atPath('maximoCandidatos')
                ->setParameter('{{ value }}', $this->getMaximoCandidatos())
                ->addViolation();
        }
    }

    /**
     * Set votosPorElector
     *
     * @param integer $votosPorElector
     * @return VotacionListasAbiertas
     */
    public function setVotosPorElector($votosPorElector)
    {
        $this->votosPorElector = $votosPorElector;

        return $this;
    }

    /**
     * Get votosPorElector
     *
     * @return integer 
     */
    public function getVotosPorElector()
    {
        return $this->votosPorElector;
    }

    /**
     * Set minimoCandidatos
     *
     * @param integer $minimoCandidatos
     * @return VotacionListasAbiertas
     */
    public function setMinimoCandidatos($minimoCandidatos)
    {
        $this->minimoCandidatos = $minimoCandidatos;

        return $this;
    }

    /**
     * Get minimoCandidatos
     *
     * @return integer 
     */
    public function getMinimoCandidatos()
    {
        return $this->minimoCandidatos;
    }

    /**
     * Set maximoCandidatos
     *
     * @param integer $maximoCandidatos
     * @return VotacionListasAbiertas
     */
    public function setMaximoCandidatos($maximoCandidatos)
    {
        $this->maximoCandidatos = $maximoCandidatos;

        return $this;
    }

    /**
     * Get maximoCandidatos
     *
     * @return integer 
     */
    public function getMaximoCandidatos()
    {
        return $this->maximoCandidatos;
    }
    

    /**
     * Add listas
     *
     * @param \AppBundle\Entity\Lista $listas
     * @return VotacionListasAbiertas
     */
    public function addLista(\AppBundle\Entity\Lista $listas)
    {
        $this->listas[] = $listas;

        return $this;
    }

    /**
     * Remove listas
     *
     * @param \AppBundle\Entity\Lista $listas
     */
    public function removeLista(\AppBundle\Entity\Lista $listas)
    {
        $this->listas->removeElement($listas);
    }

    /**
     * Get listas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getListas()
    {
        return $this->listas;
    }
    
    public function getClass()
    {
        return "VotacionListasAbiertas";
    }

    /**
     * Set numeroVotosEnBlanco
     *
     * @param integer $numeroVotosEnBlanco
     * @return VotacionListasAbiertas
     */
    public function setNumeroVotosEnBlanco($numeroVotosEnBlanco)
    {
        $this->numeroVotosEnBlanco = $numeroVotosEnBlanco;

        return $this;
    }

    /**
     * Get numeroVotosEnBlanco
     *
     * @return integer 
     */
    public function getNumeroVotosEnBlanco()
    {
        return $this->numeroVotosEnBlanco;
    }

    public function incNumeroVotosEnBlanco($valor = 1)
    {
        $this->numeroVotosEnBlanco += $valor;

        return $this;
    }
    
    public function getNumeroTotalVotos()
    {
        return $this->getNumeroVotos() + $this->getNumeroVotosEnBlanco();
    }

    public function getNumeroTotalVotantes()
    {
        if($this->getVotosPorElector() == 0)
            return null;
        
        return ($this->getNumeroVotos() + $this->getNumeroVotosEnBlanco())/ $this->getVotosPorElector();
    }
}
