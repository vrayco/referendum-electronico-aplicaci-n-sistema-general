<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use JMS\Serializer\Annotation\Accessor;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * ListasCerradas
 *
 * @ORM\Table(name="votacion_listas_cerradas")
 * @ORM\Entity
 * @Assert\Callback(methods={"configuracionCandidatosValido"})
 * @ExclusionPolicy("all")
 */
class VotacionListasCerradas extends Votacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="votos_por_elector", type="integer")
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     * @Expose
     */
    private $votosPorElector;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimo_candidatos", type="integer")
     * @Assert\NotNull(message="Debe introducir un valor")
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     */
    private $minimoCandidatos;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximo_candidatos", type="integer")
     * @Assert\NotNull(message="Debe introducir un valor")
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     */
    private $maximoCandidatos;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimo_candidatos_suplentes", type="integer")
     * @Assert\NotNull(message="Debe introducir un valor")
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     */
    private $minimoCandidatosSuplentes;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximo_candidatos_suplentes", type="integer")
     * @Assert\NotNull(message="Debe introducir un valor")
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message="Debe ser mayor o igual que 0"
     * )
     */
    private $maximoCandidatosSuplentes;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Lista", mappedBy="votacionListasCerradas", cascade={"persist", "remove"})
     * @Expose
     * @Accessor(getter="getListasRandom")
     * @OrderBy({"numeroVotos" = "DESC"})
     */
    private $listas;

    /**
     * @ORM\Column(name="numero_votos_en_blanco", type="integer")
     */
    private $numeroVotosEnBlanco;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->votosPorElector = 1;
        $this->numeroVotosEnBlanco = 0;
        $this->listas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function configuracionCandidatosValido(ExecutionContextInterface $context)
    {
        if($this->getMaximoCandidatos() < $this->getMinimoCandidatos())
        {
            $context->buildViolation('El valor {{ value }} es menor del valor de MINIMO CANDIDATOS.')
                ->atPath('maximoCandidatos')
                ->setParameter('{{ value }}', $this->getMaximoCandidatos())
                ->addViolation();
        }

        if($this->getMaximoCandidatosSuplentes() < $this->getMinimoCandidatosSuplentes())
        {
            $context->buildViolation('El valor {{ value }} es menor del valor de MINIMO CANDIDATOS SUPLENTES.')
                ->atPath('maximoCandidatosSuplentes')
                ->setParameter('{{ value }}', $this->getMaximoCandidatosSuplentes())
                ->addViolation();
        }
    }

    /**
     * Set minimoCandidatos
     *
     * @param integer $minimoCandidatos
     * @return VotacionListasCerradas
     */
    public function setMinimoCandidatos($minimoCandidatos)
    {
        $this->minimoCandidatos = $minimoCandidatos;

        return $this;
    }

    /**
     * Get minimoCandidatos
     *
     * @return integer 
     */
    public function getMinimoCandidatos()
    {
        return $this->minimoCandidatos;
    }

    /**
     * Set maximoCandidatos
     *
     * @param integer $maximoCandidatos
     * @return VotacionListasCerradas
     */
    public function setMaximoCandidatos($maximoCandidatos)
    {
        $this->maximoCandidatos = $maximoCandidatos;

        return $this;
    }

    /**
     * Get maximoCandidatos
     *
     * @return integer 
     */
    public function getMaximoCandidatos()
    {
        return $this->maximoCandidatos;
    }

    /**
     * Set minimoCandidatosSuplentes
     *
     * @param integer $minimoCandidatosSuplentes
     * @return VotacionListasCerradas
     */
    public function setMinimoCandidatosSuplentes($minimoCandidatosSuplentes)
    {
        $this->minimoCandidatosSuplentes = $minimoCandidatosSuplentes;

        return $this;
    }

    /**
     * Get minimoCandidatosSuplentes
     *
     * @return integer 
     */
    public function getMinimoCandidatosSuplentes()
    {
        return $this->minimoCandidatosSuplentes;
    }

    /**
     * Set maximoCandidatosSuplentes
     *
     * @param integer $maximoCandidatosSuplentes
     * @return VotacionListasCerradas
     */
    public function setMaximoCandidatosSuplentes($maximoCandidatosSuplentes)
    {
        $this->maximoCandidatosSuplentes = $maximoCandidatosSuplentes;

        return $this;
    }

    /**
     * Get maximoCandidatosSuplentes
     *
     * @return integer 
     */
    public function getMaximoCandidatosSuplentes()
    {
        return $this->maximoCandidatosSuplentes;
    }

    /**
     * Set votosPorElector
     *
     * @param integer $votosPorElector
     * @return VotacionListasCerradas
     */
    public function setVotosPorElector($votosPorElector)
    {
        $this->votosPorElector = $votosPorElector;

        return $this;
    }

    /**
     * Get votosPorElector
     *
     * @return integer 
     */
    public function getVotosPorElector()
    {
        return $this->votosPorElector;
    }


    /**
     * Add listas
     *
     * @param \AppBundle\Entity\Lista $listas
     * @return VotacionListasCerradas
     */
    public function addLista(\AppBundle\Entity\Lista $listas)
    {
        $this->listas[] = $listas;

        return $this;
    }

    /**
     * Remove listas
     *
     * @param \AppBundle\Entity\Lista $listas
     */
    public function removeLista(\AppBundle\Entity\Lista $listas)
    {
        $this->listas->removeElement($listas);
    }

    /**
     * Get listas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getListas()
    {
        return $this->listas;
    }

    public function getListasRandom()
    {
        $resultado = new \Doctrine\Common\Collections\ArrayCollection();

        $keys = $this->getListas()->getKeys();
        shuffle($keys);
        foreach ($keys as $key)
            $resultado->add($this->listas->get($key));

        return $resultado;
    }

    public function getClass()
    {
        return "VotacionListasCerradas";
    }

    /**
     * Set numeroVotosEnBlanco
     *
     * @param integer $numeroVotosEnBlanco
     * @return VotacionListasCerradas
     */
    public function setNumeroVotosEnBlanco($numeroVotosEnBlanco)
    {
        $this->numeroVotosEnBlanco = $numeroVotosEnBlanco;

        return $this;
    }

    /**
     * Get numeroVotosEnBlanco
     *
     * @return integer 
     */
    public function getNumeroVotosEnBlanco()
    {
        return $this->numeroVotosEnBlanco;
    }
    
    public function incNumeroVotosEnBlanco($valor = 1)
    {
        $this->numeroVotosEnBlanco += $valor;
        
        return $this;
    }

    public function getNumeroTotalVotos()
    {
        return $this->getNumeroVotos() + $this->getNumeroVotosEnBlanco();
    }
}
