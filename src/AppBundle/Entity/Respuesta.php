<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Respuesta
 *
 * @ORM\Table(name="respuesta")
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"pregunta", "orden"},
 *     errorPath="posicion",
 *     message="No pueden existir dos respuestas con el mismo valor de orden."
 * )
 * @ExclusionPolicy("all")
 */
class Respuesta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pregunta", inversedBy="respuestas")
     * @ORM\JoinColumn(name="pregunta_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $pregunta;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre completo")
     * @Expose
     */
    private $respuesta;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     * @Assert\NotBlank(message="Por favor, ingrese el orden")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     * @Expose
     */
    private $orden;

    /**
     * @ORM\Column(name="numero_votos", type="integer")
     */
    private $numeroVotos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->numeroVotos = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     * @return Respuesta
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     * @return Respuesta
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Respuesta
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set numeroVotos
     *
     * @param integer $numeroVotos
     * @return Respuesta
     */
    public function setNumeroVotos($numeroVotos)
    {
        $this->numeroVotos = $numeroVotos;

        return $this;
    }

    /**
     * Get numeroVotos
     *
     * @return integer 
     */
    public function getNumeroVotos()
    {
        return $this->numeroVotos;
    }

    public function incNumeroVotos()
    {
        $this->numeroVotos++;
        return $this;
    }
}
