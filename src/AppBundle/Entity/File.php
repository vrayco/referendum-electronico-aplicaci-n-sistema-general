<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Accessor;

/**
 * @ExclusionPolicy("all")
 */
abstract class File
{

    /**
     * @ORM\Column(type="string", length=256, nullable=false)
     * @Accessor(getter="getWebPath",setter="setPath")
     * @Expose
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     * @Expose
     */
    protected $mime_type;

    /**
     * @Assert\File(maxSize="10M")
     */
    protected $file;
    protected $temp;

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }

        $this->setMimeType($file->getMimeType());
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    // protected function getUploadDir()
    // {
    //     // get rid of the __DIR__ so it doesn't screw up
    //     // when displaying uploaded doc/image in the view.
    //     return 'uploads/eventos/carteles';
    // }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    /**
     * Set path
     *
     * @param string $path
     * @return CartelEvento
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    public function setMimeType($mime_type)
    {
        $this->mime_type = $mime_type;

        return $this;
    }

    public function getMimeType()
    {
        return $this->mime_type;
    }

    public function isImagenExtensionValid(ExecutionContextInterface $context)
    {
        $extension = array('image/jpeg','image/png','image/gif',/*,'image/tiff','image/x-ms-bmp'*/);

        if($this->getMimeType() != "")
            if( !in_array($this->getMimeType(), $extension) )
                $context->addViolationAt('file', 'El formato de la imagen no es válida. Los formatos válidos son: jpg, png.', array(), null);

    }
}