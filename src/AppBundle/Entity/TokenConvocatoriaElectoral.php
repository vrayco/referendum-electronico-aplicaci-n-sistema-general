<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TokenConvocatoriaElectoral
 *
 * @ORM\Table(name="token_convocatoria_electoral")
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"usuario", "eventoElectoral"},
 *     message="El usuario no puede tener dos token para el mismo evento."
 * )
 */
class TokenConvocatoriaElectoral
{
    const EXPIRA = "+1 days";
    const TOKEN_LENGTH = 32;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=64)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expira", type="datetime")
     */
    private $expira;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usado", type="datetime", nullable=true)
     */
    private $usado;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario", inversedBy="tokensConvocatoriaElectoral")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EventoElectoral")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id")
     */
    private $eventoElectoral;

    public function __construct()
    {
        // Establezco la fecha de expiración del token
        $this->setExpira(new \DateTime("now ".SELF::EXPIRA));
        // Genero el token
        $bytes = openssl_random_pseudo_bytes(SELF::TOKEN_LENGTH);
        $token = bin2hex($bytes);
        $this->setToken($token);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return TokenConvocatoriaElectoral
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expira
     *
     * @param \DateTime $expira
     * @return TokenConvocatoriaElectoral
     */
    public function setExpira($expira)
    {
        $this->expira = $expira;

        return $this;
    }

    /**
     * Get expira
     *
     * @return \DateTime 
     */
    public function getExpira()
    {
        return $this->expira;
    }

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     * @return TokenConvocatoriaElectoral
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral 
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return TokenConvocatoriaElectoral
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usado
     *
     * @param \DateTime $usado
     * @return TokenConvocatoriaElectoral
     */
    public function setUsado($usado)
    {
        $this->usado = $usado;

        return $this;
    }

    /**
     * Get usado
     *
     * @return \DateTime 
     */
    public function getUsado()
    {
        return $this->usado;
    }
}
