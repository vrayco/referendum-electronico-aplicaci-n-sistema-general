<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\File as BaseFile;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * CandidatoFoto
 *
 * @ORM\Table(name="candidato_foto")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"isImagenExtensionValid"})
 */
class CandidatoFoto extends BaseFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Candidato", inversedBy="foto")
     * @ORM\JoinColumn(name="lista_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Exclude()
     */
    private $candidato;

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/imagenes/candidato_foto';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        parent::preUpload();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        parent::upload();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        parent::removeUpload();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return ListaLogotipo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set mime_type
     *
     * @param string $mimeType
     * @return ListaLogotipo
     */
    public function setMimeType($mimeType)
    {
        $this->mime_type = $mimeType;

        return $this;
    }

    /**
     * Get mime_type
     *
     * @return string 
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * Set candidato
     *
     * @param \AppBundle\Entity\Candidato $candidato
     * @return CandidatoFoto
     */
    public function setCandidato(\AppBundle\Entity\Candidato $candidato = null)
    {
        $this->candidato = $candidato;

        return $this;
    }

    /**
     * Get candidato
     *
     * @return \AppBundle\Entity\Candidato 
     */
    public function getCandidato()
    {
        return $this->candidato;
    }
}
