<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Candidatura
 *
 * @ORM\Table(name="lista")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ListaRepository")
 * @Assert\Callback(methods={"listaValida"})
 * @ExclusionPolicy("all")
 */
class Lista
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_completo", type="string", length=128)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre completo")
     * @Expose
     */
    private $nombreCompleto;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_abreviado", type="string", length=32)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre abrevidado")
     * @Expose
     */
    private $nombreAbreviado;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Candidato", mappedBy="lista", cascade={"persist", "remove"})
     * @ORM\OrderBy({"posicion" = "ASC"})
     * @Expose
     */
    private $candidatos;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ListaLogotipo", mappedBy="lista", cascade={"persist", "remove"})
     * @Expose
     */
    private $logotipo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\VotacionListasCerradas", inversedBy="listas")
     * @ORM\JoinColumn(name="votacion_listas_cerradas_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $votacionListasCerradas;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\VotacionListasAbiertas", inversedBy="listas")
     * @ORM\JoinColumn(name="votacion_listas_abiertas_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $votacionListasAbiertas;

    /**
     * @ORM\Column(name="numero_votos", type="integer")
     */
    private $numeroVotos;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->numeroVotos = 0;
        $this->candidatos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function listaValida(ExecutionContextInterface $context)
    {
        if($this->getVotacionListasCerradas() and $this->getVotacionListasAbiertas())
        {
            $context->buildViolation('La lista debe tener asociado una única votación.')
                ->addViolation();
        }

        if(!$this->getVotacionListasCerradas() and !$this->getVotacionListasAbiertas())
        {
            $context->buildViolation('La lista debe tener asociado una votación.')
                ->addViolation();
        }

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreCompleto
     *
     * @param string $nombreCompleto
     * @return Lista
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;

        return $this;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }

    /**
     * Set nombreAbreviado
     *
     * @param string $nombreAbreviado
     * @return Lista
     */
    public function setNombreAbreviado($nombreAbreviado)
    {
        $this->nombreAbreviado = $nombreAbreviado;

        return $this;
    }

    /**
     * Get nombreAbreviado
     *
     * @return string 
     */
    public function getNombreAbreviado()
    {
        return $this->nombreAbreviado;
    }

    /**
     * Add candidatos
     *
     * @param \AppBundle\Entity\Candidato $candidato
     * @return Lista
     */
    public function addCandidato(\AppBundle\Entity\Candidato $candidato)
    {
        $candidato->setLista($this);
        $this->candidatos->add($candidato);

        return $this;
    }

    /**
     * Remove candidatos
     *
     * @param \AppBundle\Entity\Candidato $candidato
     */
    public function removeCandidato(\AppBundle\Entity\Candidato $candidato)
    {
        $this->candidatos->removeElement($candidato);
    }

    /**
     * Get candidatos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCandidatos()
    {
        return $this->candidatos;
    }

    

    /**
     * Set votacionListasCerradas
     *
     * @param \AppBundle\Entity\VotacionListasCerradas $votacionListasCerradas
     * @return Lista
     */
    public function setVotacionListasCerradas(\AppBundle\Entity\VotacionListasCerradas $votacionListasCerradas = null)
    {
        $this->votacionListasCerradas = $votacionListasCerradas;

        return $this;
    }

    /**
     * Get votacionListasCerradas
     *
     * @return \AppBundle\Entity\VotacionListasCerradas 
     */
    public function getVotacionListasCerradas()
    {
        return $this->votacionListasCerradas;
    }

    /**
     * Set votacionListasAbiertas
     *
     * @param \AppBundle\Entity\VotacionListasAbiertas $votacionListasAbiertas
     * @return Lista
     */
    public function setVotacionListasAbiertas(\AppBundle\Entity\VotacionListasAbiertas $votacionListasAbiertas = null)
    {
        $this->votacionListasAbiertas = $votacionListasAbiertas;

        return $this;
    }

    /**
     * Get votacionListasAbiertas
     *
     * @return \AppBundle\Entity\VotacionListasAbiertas 
     */
    public function getVotacionListasAbiertas()
    {
        return $this->votacionListasAbiertas;
    }

    public function getVotacion()
    {
        if($this->votacionListasCerradas !== null)
            return $this->votacionListasCerradas;
        else if($this->votacionListasAbiertas != null)
            return $this->votacionListasAbiertas;

        return null;
    }

    /**
     * Set logotipo
     *
     * @param \AppBundle\Entity\ListaLogotipo $logotipo
     * @return Lista
     */
    public function setLogotipo(\AppBundle\Entity\ListaLogotipo $logotipo = null)
    {
        $logotipo->setLista($this);
        $this->logotipo = $logotipo;

        return $this;
    }

    /**
     * Get logotipo
     *
     * @return \AppBundle\Entity\ListaLogotipo 
     */
    public function getLogotipo()
    {
        return $this->logotipo;
    }

    /**
     * Set numeroVotos
     *
     * @param integer $numeroVotos
     * @return Lista
     */
    public function setNumeroVotos($numeroVotos)
    {
        $this->numeroVotos = $numeroVotos;

        return $this;
    }

    /**
     * Get numeroVotos
     *
     * @return integer 
     */
    public function getNumeroVotos()
    {
        return $this->numeroVotos;
    }

    public function incNumeroVotos()
    {
        $this->numeroVotos++;
        return $this;
    }
}
