<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\Container;

/**
 * EventoElectoralRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EventoElectoralRepository extends EntityRepository
{

    public function getConvocatoriaUsuario(EventoElectoral $evento, Usuario $usuario)
    {
        $result = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c.id AS id_elector, c.tokenVotacion AS token_votacion, e as evento')
            ->from('AppBundle:EventoElectoral','e')
            ->innerJoin('e.votacion','v')
            ->innerJoin('v.censoElectoral', 'c')
            ->where('e = :evento')
            ->andWhere('c.dni = :dni')
            ->setParameter('evento', $evento)
            ->setParameter('dni', $usuario->getDni())
            ->orderBy('e.id', 'ASC')
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function getConvocatoriasUsuario(Usuario $usuario)
    {
        $result = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:EventoElectoral','e')
            ->innerJoin('e.votacion','v')
            ->innerJoin('v.censoElectoral', 'c')
            ->where('c.dni = :dni')
            ->andWhere('e.estado <> :editable')
            ->setParameter('dni', $usuario->getDni())
            ->setParameter('editable', EventoElectoral::ESTADO_EDITABLE)
            ->orderBy('e.id', 'ASC')
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function findAllByUser($usuario)
    {
        $result = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:EventoElectoral','e')
            ->innerJoin('e.administrador','a')
            ->where('a = :usuario')
            ->setParameter('usuario', $usuario->getId())
            ->orderBy('e.id', 'ASC')
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function getCensoElectoral(EventoElectoral $eventoElectoral)
    {
        $electores = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e, v')
            ->from('AppBundle:Elector', 'e')
            ->innerJoin('e.eventoElectoral', 'ev')
            ->innerJoin('e.votacion', 'v')
            ->where('ev = :eventoElectoral')
            ->setParameter('eventoElectoral', $eventoElectoral)
            ->orderBy('e.dni')
            ->getQuery()
            ->getResult();

        $result = array();

        // Obtengo la estructura de datos para representar el elector y su estado
        $temp = $this->getObjetoElector();

        // Defino en la estructura las votaciones del evento electoral
        $votaciones = $eventoElectoral->getVotacion();
        foreach($votaciones as $v) {
            $temp['votaciones'][$v->getId()] = false;
        }

        foreach($electores as $e) {
            $elector = $temp;
            $elector['id'] = $e->getId();
            $elector['dni'] = $e->getDni();
            $elector['nombre'] = $e->getNombre();
            $elector['primer_apellido'] = $e->getPrimerApellido();
            $elector['segundo_apellido'] = $e->getSegundoApellido();
            $elector['registro'] = true;

            // Añadimos estado del usuario
            $usuario = $this->getEntityManager()->getRepository('AppBundle:Usuario')->findOneBy(array('dni'=>$e->getDni()));
            if($usuario) {
                // Validacion usuario
                if($usuario->isValidDni())
                    $elector['validacion']['dni'] = $usuario->isValidDni();
                if($usuario->isValidTelefono())
                    $elector['validacion']['numero_telefono'] = $usuario->isValidTelefono();

                if($usuario->getApp()) {
                    $elector['validacion']['app'] = $usuario->isValidApp($usuario->getApp()->getRegistrationId());
                    $elector['validacion']['clave_publica'] = $usuario->isPublicKeyValid($usuario->getApp()->getRegistrationId());
                } else {
                    $elector['validacion']['app'] = false;
                    $elector['validacion']['clave_publica'] = false;
                }
            }

            // Añadimos las votaciones a las que tiene permiso
            foreach($e->getVotacion() as $v2) {
                $elector['votaciones'][$v2->getId()] = $v2;
            }

            array_push($result, $elector);
        }

        return $result;
    }

    public function getConvocados()
    {
        $result = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:EventoElectoral','e')
            ->where('e.estado <> :editable ')
            ->setParameter('editable', EventoElectoral::ESTADO_EDITABLE)
            ->orderBy('e.id', 'ASC')
            ->getQuery()
            ->getResult();

        return $result;
    }

    private function getObjetoElector()
    {
        $temp = array(
            'id'                => null,
            'dni'               => null,
            'nombre'            => null,
            'primer_apellido'   => null,
            'segundo_apellido'  => null,
            'registro'          => false,
            'validacion'=> array(
                'dni'             => false,
                'numero_telefono'  => false,
                'app'             => false,
                'clave_publica'     => false,
            ),
            'votaciones'        => array()
        );

        return $temp;

    }
}
