<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ElectorValidacion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ElectorValidacion
{
    const TOKEN_LENGTH = 32;
    const ESTADO_SIN_VALIDAR    = "SIN_VALIDAR";
    const ESTADO_VALIDADO       = "VALIDADO";
    const ESTADO_DESCARTADO     = "DESCARTADO";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Elector", inversedBy="electorValidaciones")
     * @ORM\JoinColumn(name="elector_id", referencedColumnName="id")
     */
    private $elector;

    /**
     * @var string
     *
     * @ORM\Column(name="l1", type="string", length=255)
     */
    private $l1;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoValidacion", type="string", length=255)
     */
    private $codigoValidacion;

    /**
     * @ORM\Column(name="validacion", type="datetime", nullable=true)
     */
    private $validacion;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=32)
     */
    private $estado;

    public function __construct()
    {
        $this->created_at = new \Datetime('now');
        $this->estado = self::ESTADO_SIN_VALIDAR;
        
        // Genero el token de votación
        $bytes = openssl_random_pseudo_bytes(SELF::TOKEN_LENGTH);
        $token = bin2hex($bytes);
        $this->setCodigoValidacion($token);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoValidacion
     *
     * @param string $codigoValidacion
     * @return ElectorValidacion
     */
    public function setCodigoValidacion($codigoValidacion)
    {
        $this->codigoValidacion = $codigoValidacion;

        return $this;
    }

    /**
     * Get codigoValidacion
     *
     * @return string 
     */
    public function getCodigoValidacion()
    {
        return $this->codigoValidacion;
    }

    /**
     * Set elector
     *
     * @param \AppBundle\Entity\Elector $elector
     * @return ElectorValidacion
     */
    public function setElector(\AppBundle\Entity\Elector $elector = null)
    {
        $this->elector = $elector;

        return $this;
    }

    /**
     * Get elector
     *
     * @return \AppBundle\Entity\Elector 
     */
    public function getElector()
    {
        return $this->elector;
    }

    /**
     * Set validacion
     *
     * @param \DateTime $validacion
     * @return ElectorValidacion
     */
    public function setValidacion($validacion)
    {
        $this->validacion = $validacion;

        return $this;
    }

    /**
     * Get validacion
     *
     * @return \DateTime 
     */
    public function getValidacion()
    {
        return $this->validacion;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return ElectorValidacion
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set l1
     *
     * @param string $l1
     * @return ElectorValidacion
     */
    public function setL1($l1)
    {
        $this->l1 = $l1;

        return $this;
    }

    /**
     * Get l1
     *
     * @return string 
     */
    public function getL1()
    {
        return $this->l1;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ElectorValidacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
