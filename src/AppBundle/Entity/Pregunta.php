<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Pregunta
 *
 * @ORM\Table(name="pregunta")
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"referendum", "orden"},
 *     errorPath="orden",
 *     message="No pueden existir dos preguntas con el mismo valor de orden."
 * )
 * @ExclusionPolicy("all")
 */
class Pregunta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta", type="string", length=512)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre completo")
     * @Expose
     */
    private $pregunta;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     * @Assert\NotBlank(message="Por favor, ingrese el orden")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     * @Expose
     */
    private $orden;

    // TODO eleiminar atributo
    /**
     * @var boolean
     *
     * @ORM\Column(name="multirespuesta", type="boolean")
     * @Expose
     */
    private $multirespuesta;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Respuesta", mappedBy="pregunta", cascade={"persist", "remove"})
     * @Expose
     */
    private $respuestas;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\VotacionReferendum", inversedBy="preguntas")
     * @ORM\JoinColumn(name="referendum_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $referendum;

    /**
     * @ORM\Column(name="numero_votos_en_blanco", type="integer")
     */
    private $numeroVotosEnBlanco;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set multirespuesta
     *
     * @param boolean $multirespuesta
     * @return Pregunta
     */
    public function setMultirespuesta($multirespuesta)
    {
        $this->multirespuesta = $multirespuesta;

        return $this;
    }

    /**
     * Get multirespuesta
     *
     * @return boolean 
     */
    public function getMultirespuesta()
    {
        return $this->multirespuesta;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->multirespuesta       = false;
        $this->numeroVotosEnBlanco  = 0;
        $this->respuestas           = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add respuesta
     *
     * @param \AppBundle\Entity\Respuesta $respuesta
     * @return Pregunta
     */
    public function addRespuesta(\AppBundle\Entity\Respuesta $respuesta)
    {
        $respuesta->setPregunta($this);
        $this->respuestas[] = $respuesta;

        return $this;
    }

    /**
     * Remove respuesta
     *
     * @param \AppBundle\Entity\Respuesta $respuesta
     */
    public function removeRespuesta(\AppBundle\Entity\Respuesta $respuesta)
    {
        $this->respuestas->removeElement($respuesta);
    }

    /**
     * Get respuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRespuestas()
    {
        return $this->respuestas;
    }

    /**
     * Set referendum
     *
     * @param \AppBundle\Entity\VotacionReferendum $referendum
     * @return Pregunta
     */
    public function setReferendum(\AppBundle\Entity\VotacionReferendum $referendum = null)
    {
        $this->referendum = $referendum;

        return $this;
    }

    /**
     * Get referendum
     *
     * @return \AppBundle\Entity\VotacionReferendum 
     */
    public function getReferendum()
    {
        return $this->referendum;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     * @return Pregunta
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Pregunta
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set numeroVotosEnBlanco
     *
     * @param integer $numeroVotosEnBlanco
     * @return Pregunta
     */
    public function setNumeroVotosEnBlanco($numeroVotosEnBlanco)
    {
        $this->numeroVotosEnBlanco = $numeroVotosEnBlanco;

        return $this;
    }

    /**
     * Get numeroVotosEnBlanco
     *
     * @return integer 
     */
    public function getNumeroVotosEnBlanco()
    {
        return $this->numeroVotosEnBlanco;
    }

    public function incNumeroVotosEnBlanco($valor = 1)
    {
        $this->numeroVotosEnBlanco += $valor;

        return $this;
    }
}
