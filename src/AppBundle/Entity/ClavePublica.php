<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

use Doctrine\ORM\Mapping as ORM;

/**
 * SolicitudApp
 *
 * @ORM\Table(name="clave_publica")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class ClavePublica
{
    const CODIGO_LENGTH = 32;
    const PERIODO_EXPIRACION = "now +1days";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="public_key", type="string", length=4096)
     */
    private $clavePublica;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=256)
     */
    private $codigo;

    /**
     * @var datetime
     *
     * @ORM\Column(name="validado_codigo", type="datetime", nullable=true)
     * @Expose
     */
    private $validadoCodigo;

    /**
     * @var datetime
     *
     * @ORM\Column(name="expiracion", type="datetime", nullable=false)
     */
    private $expiracion;

    /**
     * @ORM\OneToOne(targetEntity="App", inversedBy="clavePublica")
     * @ORM\JoinColumn(name="app_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $app;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", cascade={"persist"})
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario;

    public function __construct()
    {
        $this->setCodigo($this->generateCodigo(SELF::CODIGO_LENGTH));
        $this->setExpiracion(new \DateTime(SELF::PERIODO_EXPIRACION));
    }

    private function generateCodigo($numDigitos = 6)
    {
        $numDigitos *= -1;
        return $this->codigo = substr((md5(uniqid(rand(10,1000), true))),$numDigitos);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return ClavePublica
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set validadoCodigo
     *
     * @param \DateTime $validadoCodigo
     * @return ClavePublica
     */
    public function setValidadoCodigo($validadoCodigo)
    {
        $this->validadoCodigo = $validadoCodigo;

        return $this;
    }

    /**
     * Get validadoCodigo
     *
     * @return \DateTime 
     */
    public function getValidadoCodigo()
    {
        return $this->validadoCodigo;
    }

    /**
     * Set app
     *
     * @param \AppBundle\Entity\App $app
     * @return ClavePublica
     */
    public function setApp(\AppBundle\Entity\App $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \AppBundle\Entity\App 
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set clavePublica
     *
     * @param string $clavePublica
     * @return ClavePublica
     */
    public function setClavePublica($clavePublica)
    {
        $this->clavePublica = $clavePublica;

        return $this;
    }

    /**
     * Get clavePublica
     *
     * @return string 
     */
    public function getClavePublica()
    {
        return $this->clavePublica;
    }

    /**
     * Set expiracion
     *
     * @param \DateTime $expiracion
     * @return ClavePublica
     */
    public function setExpiracion($expiracion)
    {
        $this->expiracion = $expiracion;

        return $this;
    }

    /**
     * Get expiracion
     *
     * @return \DateTime 
     */
    public function getExpiracion()
    {
        return $this->expiracion;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return ClavePublica
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
