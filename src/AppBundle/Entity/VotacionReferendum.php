<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Votacion as Votacion;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * ListasCerradas
 *
 * @ORM\Table(name="votacion_referendum")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class VotacionReferendum extends Votacion
{

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Pregunta", mappedBy="referendum", cascade={"persist", "remove"})
     * @Expose
     */
    private $preguntas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preguntas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add preguntas
     *
     * @param \AppBundle\Entity\Pregunta $preguntas
     * @return VotacionReferendum
     */
    public function addPregunta(\AppBundle\Entity\Pregunta $preguntas)
    {
        $this->preguntas[] = $preguntas;

        return $this;
    }

    /**
     * Remove preguntas
     *
     * @param \AppBundle\Entity\Pregunta $preguntas
     */
    public function removePregunta(\AppBundle\Entity\Pregunta $preguntas)
    {
        $this->preguntas->removeElement($preguntas);
    }

    /**
     * Get preguntas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPreguntas()
    {
        return $this->preguntas;
    }

    public function getClass()
    {
        return "VotacionReferendum";
    }
}
