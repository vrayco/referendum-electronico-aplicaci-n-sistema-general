<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;

use Doctrine\ORM\Mapping as ORM;

/**
 * App
 *
 * @ORM\Table(name="app")
 * @ORM\Entity
 */
class App
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sistema_operativo", type="string", length=16)
     */
    private $sistemaOperativo;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_id", type="string", length=1024)
     */
    private $registrationId;

    /**
     * @ORM\OneToOne(targetEntity="Usuario", inversedBy="app", cascade={"persist"})
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario;

    /**
     * @ORM\OneToOne(targetEntity="SolicitudApp", mappedBy="app", cascade={"persist", "remove"})
     */
    private $solicitudApp;

    /**
     * @ORM\OneToOne(targetEntity="ClavePublica", mappedBy="app", cascade={"persist", "remove"})
     */
    private $clavePublica;


    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registrationId
     *
     * @param string $registrationId
     * @return App
     */
    public function setRegistrationId($registrationId)
    {
        $this->registrationId = $registrationId;

        return $this;
    }

    /**
     * Get registrationId
     *
     * @return string 
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return App
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set solicitudApp
     *
     * @param \AppBundle\Entity\SolicitudApp $solicitudApp
     * @return App
     */
    public function setSolicitudApp(\AppBundle\Entity\SolicitudApp $solicitudApp = null)
    {
        $this->solicitudApp = $solicitudApp;

        return $this;
    }

    /**
     * Get solicitudApp
     *
     * @return \AppBundle\Entity\SolicitudApp 
     */
    public function getSolicitudApp()
    {
        return $this->solicitudApp;
    }

    /**
     * Set sistemaOperativo
     *
     * @param string $sistemaOperativo
     * @return App
     */
    public function setSistemaOperativo($sistemaOperativo)
    {
        $this->sistemaOperativo = $sistemaOperativo;

        return $this;
    }

    /**
     * Get sistemaOperativo
     *
     * @return string 
     */
    public function getSistemaOperativo()
    {
        return $this->sistemaOperativo;
    }

    /**
     * Set clavePublica
     *
     * @param \AppBundle\Entity\ClavePublica $clavePublica
     * @return App
     */
    public function setClavePublica(\AppBundle\Entity\ClavePublica $clavePublica = null)
    {
        $this->clavePublica = $clavePublica;

        return $this;
    }

    /**
     * Get clavePublica
     *
     * @return \AppBundle\Entity\ClavePublica 
     */
    public function getClavePublica()
    {
        return $this->clavePublica;
    }

}
