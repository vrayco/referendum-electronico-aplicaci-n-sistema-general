<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * CensoElectoral
 *
 * @ORM\Table(name="elector")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ElectorRepository")
 * @UniqueEntity(
 *     fields={"dni", "eventoElectoral"},
 *     errorPath="dni",
 *     message="Dni duplicado."
 * )
 * @ExclusionPolicy("all")
 */
class Elector
{
    const TOKEN_LENGTH = 32;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=16)
     * @AppBundleAssert\Dni
     * @Expose
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre")
     * @Expose
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="primer_apellido", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el primer apellido")
     * @Expose
     */
    private $primerApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="segundo_apellido", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el segundo apellido")
     * @Expose
     */
    private $segundoApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="token_votacion", type="string", length=64, unique=true)
     * @Expose
     */
    private $tokenVotacion;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Votacion", inversedBy="censoElectoral")
     * @ORM\JoinColumn(name="votacion_id", referencedColumnName="id")
     */
    private $votacion;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EventoElectoral", inversedBy="censoElectoral")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id")
     */
    private $eventoElectoral;

    /**
     * @ORM\Column(name="notificacion_convocatoria", type="datetime", nullable=true)
     */
    private $notificacionConvocatoria;

    /**
     * @ORM\Column(name="notificacion_apertura", type="datetime", nullable=true)
     */
    private $notificacionApertura;

    /**
     * @ORM\Column(name="notificacion_cierre", type="datetime", nullable=true)
     */
    private $notificacionCierre;

    /**
     * @ORM\Column(name="notificacion_escrutinio", type="datetime", nullable=true)
     */
    private $notificacionEscrutinio;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ElectorValidacion", mappedBy="elector", cascade={"persist", "remove"})
     */
    private $electorValidaciones;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return CensoElectoral
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->votacion = new \Doctrine\Common\Collections\ArrayCollection();

        // Genero el token de votación
        $bytes = openssl_random_pseudo_bytes(SELF::TOKEN_LENGTH);
        $token = bin2hex($bytes);
        $this->setTokenVotacion($token);
    }

    /**
     * Add votacion
     *
     * @param \AppBundle\Entity\Votacion $votacion
     * @return Elector
     */
    public function addVotacion(\AppBundle\Entity\Votacion $votacion)
    {
        $this->votacion[] = $votacion;

        return $this;
    }

    /**
     * Remove votacion
     *
     * @param \AppBundle\Entity\Votacion $votacion
     */
    public function removeVotacion(\AppBundle\Entity\Votacion $votacion)
    {
        $this->votacion->removeElement($votacion);
    }

    /**
     * Get votacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVotacion()
    {
        return $this->votacion;
    }
    

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     * @return Elector
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral 
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Elector
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set primerApellido
     *
     * @param string $primerApellido
     * @return Elector
     */
    public function setPrimerApellido($primerApellido)
    {
        $this->primerApellido = $primerApellido;

        return $this;
    }

    /**
     * Get primerApellido
     *
     * @return string 
     */
    public function getPrimerApellido()
    {
        return $this->primerApellido;
    }

    /**
     * Set segundoApellido
     *
     * @param string $segundoApellido
     * @return Elector
     */
    public function setSegundoApellido($segundoApellido)
    {
        $this->segundoApellido = $segundoApellido;

        return $this;
    }

    /**
     * Get segundoApellido
     *
     * @return string 
     */
    public function getSegundoApellido()
    {
        return $this->segundoApellido;
    }

    /**
     * Set tokenVotacion
     *
     * @param string $tokenVotacion
     * @return Elector
     */
    public function setTokenVotacion($tokenVotacion)
    {
        $this->tokenVotacion = $tokenVotacion;

        return $this;
    }

    /**
     * Get tokenVotacion
     *
     * @return string 
     */
    public function getTokenVotacion()
    {
        return $this->tokenVotacion;
    }

    /**
     * Add electorValidaciones
     *
     * @param \AppBundle\Entity\ElectorValidacion $electorValidaciones
     * @return Elector
     */
    public function addElectorValidacione(\AppBundle\Entity\ElectorValidacion $electorValidaciones)
    {
        $this->electorValidaciones[] = $electorValidaciones;

        return $this;
    }

    /**
     * Remove electorValidaciones
     *
     * @param \AppBundle\Entity\ElectorValidacion $electorValidaciones
     */
    public function removeElectorValidacione(\AppBundle\Entity\ElectorValidacion $electorValidaciones)
    {
        $this->electorValidaciones->removeElement($electorValidaciones);
    }

    /**
     * Get electorValidaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getElectorValidaciones()
    {
        return $this->electorValidaciones;
    }

    /**
     * Set notificacionConvocatoria
     *
     * @param \DateTime $notificacionConvocatoria
     * @return Elector
     */
    public function setNotificacionConvocatoria($notificacionConvocatoria)
    {
        $this->notificacionConvocatoria = $notificacionConvocatoria;

        return $this;
    }

    /**
     * Get notificacionConvocatoria
     *
     * @return \DateTime 
     */
    public function getNotificacionConvocatoria()
    {
        return $this->notificacionConvocatoria;
    }

    /**
     * Set notificacionApertura
     *
     * @param \DateTime $notificacionApertura
     * @return Elector
     */
    public function setNotificacionApertura($notificacionApertura)
    {
        $this->notificacionApertura = $notificacionApertura;

        return $this;
    }

    /**
     * Get notificacionApertura
     *
     * @return \DateTime 
     */
    public function getNotificacionApertura()
    {
        return $this->notificacionApertura;
    }

    /**
     * Set notificacionCierre
     *
     * @param \DateTime $notificacionCierre
     * @return Elector
     */
    public function setNotificacionCierre($notificacionCierre)
    {
        $this->notificacionCierre = $notificacionCierre;

        return $this;
    }

    /**
     * Get notificacionCierre
     *
     * @return \DateTime 
     */
    public function getNotificacionCierre()
    {
        return $this->notificacionCierre;
    }

    /**
     * Set notificacionEscrutinio
     *
     * @param \DateTime $notificacionEscrutinio
     * @return Elector
     */
    public function setNotificacionEscrutinio($notificacionEscrutinio)
    {
        $this->notificacionEscrutinio = $notificacionEscrutinio;

        return $this;
    }

    /**
     * Get notificacionEscrutinio
     *
     * @return \DateTime 
     */
    public function getNotificacionEscrutinio()
    {
        return $this->notificacionEscrutinio;
    }
}
