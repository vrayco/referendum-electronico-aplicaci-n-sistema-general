<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValidacionDni
 *
 * @ORM\Table(name="validacion_dni")
 * @ORM\Entity
 */
class ValidacionDni
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validateAt", type="datetime")
     */
    private $validateAt;

    /**
     * @ORM\OneToOne(targetEntity="Usuario", inversedBy="validacionDni")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="registros")
     * @ORM\JoinColumn(name="registrador_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $registrador;
    
    public function __construct()
    {
	    $this->validateAt = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set validateAt
     *
     * @param \DateTime $validateAt
     * @return ValidacionDni
     */
    public function setValidateAt($validateAt)
    {
        $this->validateAt = $validateAt;

        return $this;
    }

    /**
     * Get validateAt
     *
     * @return \DateTime 
     */
    public function getValidateAt()
    {
        return $this->validateAt;
    }

    /**
     * Set Usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return ValidacionDni
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set registrador
     *
     * @param \AppBundle\Entity\Usuario $registrador
     * @return ValidacionDni
     */
    public function setRegistrador(\AppBundle\Entity\Usuario $registrador = null)
    {
        $this->registrador = $registrador;

        return $this;
    }

    /**
     * Get registrador
     *
     * @return \AppBundle\Entity\User 
     */
    public function getRegistrador()
    {
        return $this->registrador;
    }
}
