<?php
	
namespace AppBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

 
/**
 * @ORM\Entity
 * @ORM\Table(name="usuario")
 * @UniqueEntity("dni")
 * @ExclusionPolicy("all") 
 */
class Usuario implements \Serializable
{
 
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Expose     
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=16, unique=true)
	 * @AppBundleAssert\Dni
     * @Expose
     * @Type("string")
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Por favor, ingrese su email")
     * @Expose
     * @Type("string")
     */    
    private $email;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese su nombre")
     * @Expose
     * @Type("string")
     */
    private $nombre;

	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Por favor, ingrese sus apellidos")
     * @Expose
     * @Type("string")
     */    
    private $apellidos;

	/**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="usuarios", cascade={"persist"})
     * @ORM\OrderBy({"name" = "ASC"})
     * @Expose
     *
     */
    private $roles;

    /**
     * @ORM\OneToOne(targetEntity="ValidacionDni", mappedBy="usuario", cascade={"persist", "remove"})
     * @Expose
     */
    protected $validacionDni;	

    /**
     * @ORM\OneToOne(targetEntity="ValidacionDniAuto", mappedBy="usuario", cascade={"persist", "remove"})
     * @Expose
     */
    protected $validacionDniAuto;

    /**
     * @ORM\OneToOne(targetEntity="TelefonoMovil", mappedBy="usuario", cascade={"persist", "remove"})
     * @Expose
     */
    protected $telefonoMovil;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\App", mappedBy="usuario", cascade={"persist", "remove"})
     */
    protected $app;

    /**
     * @ORM\OneToMany(targetEntity="ValidacionDni", mappedBy="registrador")
     */
    protected $registros;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\EventoElectoral", mappedBy="administrador")
     **/
    protected $eventosElecorales;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TokenConvocatoriaElectoral", mappedBy="usuario", cascade={"persist", "remove"})
     */
    private $tokensConvocatoriaElectoral;

    public function __construct()
    {
		$this->roles = new ArrayCollection();
		$this->registros = new ArrayCollection();
        $this->eventosElecorales = new ArrayCollection();
        $this->tokensConvocatoriaElectoral = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getDni()." - ".$this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nif
     *
     * @param string $nif
     * @return Usuario
     */
    public function setDni($dni)
    {
        $this->dni= $dni;

        return $this;
    }
    
    public function getUsuario()
    {
	    return $this->dni;
    }
    
    public function getRole()
    {
		$roles = array();
	    foreach ($this->roles as $role) {
	        $roles[] = $role->getRole();
	    }

	    return $roles;
    }

    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * Get nif
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }    
    
    
    public function getUsername()
    {
		return $this->dni;    
    }
    
    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Usuario
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set validacionDni
     *
     * @param \AppBundle\Entity\ValidacionDni $validacionDni
     * @return Usuario
     */
    public function setValidacionDni(\AppBundle\Entity\ValidacionDni $validacionDni = null)
    {
        $this->validacionDni = $validacionDni;

        return $this;
    }

    /**
     * Get validacionDni
     *
     * @return \AppBundle\Entity\ValidacionDni 
     */
    public function getValidacionDni()
    {
        return $this->validacionDni;
    }

    /**
     * Set ValidacionDniAuto
     *
     * @param \AppBundle\Entity\ValidacionDniAuto $ValidacionDniAuto
     * @return Usuario
     */
    public function setValidacionDniAuto(\AppBundle\Entity\ValidacionDniAuto $validacionDniAuto = null)
    {
        $this->validacionDniAuto = $validacionDniAuto;

        return $this;
    }

    /**
     * Get ValidacionDniAuto
     *
     * @return \AppBundle\Entity\ValidacionDniAuto 
     */
    public function getValidacionDniAuto()
    {
        return $this->validacionDniAuto;
    }

    /**
     * Set TelefonoMovil
     *
     * @param \AppBundle\Entity\TelefonoMovil $telefonoMovil
     * @return Usuario
     */
    public function setTelefonoMovil(\AppBundle\Entity\TelefonoMovil $telefonoMovil = null)
    {
        $this->telefonoMovil = $telefonoMovil;

        return $this;
    }

    /**
     * Get TelefonoMovil
     *
     * @return \AppBundle\Entity\TelefonoMovil 
     */
    public function getTelefonoMovil()
    {
        return $this->telefonoMovil;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add roles
     *
     * @param \AppBundle\Entity\Role $roles
     * @return Usuario
     */
    public function addRole(\AppBundle\Entity\Role $roles = null)
    {
        if($roles)
            $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \AppBundle\Entity\Role $roles
     */
    public function removeRole(\AppBundle\Entity\Role $rol)
    {
        if($rol->getName() !== 'Usuario')
            $this->roles->removeElement($rol);
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->dni,
            $this->email,
            $this->nombre,
            $this->apellidos,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->dni,
            $this->email,
            $this->nombre,
            $this->apellidos,
        ) = unserialize($serialized);
    }

    public function isValid()
    {
        if($this->isValidDni() and $this->isValidTelefono() and $this->getApp() and $this->isValidApp($this->getApp()->getRegistrationId()) and $this->isPublicKeyValid($this->getApp()->getRegistrationId()))
            return true;

        return false;
    }
    
    public function isValidDni()
    {
	    if($this->validacionDni !== null or $this->validacionDniAuto !== null)
	    	return true;

	    return false;
    }

    public function isValidTelefono()
    {
	    if($this->getTelefonoMovil() !== null)
	    	if($this->getTelefonoMovil()->getValidado())
		    	return true;

	    return false;
    }

    public function isValidApp($registrationId)
    {
        if($this->getApp() !== null)
            if($this->getApp()->getRegistrationId() === $registrationId)
                return true;

        return false;
    }

    public function isPublicKeyValid($registrationId)
    {
        if($this->isValidApp($registrationId) !== false)
            if ($this->getApp()->getClavePublica() !== null)
                if ($this->getApp()->getClavePublica()->getValidadoCodigo() !== null)
                    return true;

        return false;
    }

    /**
     * Add registros
     *
     * @param \AppBundle\Entity\ValidacionDni $registros
     * @return Usuario
     */
    public function addRegistro(\AppBundle\Entity\ValidacionDni $registros)
    {
        $this->registros[] = $registros;

        return $this;
    }

    /**
     * Remove registros
     *
     * @param \AppBundle\Entity\ValidacionDni $registros
     */
    public function removeRegistro(\AppBundle\Entity\ValidacionDni $registros)
    {
        $this->registros->removeElement($registros);
    }

    /**
     * Get registros
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegistros()
    {
        return $this->registros;
    }

    /**
     * Set app
     *
     * @param \AppBundle\Entity\App $app
     * @return Usuario
     */
    public function setApp(\AppBundle\Entity\App $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \AppBundle\Entity\App
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Add eventosElecorales
     *
     * @param \AppBundle\Entity\EventoElectoral $eventosElecorales
     * @return Usuario
     */
    public function addEventosElecorale(\AppBundle\Entity\EventoElectoral $eventosElecorales)
    {
        $this->eventosElecorales[] = $eventosElecorales;

        return $this;
    }

    /**
     * Remove eventosElecorales
     *
     * @param \AppBundle\Entity\EventoElectoral $eventosElecorales
     */
    public function removeEventosElecorale(\AppBundle\Entity\EventoElectoral $eventosElecorales)
    {
        $this->eventosElecorales->removeElement($eventosElecorales);
    }

    /**
     * Get eventosElecorales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEventosElecorales()
    {
        return $this->eventosElecorales;
    }

    /**
     * Add tokensConvocatoriaElectoral
     *
     * @param \AppBundle\Entity\TokenConvocatoriaElectoral $tokensConvocatoriaElectoral
     * @return Usuario
     */
    public function addTokensConvocatoriaElectoral(\AppBundle\Entity\TokenConvocatoriaElectoral $tokensConvocatoriaElectoral)
    {
        $this->tokensConvocatoriaElectoral[] = $tokensConvocatoriaElectoral;

        return $this;
    }

    /**
     * Remove tokensConvocatoriaElectoral
     *
     * @param \AppBundle\Entity\TokenConvocatoriaElectoral $tokensConvocatoriaElectoral
     */
    public function removeTokensConvocatoriaElectoral(\AppBundle\Entity\TokenConvocatoriaElectoral $tokensConvocatoriaElectoral)
    {
        $this->tokensConvocatoriaElectoral->removeElement($tokensConvocatoriaElectoral);
    }

    /**
     * Get tokensConvocatoriaElectoral
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTokensConvocatoriaElectoral()
    {
        return $this->tokensConvocatoriaElectoral;
    }
}
