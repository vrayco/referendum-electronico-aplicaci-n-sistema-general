<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

use Doctrine\ORM\Mapping as ORM;

/**
 * TelefonoMovil
 *
 * @ORM\Table(name="telefono_movil")
 * @ORM\Entity
 * @UniqueEntity("numeroTelefono")
 * @ExclusionPolicy("all")
 */
class TelefonoMovil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="prefijo_país", type="string", length=2)
     * @Assert\NotBlank(message="Por favor, ingrese el prefijo del país")
	 * @Assert\Choice(choices = {"34"}, message = "Escoge un prefijo válido.")
     * @Expose
     */    
    private $prefijoPais;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_telefono", type="string", length=16, unique=true)
     * @Assert\NotBlank(message="Por favor, ingrese su teléfono")  
	 * @AppBundleAssert\TelefonoMovil
     * @Expose
     */    
    private $numeroTelefono;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validateAt", type="datetime", nullable=true)
     */
    private $validateAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validado", type="boolean")
     */
    private $validado;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=256)
     */
    private $codigo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="envio_sms_codigo", type="datetime", nullable=true)
     */
    private $envioSmsCodigo;

    /**
     * @ORM\OneToOne(targetEntity="Usuario", inversedBy="telefonoMovil", cascade={"persist"})
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario;


    public function __construct()
    {
	    $this->validado = false;
		$this->setCodigo();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set validateAt
     *
     * @param \DateTime $validateAt
     * @return TelefonoMovil
     */
    public function setValidateAt($validateAt)
    {
        $this->validateAt = $validateAt;

        return $this;
    }

    /**
     * Get validateAt
     *
     * @return \DateTime 
     */
    public function getValidateAt()
    {
        return $this->validateAt;
    }

    /**
     * Set validado
     *
     * @param boolean $validado
     * @return TelefonoMovil
     */
    public function setValidado($validado)
    {
        if($validado) {
            $this->validado = true;
            $this->validateAt = new \DateTime('now');
        } else {
            $this->validado = false;
            $this->validateAt = null;
        }

        return $this;
    }

    /**
     * Get validado
     *
     * @return boolean 
     */
    public function getValidado()
    {
        return $this->validado;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return TelefonoMovil
     */
    public function setCodigo($numDigitos = 6)
    {
        $numDigitos *= -1;
        $this->codigo = $this->codigo = substr((md5(uniqid(rand(10,1000), true))),$numDigitos);

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set numeroTelefono
     *
     * @param string $numeroTelefono
     * @return TelefonoMovil
     */
    public function setNumeroTelefono($numeroTelefono)
    {
        $this->numeroTelefono = $numeroTelefono;

        return $this;
    }

    /**
     * Get numeroTelefono
     *
     * @return string 
     */
    public function getNumeroTelefono()
    {
        return $this->numeroTelefono;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return TelefonoMovil
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set envioSmsCodigo
     *
     * @param boolean $envioSmsCodigo
     * @return TelefonoMovil
     */
    public function setEnvioSmsCodigo($envioSmsCodigo)
    {
        $this->envioSmsCodigo = $envioSmsCodigo;

        return $this;
    }

    /**
     * Get envioSmsCodigo
     *
     * @return boolean 
     */
    public function getEnvioSmsCodigo()
    {
        return $this->envioSmsCodigo;
    }

    /**
     * Set prefijoPais
     *
     * @param string $prefijoPais
     * @return TelefonoMovil
     */
    public function setPrefijoPais($prefijoPais)
    {
        $this->prefijoPais = $prefijoPais;

        return $this;
    }

    /**
     * Get prefijoPais
     *
     * @return string 
     */
    public function getPrefijoPais()
    {
        return $this->prefijoPais;
    }
}
