<?php
// src/Acme/TaskBundle/Form/Type/TaskType.php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserRoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('roles', 'entity', array(
            'multiple' => true,   // Multiple selection allowed
            'expanded' => false,
            'class' => 'AppBundle:Role',
            'property' => 'name',
//            'query_builder' => function(EntityRepository $er) {
//                return $er->createQueryBuilder('u')
//                    ->where('u.name <> :role')
//                    ->setParameter('role', "Usuario");
//
//            }
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario',
        ));
    }

    public function getName()
    {
        return 'user_role';
    }
}