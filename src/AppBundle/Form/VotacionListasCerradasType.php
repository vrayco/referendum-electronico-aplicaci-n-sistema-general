<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VotacionListasCerradasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('descripcion')
            ->add('logotipo', new VotacionLogotipoType(), array(
                'required' => false
            ))
            ->add('votoEnBlanco')
            ->add('votosPorElector')
            ->add('minimoCandidatos')
            ->add('maximoCandidatos')
            ->add('minimoCandidatosSuplentes')
            ->add('maximoCandidatosSuplentes')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\VotacionListasCerradas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_votacionlistascerradas';
    }
}
