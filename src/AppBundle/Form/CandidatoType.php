<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CandidatoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreCompleto')
            ->add('posicion')
            ->add('foto', new CandidatoFotoType(), array(
                'required' => false
            ));

        if($options['seccion_lista'])
            $builder->add('seccionLista','choice', array(
                'choices' => array(
                    'titular'   => 'Titular',
                    'suplente'  => 'Suplente',
                    ),
                ))
            ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'AppBundle\Entity\Candidato',
            'seccion_lista' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_candidato';
    }
}
