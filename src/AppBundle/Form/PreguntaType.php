<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PreguntaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pregunta')
            ->add('orden')
            ->add('multirespuesta')
            ->add('respuestas', 'collection', array(
                'type'                  => new RespuestaType(),
                'allow_add'             => true,
                'allow_delete'          => true,
                'cascade_validation'    => true,
                'by_reference'          => false
            ));
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'AppBundle\Entity\Pregunta',
            'cascade_validation'    => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_pregunta';
    }
}
