<?php

namespace AppBundle\Form;

use AppBundle\Entity\EventoElectoral;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventoElectoralConvocarType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('estado','choice', array(
            'choices' => array(
                EventoElectoral::ESTADO_EDITABLE    => 'No convocar',
                EventoElectoral::ESTADO_CONVOCADO   => 'Convocar',
            ),
        ))
        ;

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EventoElectoral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_eventoelectoral_convocar';
    }
}
