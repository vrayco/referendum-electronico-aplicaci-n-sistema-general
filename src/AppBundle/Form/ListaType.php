<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ListaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreCompleto')
            ->add('nombreAbreviado')
            ->add('logotipo', new ListaLogotipoType(), array(
                'required' => false
            ))
            ->add('candidatos', 'collection', array(
                'type'                  => new CandidatoType(),
                'allow_add'             => true,
                'allow_delete'          => true,
                'cascade_validation'    => true,
                'by_reference'          =>false,
                'options'               => array('seccion_lista'=> $options['seccion_lista'])
                ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'AppBundle\Entity\Lista',
            'cascade_validation'    => true,
            'seccion_lista' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_lista';
    }
}
