<?php

namespace AppBundle\Form;

use AppBundle\Entity\EventoElectoral;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventoElectoralAbrirType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('estado','choice', array(
            'choices' => array(
                EventoElectoral::ESTADO_CONVOCADO  => 'No, no abrir el proceso electoral.',
                EventoElectoral::ESTADO_ABIERTO     => 'Sí, abrir el proceso electoral.',
            ),
        ))
        ;

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EventoElectoral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_eventoelectoral_abrir';
    }
}
