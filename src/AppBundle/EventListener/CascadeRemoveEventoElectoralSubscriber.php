<?php
// src/Acme/SearchBundle/EventListener/SearchIndexerSubscriber.php
namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
// for Doctrine 2.4: Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use AppBundle\Entity\Votacion;
use AppBundle\Entity\VotacionListasCerradas;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\Elector;

class CascadeRemoveEventoElectoralSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            //'preRemove',
        );
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $this->remove($args);
    }

    public function remove(LifecycleEventArgs $args)
    {
//        dump('Subscriber');
//        $entity = $args->getEntity();
//        $entityManager = $args->getEntityManager();
//
//        dump($entity);
//
//        // perhaps you only want to act on some "Product" entity
//        if ($entity instanceof EventoElectoral) {
//            dump('Evento Electoral');
//            $votaciones = $entity->getVotacion();
//            foreach($votaciones as $v) {
//                $candidaturas = $v->getCandidaturas();
//                foreach($candidaturas as $c) {
//                    $v->removeCandidatura($c);
//                }
//                $entity->removeVotacion($v);
//            }
//            $entityManager->flush();
//        }
//
//        if ($entity instanceof Elector) {
//            dump("ELECTORRRR");
//        }

    }
}