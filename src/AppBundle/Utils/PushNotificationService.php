<?php

namespace AppBundle\Utils;
use Symfony\Component\Security\Core\Util\StringUtils;

class PushNotificationService
{
    private $gcm;

    private $registrationIds = array();
    private $data = null;
    private $plataforma = null;

    public function __construct($gcm)
    {
        $this->gcm = $gcm;
    }

    public function send()
    {

        if(count($this->registrationIds) == 0 || !$this->data || !$this->plataforma)
            return false;

        if(StringUtils::equals("ANDROID", $this->plataforma)) {

            $this->gcm->setData($this->data);
            $this->gcm->setRegistrationIds($this->registrationIds);
            $resultado = $this->gcm->send();
            $resultado = json_decode($resultado);

            return $resultado;
        }

        return false;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setRegistrationIds($registrationIds)
    {
        $this->registrationIds = $registrationIds;
    }

    public function setPlataforma($plataforma)
    {
        $this->plataforma = $plataforma;
    }

}