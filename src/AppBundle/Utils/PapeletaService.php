<?php

namespace AppBundle\Utils;

use AppBundle\Controller\Backend\EventoElectoralController;
use AppBundle\Entity\Elector;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Votacion;
use AppBundle\Validator\Constraints\DniValidator;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\KernelInterface;

class PapeletaService
{
    private $container;
    private $em;
    private $rsa;
    private $router;

    public function __construct(Container $container, EntityManager $em, RsaService $rsa, Router $router)
    {
        $this->container    = $container;
        $this->em           = $em;
        $this->rsa          = $rsa;
        $this->router       = $router;
    }
    
    public function getPapeletaElector(EventoElectoral $eventoElectoral, Usuario $usuario)
    {
        $elector = $this->em->getRepository('AppBundle:Elector')->findOneBy(array('eventoElectoral' => $eventoElectoral, 'dni' => $usuario->getDni()));
        
        if(!$elector)
            return null;

        $infoEventoElectoral = array(
            'id'                    => $eventoElectoral->getId(),
            'nombre'                => $eventoElectoral->getNombre(),
            'descripcion'           => $eventoElectoral->getDescripcion(),
            'estado'                => $eventoElectoral->getEstado(),
            'logotipo'              => $eventoElectoral->getLogotipo(),
            'url_tablon_anuncios'   => $this->router->generate('tablon_anuncios_show', array('id' => $eventoElectoral->getId())),
            'votaciones'            => $elector->getVotacion()
        );

        $clavePublicaApp = $usuario->getApp()->getClavePublica()->getClavePublica();
        $tokenVotacion = $this->rsa->publicEncrypt($elector->getTokenVotacion(), $clavePublicaApp);

        $data = array(
            'evento_electoral'  => $infoEventoElectoral,
            'id_elector'        => $elector->getId(),
            'token_votacion'    => $tokenVotacion,
            'public_key'        => $this->rsa->getPublicKey(sprintf(EventoElectoralController::PREFIX_CLAVE_RSA, $eventoElectoral->getId())),
        );

        return $data;
    }
}