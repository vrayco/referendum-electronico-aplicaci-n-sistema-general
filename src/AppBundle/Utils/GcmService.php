<?php

namespace AppBundle\Utils;

use Symfony\Component\HttpKernel\KernelInterface;

class GcmService
{
    private $kernel;

    private $apiKey = null;
    private $url = null;

    private $registrationIds = array();
    private $data = null;

    public function __construct(KernelInterface $kernel, $apiKey, $url)
    {
        $this->kernel = $kernel;
        $this->apiKey = $apiKey;
        $this->url = $url;
    }

    public function send()
    {
        //Para los test, simulo el envío
        if($this->kernel->getEnvironment() == 'test') {
            $result = '{"success":1}';
            return $result;
        }

        $post = array(
            'registration_ids'  => $this->registrationIds,
            'data'              => $this->data,
        );

        $headers = array(
            'Authorization: key=' . $this->apiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, $this->url );

        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );
        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) )
        {
            //dump('GCM error: ' . curl_error( $ch ));
        }
        curl_close( $ch );

        return $result;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setRegistrationIds($registrationIds)
    {
        $this->registrationIds = $registrationIds;
    }

}