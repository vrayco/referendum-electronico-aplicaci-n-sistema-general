<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Elector;
use AppBundle\Entity\Votacion;
use AppBundle\Validator\Constraints\DniValidator;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\KernelInterface;

class CensoElectoralService
{
    private $input;
    private $votacion;
    private $mensajeError = null;
    private $container;
    private $em;

    public function __construct(Container $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
        $this->texto = null;
        $this->mensajeError = null;
    }

    public function setTexto($input) {
        $this->mensajeError = null;
        $this->input = $input;
    }

    public function setVotacion(Votacion $votacion)
    {
        $this->votacion = $votacion;
    }

    public function load()
    {
        $input = $this->input;
        $numLinea = 0;
        $eventoElectoral = $this->votacion->getEventoElectoral();

        $dnis = array();
        // Leo línea a línea
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $input) as $line) {
            $numLinea++;
            $cadenas = explode(';', trim($line));

            $datos = array(
                'dni'               => null,
                'nombre'            => null,
                'primer_apellido'   => null,
                'segundo_apellido'  => null
            );

            if(isset($cadenas[0]))
                $datos['dni'] = $cadenas[0];
            if(isset($cadenas[1]))
                $datos['nombre'] = $cadenas[1];
            if(isset($cadenas[2]))
                $datos['primer_apellido'] = $cadenas[2];
            if(isset($cadenas[3]))
                $datos['segundo_apellido'] = $cadenas[3];

            if (array_search($datos['dni'], $dnis) === false) {
                // Busco dni en el censo
                $elector = $this->em->getRepository('AppBundle:Elector')->findOneBy(array('dni' => $datos['dni'], 'eventoElectoral' => $eventoElectoral));

                if ($elector) {
                    $votaciones = $elector->getVotacion();
                    if(!$votaciones->contains($this->votacion))
                        $elector->addVotacion($this->votacion);
                } else {
                    $elector = new Elector();
                    $elector->setEventoElectoral($eventoElectoral);
                    $elector->addVotacion($this->votacion);
                    $elector->setDni($datos['dni']);
                    $elector->setNombre($datos['nombre']);
                    $elector->setPrimerApellido($datos['primer_apellido']);
                    $elector->setSegundoApellido($datos['segundo_apellido']);

                    $errors = $this->container->get('validator')->validate($elector);
                    if (count($errors) > 0) {
                        foreach ($errors as $error) {
                            $this->mensajeError .= "LINEA " . $numLinea . ": " . $error->getMessage().". ";
                        }

                        return false;
                    }
                }

                array_push($dnis, $datos['dni']);
                $this->em->persist($elector);

            } else {
                $this->mensajeError .= "LINEA " . $numLinea . ": Dni repetido.";

                return false;
            }
        }

        $this->em->flush();

        return true;
    }

    public function clean($votacion) {

        $censo = $votacion->getCensoElectoral();

        foreach($censo as $elector) {
            $elector->removeVotacion($votacion);
            if($elector->getVotacion()->count() == 0)
                $this->em->remove($elector);
        }
        $this->em->flush();
    }

    public function getMensajeError() {
        return $this->mensajeError;
    }

}