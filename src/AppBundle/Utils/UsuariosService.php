<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Usuario;
use AppBundle\Entity\TelefonoMovil;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class UsuariosService
{
	private $em;
    private $proveedorSms;
    const TEXTO_CODIGO_SMS                          = "TU CODIGO DE VALIDACION ES: %s";
    const MENSAJE_FLASH_CODIGO_SMS_OK               = "El envío del código de validación para el usuario con dni %s se ha realizado con éxito.";
    const MENSAJE_FLASH_CODIGO_SMS_NO_OK            = "El envío del código de validación para el usuario con dni %s no se ha realizado.";
    const MENSAJE_FLASH_USUARIO_CODIGO_SMS_OK       = "Hemos enviado tu código de validación por sms con éxito.";
    const MENSAJE_FLASH_USUARIO_CODIGO_SMS_NO_OK    = "¡Ups! No te hemos podido enviar por sms tu código de validación";


    public function __construct(EntityManager $em, BulkSmsService $proveedorSms)
	{
		$this->em = $em;
        $this->proveedorSms = $proveedorSms;
	}
	
	public function addRole(Usuario $usuario, $nombreRol)
	{
		$rol = $this->em->getRepository('AppBundle:Role')->findOneBy(array(
			'name'	=>	$nombreRol
		));

		if(!$rol) {
            throw new \RuntimeException(sprintf(sprintf("El rol '%s' no se encuentra en la BD", $nombreRol)));
        }

		$usuario->addRole($rol);
		
		return $usuario;
	}

    public function enviarCodigo(TelefonoMovil $telefono, array $opciones = array())
    {
        if(!isset($opciones['texto_codigo_sms']))
            throw new \RuntimeException(sprintf('Falta la opcion "texto_codigo_sms"'));

        // Envío sms con el código de validación del número de teléfono
        $envio = $this->proveedorSms->sendSms(array(
            'numeroTelefono'	=> $telefono->getPrefijoPais().$telefono->getNumeroTelefono(),
            'mensaje'			=> sprintf($opciones['texto_codigo_sms'], $telefono->getCodigo())
        ));

        if($envio) {
            $telefono->setEnvioSmsCodigo(new \DateTime('now'));
            $this->em->persist($telefono);
            $this->em->flush();

            if(isset($opciones['flash_bag'])) {
                $opciones['flash_bag']->add(
                    'success',
                    $opciones['mensaje_flash_ok']
                );
            }

            return true;
        } else {
            if(isset($opciones['flash_bag'])) {
                $opciones['flash_bag']->add(
                    'danger',
                    $opciones['mensajmensaje_flash_no_ok']
                );
            }

            return false;
        }
    }

    public function inicializarValidacionTelefono(Usuario $usuario, Request $request, $receptor = null)
    {
        $opciones = array(
            'texto_codigo_sms'      => self::TEXTO_CODIGO_SMS,
            'flash_bag'             => $request->getSession()->getFlashBag(),
        );

        // Si receptor no es nulo, el mensaje es para el usuario final (ROLE_USUARIO)
        if($receptor) {
            $opciones['mensaje_flash_ok'] = sprintf(self::MENSAJE_FLASH_USUARIO_CODIGO_SMS_OK, $usuario->getDni());
            $opciones['mensaje_flash_no_ok'] = sprintf(self::MENSAJE_FLASH_USUARIO_CODIGO_SMS_NO_OK, $usuario->getDni());
        } else {
            $opciones['mensaje_flash_ok'] = sprintf(self::MENSAJE_FLASH_CODIGO_SMS_OK, $usuario->getDni());
            $opciones['mensaje_flash_no_ok'] = sprintf(self::MENSAJE_FLASH_CODIGO_SMS_NO_OK, $usuario->getDni());
        }

        $usuario->getTelefonoMovil()->setValidado(false);
        $usuario->getTelefonoMovil()->setCodigo();

        // Si el usuario tenía una aplicación validada, la elimino
        if($usuario->getApp()) {
            $app = $usuario->getApp();
            $this->em->remove($app);
            $usuario->setApp(null);
        }

        // Envío sms con el código de validación del número de teléfono
        $this->enviarCodigo(
            $usuario->getTelefonoMovil(),
            $opciones
        );
    }

    public function getValidacionUsuario(Usuario $usuario)
    {
        $validacion = array(
            'validacion_usuario' => array(
                'dni'               => $usuario->isValidDni(),
                'telefono_movil'    => $usuario->isValidTelefono(),
            )
        );

        return $validacion;
    }

    public function getValidacionAplicacion(Usuario $usuario, $registrationId)
    {
        $validacion = array(
            'validacion_aplicacion' => array(
                'app'               => $usuario->isValidApp($registrationId),
                'clave_publica'     => $usuario->isPublicKeyValid($registrationId)
            )
        );

        return $validacion;
    }

    public function checkValidacionUsuario(Usuario $usuario)
    {
        if($usuario->isValidDni() && $usuario->isValidTelefono())
            return true;

        return false;
    }

    public function checkValidacionAplicacion(Usuario $usuario)
    {
        if($usuario->getApp() and $usuario->isValidApp($usuario->getApp()->getRegistrationId()) and $usuario->isPublicKeyValid($usuario->getApp()->getRegistrationId()))
                return true;

        return false;
    }
}