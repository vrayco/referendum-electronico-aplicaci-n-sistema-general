<?php

namespace AppBundle\Utils;


use AppBundle\Entity\EventoElectoral;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;


class APISistemaElectoralService
{
    const APERTURA_EVENTO_ELECTORAL = "/api/private/v1/evento-electoral/apertura.json";
    const CIERRE_EVENTO_ELECTORAL   = "/api/private/v1/evento-electoral/cierre.json";
    const CONVOCAR_EVENTO_ELECTORAL = "/api/private/v1/evento-electoral/convocar.json";
    const RESPUESTA_VALIDAR_CV      = "/api/private/v1/validar/voto.json";
    const PAPELETAS                 = "/api/public/v1/evento-electoral/%s/papeletas-y-l1s.json";

    private $container;
    private $apiUrl;

    public function __construct(Container $container, $apiUrl)
    {
        $this->container    = $container;
        $this->apiUrl       = $apiUrl;
    }

    public function convocarEventoElectoral(EventoElectoral $evento)
    {
        $url = $this->apiUrl . sprintf(self::CONVOCAR_EVENTO_ELECTORAL);

        $postData = array(
            'id'            => $evento->getId(),
            'nombre'        => $evento->getNombre(),
        );

        try {
            $ch = curl_init($url);
            if (FALSE === $ch)
                throw new Exception(curl_error($ch), curl_errno($ch));

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_POST            => TRUE,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_HTTPHEADER      => array(
                    //'Authorization: '.$authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));

            // Send the request
            $response = curl_exec($ch);
            
            if (FALSE === $response)
                throw new Exception(curl_error($ch), curl_errno($ch));

            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case Response::HTTP_CREATED:
                    $json = json_decode($response);
                    return $json;
                    break;
                case Response::HTTP_BAD_REQUEST:
                    return false;
                default:
                    return false;
            }
        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

            return false;
        }
    }

    public function abrirProcesoElectoral(EventoElectoral $evento)
    {
        $url = $this->apiUrl . sprintf(self::APERTURA_EVENTO_ELECTORAL);

        $postData = array(
            'id'            => $evento->getId(),
        );

        try {
            $ch = curl_init($url);
            if (FALSE === $ch)
                throw new Exception(curl_error($ch), curl_errno($ch));

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_POST            => TRUE,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_HTTPHEADER      => array(
                    //'Authorization: '.$authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));

            // Send the request
            $response = curl_exec($ch);

            if (FALSE === $response)
                throw new Exception(curl_error($ch), curl_errno($ch));

            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case Response::HTTP_ACCEPTED:
                    $json = json_decode($response);
                    return $json;
                    break;
                case Response::HTTP_BAD_REQUEST:
                    return false;
                default:
                    return false;
            }
        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

            return false;
        }
    }

    public function cerrarProcesoElectoral(EventoElectoral $evento)
    {
        $url = $this->apiUrl . sprintf(self::CIERRE_EVENTO_ELECTORAL);

        $postData = array(
            'id'            => $evento->getId(),
        );

        try {
            $ch = curl_init($url);
            if (FALSE === $ch)
                throw new Exception(curl_error($ch), curl_errno($ch));

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_POST            => TRUE,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_HTTPHEADER      => array(
                    //'Authorization: '.$authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));

            // Send the request
            $response = curl_exec($ch);

            if (FALSE === $response)
                throw new Exception(curl_error($ch), curl_errno($ch));

            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case Response::HTTP_ACCEPTED:
                    $json = json_decode($response);
                    return $json;
                    break;
                case Response::HTTP_BAD_REQUEST:
                    return false;
                default:
                    return false;
            }
        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

            return false;
        }
    }

    public function respuestaValidarCV($cv, $l1, $l1s, $votaciones)
    {
        $url = $this->apiUrl . sprintf(self::RESPUESTA_VALIDAR_CV);

        $postData = array(
            'cv'            => $cv,
            'l1'            => $l1,
            'l1s'           => json_encode($l1s),
            'votaciones'    => json_encode($votaciones)
        );

        try {
            $ch = curl_init($url);
            if (FALSE === $ch)
                throw new Exception(curl_error($ch), curl_errno($ch));

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_POST            => TRUE,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_HTTPHEADER      => array(
                    //'Authorization: '.$authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));

            // Send the request
            $response = curl_exec($ch);

            if (FALSE === $response)
                throw new Exception(curl_error($ch), curl_errno($ch));

            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case Response::HTTP_ACCEPTED:
                    $json = json_decode($response);
                    return $json;
                    break;
                case Response::HTTP_BAD_REQUEST:
                    return false;
                default:
                    return false;
            }
        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

            return false;
        }
    }

    public function getPapeletas(EventoElectoral $evento)
    {
        $url = $this->apiUrl . sprintf(self::PAPELETAS, $evento->getId());

        try {
            $ch = curl_init($url);
            if (FALSE === $ch)
                throw new Exception(curl_error($ch), curl_errno($ch));

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_POST            => FALSE,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_HTTPHEADER      => array(
                    //'Authorization: '.$authToken,
                    'Content-Type: application/json'
                ),
                //CURLOPT_POSTFIELDS => json_encode($postData)
            ));

            // Send the request
            $response = curl_exec($ch);

            if (FALSE === $response)
                throw new Exception(curl_error($ch), curl_errno($ch));

            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case Response::HTTP_OK:
                    $json = json_decode($response);
                    return $json->papeletas;
                    break;
                case Response::HTTP_BAD_REQUEST:
                    return false;
                default:
                    return false;
            }

        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

            return false;
        }
    }
}