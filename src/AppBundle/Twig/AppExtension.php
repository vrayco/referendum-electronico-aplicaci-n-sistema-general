<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('class', array($this, 'getClassFilter')),
        );
    }

    public function getClassFilter($object)
    {
        return (new \ReflectionClass($object))->getShortName();
    }

    public function getName()
    {
        return 'app_extension';
    }
}
