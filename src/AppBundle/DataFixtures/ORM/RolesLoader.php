<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Alice\DataFixtureLoader;
use Nelmio\Alice\Fixtures;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class RolesLoader extends DataFixtureLoader implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getFixtures()
    {
        return  array(
            //__DIR__ . '/roles.yml',

        );
    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}