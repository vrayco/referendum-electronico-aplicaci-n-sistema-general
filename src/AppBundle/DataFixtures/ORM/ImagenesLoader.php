<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CandidatoFoto;
use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\EventoElectoralLogotipo;
use AppBundle\Entity\ListaLogotipo;
use AppBundle\Entity\VotacionLogotipo;
use AppBundle\Form\CandidatoFotoType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImagenesLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
//        // Imagen del EVENTO ELECTORAL
//        $ev = $manager->getRepository('AppBundle:EventoElectoral')->findOneBy(array('nombre' => "ELECCIONES A CORTES GENERALES 2016"));
//        $logotipo = new EventoElectoralLogotipo();
//        $nombre_imagen = 'general.png';
//        $src    = $this->container->get('kernel')->getRootDir().'/../web/images/test/'.$nombre_imagen;
//        $dest   = $this->container->get('kernel')->getRootDir().'/../web/uploads/imagenes/evento_electoral_logotipo/'.$nombre_imagen;
//        if(!copy($src,$dest))
//            echo "ERROR: Copiando archivos!!!";
//        $file = new UploadedFile($dest,$nombre_imagen,null,null,null,true);
//        $logotipo->setFile($file);
//        $logotipo->setEventoElectoral($ev);
//        $manager->persist($logotipo);
//
//
//        // Imagen de VOTACIÓN: CONGRESO
//        $votacionCongreso = $manager->getRepository('AppBundle:Votacion')->findOneBy(array('nombre' => "CONGRESO DE LOS DIPUTADOS"));
//        $logotipo = new VotacionLogotipo();
//        $nombre_imagen = 'congreso.png';
//        $src    = $this->container->get('kernel')->getRootDir().'/../web/images/test/'.$nombre_imagen;
//        $dest   = $this->container->get('kernel')->getRootDir().'/../web/uploads/imagenes/votacion_logotipo/'.$nombre_imagen;
//        if(!copy($src,$dest))
//            echo "ERROR: Copiando archivos!!!";
//        $file = new UploadedFile($dest,$nombre_imagen,null,null,null,true);
//        $logotipo->setFile($file);
//        $logotipo->setVotacion($votacionCongreso);
//        $manager->persist($logotipo);
//
//
//        // Imagen de VOTACIÓN: SENADO
//        $votacionSenado = $manager->getRepository('AppBundle:Votacion')->findOneBy(array('nombre' => "SENADO"));
//        $logotipo = new VotacionLogotipo();
//        $nombre_imagen = 'senado.png';
//        $src    = $this->container->get('kernel')->getRootDir().'/../web/images/test/'.$nombre_imagen;
//        $dest   = $this->container->get('kernel')->getRootDir().'/../web/uploads/imagenes/votacion_logotipo/'.$nombre_imagen;
//        if(!copy($src,$dest))
//            echo "ERROR: Copiando archivos!!!";
//        $file = new UploadedFile($dest,$nombre_imagen,null,null,null,true);
//        $logotipo->setFile($file);
//        $logotipo->setVotacion($votacionSenado);
//        $manager->persist($logotipo);
//
//        $data = array(
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'nombreImagen'      => 'cc.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PP',
//                'nombreImagen'      => 'pp.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PODEMOS-IU-EQUO-CLIAS',
//                'nombreImagen'      => 'podemos.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PSOE-NCa',
//                'nombreImagen'      => 'psoe.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'C\'s',
//                'nombreImagen'      => 'ciudadanos.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PCPC',
//                'nombreImagen'      => 'pcpc.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PACMA',
//                'nombreImagen'      => 'pacma.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'EB',
//                'nombreImagen'      => 'eb.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'JXC',
//                'nombreImagen'      => 'jxc.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'RECORTES CERO-GRUPO VERDE',
//                'nombreImagen'      => 'recortescero.png'
//            ),
//
//        );
//
//        foreach ($data as $d) {
//            // Imagen de LISTA: CC
//            // Congreso
//            $lista = $manager->getRepository('AppBundle:Lista')->findOneBy(array('nombreAbreviado' => $d['nombreAbreviado'], 'votacionListasCerradas' => $votacionCongreso));
//            $logotipo = new ListaLogotipo();
//            $src = $this->container->get('kernel')->getRootDir() . '/../web/images/test/' . $d['nombreImagen'];
//            $dest = $this->container->get('kernel')->getRootDir() . '/../web/uploads/imagenes/lista_logotipo/' . $d['nombreImagen'];
//            if (!copy($src, $dest))
//                echo "ERROR: Copiando archivos!!!";
//            $file = new UploadedFile($dest, $d['nombreImagen'], null, null, null, true);
//            $logotipo->setFile($file);
//            $logotipo->setLista($lista);
//            $manager->persist($logotipo);
//            // Senado
//            $lista = $manager->getRepository('AppBundle:Lista')->findOneBy(array('nombreAbreviado' => $d['nombreAbreviado'], 'votacionListasAbiertas' => $votacionSenado));
//            $logotipo = new ListaLogotipo();
//            $nombre_imagen = 'cc.jpg';
//            $src = $this->container->get('kernel')->getRootDir() . '/../web/images/test/' . $d['nombreImagen'];
//            $dest = $this->container->get('kernel')->getRootDir() . '/../web/uploads/imagenes/lista_logotipo/2' . $d['nombreImagen'];
//            if (!copy($src, $dest))
//                echo "ERROR: Copiando archivos!!!";
//            $file = new UploadedFile($dest, '2' . $d['nombreImagen'], null, null, null, true);
//            $logotipo->setFile($file);
//            $logotipo->setLista($lista);
//            $manager->persist($logotipo);
//        }
//
//        $candidatosCongreso = array(
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'posicion'          => 1,
//                'nombreImagen'      => 'cc1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'posicion'          => 2,
//                'nombreImagen'      => 'cc2.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'posicion'          => 3,
//                'nombreImagen'      => 'cc3.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'posicion'          => 4,
//                'nombreImagen'      => 'cc4.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PP',
//                'posicion'          => 1,
//                'nombreImagen'      => 'pp1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PP',
//                'posicion'          => 2,
//                'nombreImagen'      => 'pp2.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PP',
//                'posicion'          => 3,
//                'nombreImagen'      => 'pp3.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PODEMOS-IU-EQUO-CLIAS',
//                'posicion'          => 1,
//                'nombreImagen'      => 'podemos1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PODEMOS-IU-EQUO-CLIAS',
//                'posicion'          => 4,
//                'nombreImagen'      => 'podemos4.png'
//            ),
//
//            array(
//                'nombreAbreviado'   => 'PSOE-NCa',
//                'posicion'          => 1,
//                'nombreImagen'      => 'psoe1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PSOE-NCa',
//                'posicion'          => 2,
//                'nombreImagen'      => 'psoe2.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PSOE-NCa',
//                'posicion'          => 4,
//                'nombreImagen'      => 'psoe4.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'C\'s',
//                'posicion'          => 1,
//                'nombreImagen'      => 'ciudadanos1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'C\'s',
//                'posicion'          => 2,
//                'nombreImagen'      => 'ciudadanos2.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PACMA',
//                'posicion'          => 1,
//                'nombreImagen'      => 'pacma1.png'
//            ),
//        );
//        foreach ($candidatosCongreso as $c) {
//            $lista = $manager->getRepository('AppBundle:Lista')->findOneBy(array('nombreAbreviado' => $c['nombreAbreviado'], 'votacionListasCerradas' => $votacionCongreso));
//            $candidato = $manager->getRepository('AppBundle:Candidato')->findOneBy(array(
//                'lista' => $lista,
//                'posicion' => $c['posicion']
//            ));
//            $foto = new CandidatoFoto();
//            $src = $this->container->get('kernel')->getRootDir() . '/../web/images/test/candidatos/congreso/' . $c['nombreImagen'];
//            $dest = $this->container->get('kernel')->getRootDir() . '/../web/uploads/imagenes/candidato_foto/' . $c['nombreImagen'];
//            if (!copy($src, $dest))
//                echo "ERROR: Copiando archivos!!!";
//            $file = new UploadedFile($dest, $c['nombreImagen'], null, null, null, true);
//            $foto->setFile($file);
//            $foto->setCandidato($candidato);
//            $manager->persist($foto);
//        }
//
//        $candidatosSenado = array(
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'nombreCompleto'    => "Mariano Pérez Hernández",
//                'nombreImagen'      => 'senado_cc1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'CCa-PNC',
//                'nombreCompleto'    => "María de la O Gaspar González",
//                'nombreImagen'      => 'senado_cc2.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PP',
//                'nombreCompleto'    => "Antonio Alarcó Hernández",
//                'nombreImagen'      => 'senado_pp1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PSOE-NCa',
//                'nombreCompleto'    => "Olivia María Delgado Oval",
//                'nombreImagen'      => 'senado_psoe1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PSOE-NCa',
//                'nombreCompleto'    => "José Vicente González Bethencourt",
//                'nombreImagen'      => 'senado_psoe2.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PODEMOS-IU-EQUO-CLIAS',
//                'nombreCompleto'    => "Manuel José González Mauricio",
//                'nombreImagen'      => 'senado_podemos1.png'
//            ),
//            array(
//                'nombreAbreviado'   => 'PODEMOS-IU-EQUO-CLIAS',
//                'nombreCompleto'    => "Marta Esther Jiménez Jaén",
//                'nombreImagen'      => 'senado_podemos2.png'
//            ),
//        );
//        foreach ($candidatosSenado as $c) {
//            $lista = $manager->getRepository('AppBundle:Lista')->findOneBy(array('nombreAbreviado' => $c['nombreAbreviado'], 'votacionListasAbiertas' => $votacionSenado));
//            $candidato = $manager->getRepository('AppBundle:Candidato')->findOneBy(array(
//                'lista' => $lista,
//                'nombreCompleto' => $c['nombreCompleto']
//            ));
//
//            $foto = new CandidatoFoto();
//            $src = $this->container->get('kernel')->getRootDir() . '/../web/images/test/candidatos/senado/' . $c['nombreImagen'];
//            $dest = $this->container->get('kernel')->getRootDir() . '/../web/uploads/imagenes/candidato_foto/' . $c['nombreImagen'];
//            if (!copy($src, $dest))
//                echo "ERROR: Copiando archivos!!!";
//            $file = new UploadedFile($dest, $c['nombreImagen'], null, null, null, true);
//            $foto->setFile($file);
//            $foto->setCandidato($candidato);
//            $manager->persist($foto);
//        }
//
//        $manager->flush();
    }

    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}