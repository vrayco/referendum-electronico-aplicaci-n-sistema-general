<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Alice\DataFixtureLoader;
use Nelmio\Alice\Fixtures;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class EventoElectoralLoader extends DataFixtureLoader implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getFixtures()
    {
        return  array(
            //__DIR__ . '/evento_electoral_new.yml',
//            __DIR__ . '/evento_electoral_test_new.yml',
            __DIR__ . '/evento_electoral_test_new2.yml',

        );
    }

    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }
}