<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Alice\DataFixtureLoader;
use Nelmio\Alice\Fixtures;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class SolicitudesAppLoader extends DataFixtureLoader implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getFixtures()
    {
        return  array(
//            __DIR__ . '/solicitudes_app.yml',

        );
    }

    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}